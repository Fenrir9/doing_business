import lib.holt_lib
import logging
import matplotlib.pyplot as mtp
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from openpyxl import load_workbook
import pandas as pd
from pathlib import Path
import re
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split


def run_holt(data_version, iteration):
    if data_version == "first_version":
        sheet_file_path = '../data/all_data_common_denominator/' + data_version + '.xlsx'
        metrics_file_path = '../data/all_data_common_denominator/holt/'
    elif data_version == "second_version":
        sheet_file_path = '../data/new_methodology/' + data_version + '.xlsx'
        metrics_file_path = '../data/new_methodology/holt/'
    else:
        logging.exception("Invalid data_version")
        quit()

    sheetnames = load_workbook(sheet_file_path, read_only=True, keep_links=False).sheetnames

    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
        sheetnames.remove(sheetname)

    for sheetname in sheetnames: #["startingBusinessData"]:#
        fieldData = pd.read_excel(sheet_file_path, sheet_name=sheetname)
        print(sheetname)

        countries = {country for country in fieldData['Economy'].values}

        columns = list(fieldData.columns)
        for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
            columns.remove(meta_data)

        for country in sorted(countries): #["Poland", "Zimbabwe"]: #

            plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                              '/holt/' + sheetname + '/'

            Path(plots_directory).mkdir(parents=True, exist_ok=True)

            with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
                for field in columns: #["Score.Time...Men..days."]: #
                    x = np.array(
                        fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')["DB.Year"].values).reshape(
                    (-1, 1))
                    y = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[field].values

                    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, shuffle=False)
                    to_predict_x = [year for year in range(4) + x.max() + 1]

                    y = pd.Series(y)
                    y_train = pd.Series(y_train)

                    test_holt_fcast_linear = lib.holt_lib.holt(y, y_train, y_test, 0.6, 0.2, len(X_test))

                    predict_holt_linear = lib.holt_lib.holt(y, y, y_test, 0.6, 0.2, len(to_predict_x))

                    regex = re.compile(r'Score.*')
                    if regex.match(field):
                        test_holt_fcast_linear = [0 if x < 0 else x for x in test_holt_fcast_linear]
                        test_holt_fcast_linear = [100 if x > 100 else x for x in test_holt_fcast_linear]
                        predict_holt_linear = [0 if x < 0 else x for x in predict_holt_linear]
                        predict_holt_linear = [100 if x > 100 else x for x in predict_holt_linear]
                    else:
                        test_holt_fcast_linear = [0 if x < 0 else x for x in test_holt_fcast_linear]
                        predict_holt_linear = [0 if x < 0 else x for x in predict_holt_linear]

                    mtp.scatter(X_train, y_train, color="blue", marker='x')
                    mtp.scatter(X_test, y_test, color="red", marker='+')
                    mtp.scatter(X_test, test_holt_fcast_linear, color="black", marker='x')
                    mtp.scatter(to_predict_x, predict_holt_linear, color="green", marker='x')

                    mtp.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
                    mtp.xticks(rotation=90)
                    mtp.title(f"{field}")
                    mtp.xlabel("Year")
                    mtp.ylabel("Value")
                    mtp.legend(['training data', 'test data', 'approx of test data linear ', 'predicted score linear ', 'approx of test data exponential', 'predicted score exponential '])
                    mtp.grid()
                    # mtp.show()
                    pdf.savefig()
                    mtp.close()

                    metric_row = pd.DataFrame({
                        "algorithm: ": ["holt_linear"],
                        "field_of_study": [sheetname],
                        "indicator": [field],
                        "country": [country],
                        "years": [X_train.tolist() + X_test.tolist() + to_predict_x],
                        "score": [y_train.tolist() + test_holt_fcast_linear + predict_holt_linear],
                        "SmoothL1Loss": ["NA"],
                        "MAE": [mean_absolute_error(y_test, test_holt_fcast_linear)],
                        "RMSE": [mean_squared_error(y_test, test_holt_fcast_linear, squared=False)]})

                    errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

    #
    Path(metrics_file_path).mkdir(parents=True, exist_ok=True)
    errorDataFrame.to_excel(metrics_file_path + 'metrics' + str(iteration) + '.xlsx', index=False)