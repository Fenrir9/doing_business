#!/bin/bash -l
## Nazwa zlecenia
#SBATCH -J eodb_holt_job
## Liczba alokowanych węzłów
#SBATCH -N 1
## Liczba zadań per węzeł (domyślnie jest to liczba alokowanych rdzeni na węźle)
#SBATCH --ntasks-per-node=1
## Ilość pamięci przypadającej na jeden rdzeń obliczeniowy (domyślnie 5GB na rdzeń)
#SBATCH --mem-per-cpu=5GB
## Maksymalny czas trwania zlecenia (format HH:MM:SS)
#SBATCH --time=72:00:00
## Nazwa grantu do rozliczenia zużycia zasobów
#SBATCH -A plgdypl2022
## Specyfikacja partycji
#SBATCH -p plgrid
## Plik ze standardowym wyjściem
#SBATCH --output="output_holt.out"
## Plik ze standardowym wyjściem błędów
#SBATCH --error="error_holt.err"


## przejscie do katalogu z ktorego wywolany zostal sbatch
cd $SLURM_SUBMIT_DIR
echo $PATH

#srun /bin/hostname
#module load plgrid/apps/adf/2014.07
#adf input.adf

cd doing_business/ml_python/
pwd
ls

python3 automatic_holt_run.py