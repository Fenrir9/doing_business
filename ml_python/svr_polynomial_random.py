import logging
import matplotlib.pyplot as mtp
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from openpyxl import load_workbook
import pandas as pd
from pathlib import Path
import re
from sklearn import linear_model
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR


def run_svr_polynomial_radnom(data_version, iteration):

    if data_version == "first_version":
        sheet_file_path = '../data/all_data_common_denominator/' + data_version + '.xlsx'
        metrics_file_path = '../data/all_data_common_denominator/svr_polynomial_random/'
    elif data_version == "second_version":
        sheet_file_path = '../data/new_methodology/' + data_version + '.xlsx'
        metrics_file_path = '../data/new_methodology/svr_polynomial_random/'
    else:
        logging.exception("Invalid data_version")
        quit()

    sheetnames = load_workbook(sheet_file_path, read_only=True, keep_links=False).sheetnames

    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
        sheetnames.remove(sheetname)

    for sheetname in sheetnames:
        fieldData = pd.read_excel(sheet_file_path, sheet_name=sheetname)
        print(sheetname)

        countries = {country for country in fieldData['Economy'].values}

        columns = list(fieldData.columns)
        for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
            columns.remove(meta_data)

        #regex = re.compile(r'Score.*')

        #columns = [i for i in columns if not regex.match(i)]

        for country in sorted(countries): #["Poland", "Zimbabwe"]: #
            plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                              '/svr_polynomial_random/' + sheetname + '/'
            Path(plots_directory).mkdir(parents=True, exist_ok=True)

            with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
                for field in columns:

                    x = np.array(
                        fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')["DB.Year"].values).reshape(
                    (-1, 1))
                    y = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[field].values

                    scaler = preprocessing.StandardScaler().fit(y.reshape(-1, 1))

                    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, shuffle=True)

                    y_train = scaler.transform(y_train.reshape(-1, 1))
                    y_test = scaler.transform(y_test.reshape(-1, 1))

                    to_predict_x = [year for year in range(4) + x.max() + 1]

                    #reg = SVR(kernel='poly', cache_size=7000)
                    reg = make_pipeline(StandardScaler(), SVR(C=1.0, epsilon=0.2, kernel='poly', tol=1e-4))
                    reg.fit(X_train, y_train.ravel())

                    all_x = np.array(np.append(x, to_predict_x)).reshape((-1, 1))
                    values = reg.predict(all_x)
                    # values = lin_reg.predict(poly_regs.fit_transform(all_x))
                    predicted_y = [reg.predict(np.asanyarray(to_predict_x).reshape(-1, 1))]

                    y_train = scaler.inverse_transform(y_train)
                    y_test = scaler.inverse_transform(y_test)
                    predicted_y = scaler.inverse_transform(predicted_y)
                    values = scaler.inverse_transform(values.reshape(-1, 1)).ravel()

                    regex = re.compile(r'Score.*')
                    if regex.match(field):
                        predicted_y = [0 if x < 0 else x for x in predicted_y[0]]
                        predicted_y = [100 if x > 100 else x for x in predicted_y]
                        values = [0 if x < 0 else x for x in values]
                        values = [100 if x > 100 else x for x in values]
                    else:
                        predicted_y = [0 if x < 0 else x for x in predicted_y[0]]
                        values = [0 if x < 0 else x for x in values]

                    mtp.scatter(X_train, y_train, color="blue", marker='x')
                    mtp.scatter(X_test, [values[int(iter - np.amin(x))] for iter in X_test.flatten().tolist()], color="black", marker='x')
                    mtp.scatter(X_test, y_test, color="red", marker='+')
                    mtp.scatter(to_predict_x, predicted_y, color="green", marker='x')
                    # y_regression = reg.predict(x)
                    # y_regression[y_regression < 0] = 0
                    # mtp.plot(x, y_regression)

                    mtp.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
                    mtp.xticks(rotation=90)
                    mtp.title(f"{field}")
                    mtp.xlabel("Year")
                    mtp.ylabel("Value")
                    mtp.legend(['training data', 'approx of test data', 'test data', 'predicted score'])
                    mtp.grid()
                    # mtp.show()
                    pdf.savefig()
                    mtp.close()

                    rmse = mean_squared_error(y_test, [values[int(iter - np.amin(x))] for iter in X_test.flatten().tolist()],
                                              squared=False)
                    mae = mean_absolute_error(y_test, [values[int(iter - np.amin(x))] for iter in X_test.flatten().tolist()])

                    metric_row = pd.DataFrame({
                        "algorithm: ": ["svr_polynomial_random"],
                        "field_of_study": [sheetname],
                        "indicator": [field],
                        "country": [country],
                        "years": [np.transpose(X_train)[0].tolist() + np.transpose(X_test)[0].tolist() + to_predict_x],
                        "score": [y_train.ravel().tolist() + [values[int(iter - np.amin(x))] for iter in X_test.flatten().tolist()] + values[-len(to_predict_x):]],
                        "SmoothL1Loss": ["NA"],
                        "MAE": [mae],
                        "RMSE": [rmse]})

                    errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

    Path(metrics_file_path).mkdir(parents=True, exist_ok=True)
    errorDataFrame.to_excel(metrics_file_path + 'metrics' + str(iteration) + '.xlsx', index=False)
