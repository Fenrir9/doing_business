import matplotlib.pyplot as mtp
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from openpyxl import load_workbook
import pandas as pd
import re


sheetnames = load_workbook('../data/all_data_common_denominator/first_version.xlsx', read_only=True, keep_links=False)\
    .sheetnames

for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData']:
    sheetnames.remove(sheetname)

for sheetname in sheetnames:
    fieldData = pd.read_excel('../data/all_data_common_denominator/first_version.xlsx',
                              sheet_name=sheetname)
    print(sheetname)

    countries = {country for country in fieldData['Economy'].values}

    columns = list(fieldData.columns)
    for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
        columns.remove(meta_data)

    regex = re.compile(r'Score.*')

    columns = [i for i in columns if not regex.match(i)]

    for country in sorted(countries):
        with PdfPages(
                '../data/plots/logarithmical_regression/' + sheetname + '/' + country + '_' + sheetname + '.pdf') as pdf:
            for field in columns:

                x = np.array(
                    fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')["DB.Year"] - 2003)
                y = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[field].values
                to_predict_x = [year for year in range(4) + x.max() + 1]

                fit = np.polyfit(np.log(x), y, 1)

                all_x = np.array(np.append(x, to_predict_x)).reshape((-1, 1))

                # values = lin_reg.predict(poly_regs.fit_transform(all_x))
                predicted_y = [fit[1] + fit[0] * x for x in to_predict_x]

                regex = re.compile(r'Score.*')
                if regex.match(field):
                    predicted_y = [0 if x < 0 else x for x in predicted_y]
                    predicted_y = [100 if x > 100 else x for x in predicted_y]
                else:
                    predicted_y = [0 if x < 0 else x for x in predicted_y]

                mtp.scatter(x, y, color="blue")
                mtp.scatter(to_predict_x, predicted_y, color="green")
                mtp.plot(x, fit[1] + fit[0] * x, color="red")

                mtp.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
                mtp.xticks(rotation=90)
                mtp.title(f"{field}")
                mtp.xlabel("Year")
                mtp.ylabel("Value")
                # mtp.show()
                pdf.savefig()
                mtp.close()
