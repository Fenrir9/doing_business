import lib.polynomial_regression_lib
import logging
from openpyxl import load_workbook
import pandas as pd
from pathlib import Path


def run_polynomial_regression_sklearn(data_version, iteration):
    if data_version == "first_version":
        sheet_file_path = '../data/all_data_common_denominator/' + data_version + '.xlsx'
        metrics_file_path = '../data/all_data_common_denominator/polynomial_regression_sklearn/'
    elif data_version == "second_version":
        sheet_file_path = '../data/new_methodology/' + data_version + '.xlsx'
        metrics_file_path = '../data/new_methodology/polynomial_regression_sklearn/'
    else:
        logging.exception("Invalid data_version")
        quit()

    sheetnames = load_workbook(sheet_file_path, read_only=True, keep_links=False).sheetnames

    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
        sheetnames.remove(sheetname)

    for sheetname in sheetnames:
        fieldData = pd.read_excel(sheet_file_path, sheet_name=sheetname)
        print(sheetname)

        countries = {country for country in fieldData['Economy'].values}

        columns = list(fieldData.columns)
        for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
            columns.remove(meta_data)

        for country in sorted(countries): #["Poland", "Zimbabwe"]: #
            errorDataFrame = pd.concat(
                [errorDataFrame,
                 lib.polynomial_regression_lib.save_plots_for_country(
                     country,
                     sheetname,
                     columns,
                     fieldData,
                     data_version,
                     iteration)])

    Path(metrics_file_path).mkdir(parents=True, exist_ok=True)
    errorDataFrame.to_excel(metrics_file_path + 'metrics' + str(iteration) + '.xlsx', index=False)


# poly_regs = PolynomialFeatures(degree=1)
# x_poly = poly_regs.fit_transform(x)
# lin_regs = LinearRegression()
# lin_regs.fit(x, y)
#
# mtp.scatter(x, y, color="blue")
# new_y = np.array([lin_regs.coef_ * i + lin_regs.intercept_ for i in np.append(x, to_predict_x)]).reshape(-1, 1)
#
# mtp.plot(np.append(x, to_predict_x), new_y, color="red")
# mtp.scatter(to_predict_x, [lin_regs.coef_ * i + lin_regs.intercept_ for i in to_predict_x], color="green")
# mtp.xticks(np.arange(min(x), max(to_predict_x)+1, 1.0))
# mtp.xticks(rotation=90)
#
# mtp.title("Time...Men..days.")
# mtp.xlabel("Year")
# mtp.ylabel("Value")
# mtp.show()