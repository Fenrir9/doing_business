import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as mtp
from neuralprophet import NeuralProphet, set_log_level
from openpyxl import load_workbook
# set_log_level("ERROR")

sheetnames = load_workbook('../data/all_data_common_denominator/first_version.xlsx', read_only=True, keep_links=False)\
    .sheetnames

for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
    sheetnames.remove(sheetname)

for sheetname in ["easeOfDoingBusinessData"]: #sheetnames:
    fieldData = pd.read_excel('../data/all_data_common_denominator/first_version.xlsx',
                              sheet_name=sheetname)
    print(sheetname)

    countries = {country for country in fieldData['Economy'].values}

    columns = list(fieldData.columns)
    for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
        columns.remove(meta_data)

    for country in ["Poland", "Zimbabwe"]: #sorted(countries):
        with PdfPages(
                '../data/plots/neural_prophet_time_based_features_only_cross_validation/' + sheetname + '/' + country + '_' + sheetname + '.pdf') as pdf:
            for field in columns:

            #fieldData = pd.read_excel('../data/all_data_common_denominator/first_version.xlsx', sheet_name="startingBusinessData")

                sf_load_df = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[["DB.Year", field]]

                sf_load_df = sf_load_df.rename(columns={"DB.Year": "ds"})
                sf_load_df = sf_load_df.rename(columns={field: "y"})
                #sf_load_df = fieldData


                sf_load_df["ds"] = pd.to_datetime(sf_load_df["ds"].astype('str'), format='%Y')
                sf_load_df["ds"] = sf_load_df["ds"].apply(lambda dt: dt.replace(month=12, day=31))
                sf_load_df.head(3)

                m = NeuralProphet(
                    # weekly_seasonality=6,
                    # daily_seasonality=10,
                    yearly_seasonality=False,
                    trend_reg=0,
                    learning_rate=0.1,
                )
                (df_train1, df_test1), (df_train2, df_test2), (df_train3, df_test3), (df_train4, df_test4), (df_train5, df_test5) = \
                    m.crossvalidation_split_df(sf_load_df, freq='Y')  # , valid_p = 0.3) #, valid_p = 1.0/12)
                df_train = pd.DataFrame(df_train1, df_train2)
                if len(df_train["y"].unique()) == 1:
                    # print(sf_load_df["y"].iloc[0])
                    # print(sf_load_df["y"].max())
                    a = df_train["y"].unique()
                    to_predict_x = [2022, 2023, 2024]
                    predicted_y = [sf_load_df["y"].iloc[0]] * 3

                    mtp.scatter(to_predict_x, predicted_y, color="blue")
                else:

                    #metrics = m.fit(df_train, freq='Y', validation_df=df_test, progress='plot')
                    metrics = m.fit(df_train, freq='Y', validation_df=df_test, progress='bar')
                    metrics.tail(1)

                    forecast = m.predict(df_train)
                    # fig = m.plot(forecast)
                    #
                    # mtp.show()
#
                    forecast = m.predict(df_test)
                    m = m.highlight_nth_step_ahead_of_each_forecast(1)
                    # fig = m.plot(forecast[-7*24:])
                    # fig = m.plot(forecast)
                    # mtp.show()


                    # fig_param = m.plot_parameters()
                    # mtp.show()

                    # future = df_test
                    # for prediction_number in range(3):
                    #     future = m.make_future_dataframe(future, periods=6, n_historic_predictions=len(sf_load_df))
                    #     forecast = m.predict(future)
                    #     future["y"].iloc[-1:] = forecast.tail(1)["yhat1"].item()

                    future = m.make_future_dataframe(df_test, periods=3, n_historic_predictions=len(sf_load_df))
                    forecast = m.predict(future)

                    #fig = mtp.scatter(sf_load_df["ds"], sf_load_df["y"], color="blue")

                    forecast["ds"] = forecast["ds"] - pd.DateOffset(years=1)

                    fig, ax = mtp.subplots(figsize=(10, 10))
                    m.plot(forecast, xlabel="Year", ylabel="Value", ax=ax)
                    ax.set_title(f"{field}")
                    #mtp.show()
                    #print(future)

                pdf.savefig()
                mtp.close()
