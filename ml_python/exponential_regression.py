import logging
import matplotlib.pyplot as mtp
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from openpyxl import load_workbook
import pandas as pd
from pathlib import Path
import re
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split


def run_exponential_regression(data_version, iteration):
    if data_version == "first_version":
        sheet_file_path = '../data/all_data_common_denominator/' + data_version + '.xlsx'
        metrics_file_path = '../data/all_data_common_denominator/exponential_regression/'
    elif data_version == "second_version":
        sheet_file_path = '../data/new_methodology/' + data_version + '.xlsx'
        metrics_file_path = '../data/new_methodology/exponential_regression/'
    else:
        logging.exception("Invalid data_version")
        quit()

    sheetnames = load_workbook(sheet_file_path, read_only=True, keep_links=False).sheetnames

    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
        sheetnames.remove(sheetname)

    for sheetname in sheetnames:
        fieldData = pd.read_excel(sheet_file_path, sheet_name=sheetname)
        print(sheetname)

        countries = {country for country in fieldData['Economy'].values}

        columns = list(fieldData.columns)
        for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
            columns.remove(meta_data)

        # regex = re.compile(r'Score.*')
        # columns = [i for i in columns if not regex.match(i)]

        for country in sorted(countries): #["Poland", "Zimbabwe"]: #

            plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                              '/exponential_regression/' + sheetname + '/'

            Path(plots_directory).mkdir(parents=True, exist_ok=True)

            with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
                for field in columns:
                    #print(field)
                    x = np.array(
                        fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')["DB.Year"])
                    y = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[field].values
                    to_predict_x = [year for year in range(4) + x.max() + 1]

                    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, shuffle=False)
                    to_predict_x = [year for year in range(4) + x.max() + 1]

                    fit = np.polyfit(X_train, np.log(y_train), 1)

                    all_x = np.array(np.append(x, to_predict_x)).reshape((-1, 1))
                    all_x = np.transpose(all_x).tolist()[0]
                    values = [np.exp(fit[1] + fit[0] * x) for x in all_x]
                    predicted_y = [np.exp(fit[1] + fit[0] * x) for x in to_predict_x]

                    regex = re.compile(r'Score.*')
                    if regex.match(field):
                        predicted_y = [0 if x < 0 else x for x in predicted_y]
                        predicted_y = [100 if x > 100 else x for x in predicted_y]
                        values = [0 if x < 0 else x for x in values]
                        values = [100 if x > 100 else x for x in values]
                    else:
                        predicted_y = [0 if x < 0 else x for x in predicted_y]
                        values = [0 if x < 0 else x for x in values]

                    mtp.scatter(X_train, y_train, color="blue", marker='x')
                    mtp.scatter(X_test, y_test, color="red", marker='x')
                    mtp.scatter(to_predict_x, predicted_y, color="green", marker='x')

                    if not np.isnan(np.sum(fit)):
                        mtp.plot(x, np.exp(fit[1] + fit[0] * x))

                    mtp.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
                    mtp.xticks(rotation=90)
                    mtp.title(f"{field}")
                    mtp.xlabel("Year")
                    mtp.ylabel("Value")
                    mtp.legend(['training data', 'test data', 'predicted score', 'regression'])
                    mtp.grid()
                    # mtp.show()
                    pdf.savefig()
                    mtp.close()

                    if not np.isnan(np.sum(values)):
                        rmse = mean_squared_error(y_test, values[-len(to_predict_x) - len(y_test):-len(to_predict_x)],
                                                  squared=False)
                        mae = mean_absolute_error(y_test, values[-len(to_predict_x) - len(y_test):-len(to_predict_x)])

                    metric_row = pd.DataFrame({
                        "algorithm: ": ["exponential_regression"],
                        "field_of_study": [sheetname],
                        "indicator": [field],
                        "country": [country],
                        "years": [X_train.tolist() + X_test.tolist() + to_predict_x],
                        "score": [y_train.tolist() + values[-len(to_predict_x) - len(y_test):-len(to_predict_x)] + values[-len(to_predict_x):]],
                        "SmoothL1Loss": ["NA"],
                        "MAE": [mae],
                        "RMSE": [rmse]})

                    errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

    Path(metrics_file_path).mkdir(parents=True, exist_ok=True)
    errorDataFrame.to_excel(metrics_file_path + 'metrics' + str(iteration) + '.xlsx', index=False)
    # errorDataFrame.to_excel("../data/plots/exponential_regression/metrics.xlsx", index=False)
