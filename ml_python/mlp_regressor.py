import logging
import matplotlib.pyplot as mtp
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from openpyxl import load_workbook
import pandas as pd
from pathlib import Path
import re
from sklearn import linear_model
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler


def run_mlp_regressor(data_version, iteration):

    if data_version == "first_version":
        sheet_file_path = '../data/all_data_common_denominator/' + data_version + '.xlsx'
        metrics_file_path = '../data/all_data_common_denominator/mlp_regressor/'
    elif data_version == "second_version":
        sheet_file_path = '../data/new_methodology/' + data_version + '.xlsx'
        metrics_file_path = '../data/new_methodology/mlp_regressor/'
    else:
        logging.exception("Invalid data_version")
        quit()

    sheetnames = load_workbook(sheet_file_path, read_only=True, keep_links=False).sheetnames

    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
        sheetnames.remove(sheetname)

    for sheetname in sheetnames: #["dealingWithConstructionPermitsD"]: #
        fieldData = pd.read_excel(sheet_file_path, sheet_name=sheetname)
        print(sheetname)

        countries = {country for country in fieldData['Economy'].values}

        columns = list(fieldData.columns)
        for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
            columns.remove(meta_data)

        #regex = re.compile(r'Score.*')

        #columns = [i for i in columns if not regex.match(i)]

        for country in sorted(countries): #["Poland", "Zimbabwe"]: #["Afghanistan"]: #
            plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                              '/mlp_regressor/' + sheetname + '/'
            Path(plots_directory).mkdir(parents=True, exist_ok=True)

            with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
                for field in columns:

                    x = np.array(
                        fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')["DB.Year"].values).reshape(
                    (-1, 1))
                    y = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[field].values

                    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, shuffle=True)
                    to_predict_x = [year for year in range(4) + x.max() + 1]

                    scaler = preprocessing.StandardScaler().fit(y.reshape(-1, 1))
                    y_train = scaler.transform(y_train.reshape(-1, 1))
                    y_test = scaler.transform(y_test.reshape(-1, 1))

                    # sc = StandardScaler()
                    # scaler = sc.fit(X_train)
                    # trainX_scaled = scaler.transform(X_train)
                    # testX_scaled = scaler.transform(X_test)
                    # to_predict_x_scalled = scaler.transform(np.array(to_predict_x).reshape(-1, 1))

                    mlp_reg = make_pipeline(StandardScaler(), MLPRegressor(max_iter=10000, activation='relu', solver='adam'))
                    mlp_reg.fit(X_train, y_train.ravel())

                    #mlp_reg = MLPRegressor(max_iter=10000, activation='relu', solver='adam')

                    all_x = np.array(np.append(x, to_predict_x)).reshape((-1, 1))
                    values = mlp_reg.predict(all_x)

                    future_y = [mlp_reg.predict(np.asanyarray(to_predict_x).reshape(-1, 1))]

                    y_train = scaler.inverse_transform(y_train)
                    y_test = scaler.inverse_transform(y_test)
                    future_y = scaler.inverse_transform(future_y)
                    values = scaler.inverse_transform(values.reshape(-1, 1)).ravel()

                    regex = re.compile(r'Score.*')
                    if regex.match(field):
                        future_y = [0 if x < 0 else x for x in future_y[0]]
                        future_y = [100 if x > 100 else x for x in future_y]
                        values = [0 if x < 0 else x for x in values]
                        values = [100 if x > 100 else x for x in values]
                    else:
                        future_y = [0 if x < 0 else x for x in future_y[0]]
                        values = [0 if x < 0 else x for x in values]

                    mtp.scatter(X_train, y_train, color="blue", marker='x')
                    mtp.scatter(X_test, [values[int(iter - np.amin(x))] for iter in X_test.flatten().tolist()], color="black", marker='x')
                    mtp.scatter(X_test, y_test, color="red", marker='+')
                    mtp.scatter(to_predict_x, future_y, color="green", marker='x')
                    # y_regression = reg.predict(x)
                    # y_regression[y_regression < 0] = 0
                    # mtp.plot(x, y_regression)

                    mtp.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
                    mtp.xticks(rotation=90)
                    mtp.title(f"{field}")
                    mtp.xlabel("Year")
                    mtp.ylabel("Value")
                    mtp.legend(['training data', 'approx of test data', 'test data', 'predicted score'])
                    mtp.grid()
                    # mtp.show()
                    pdf.savefig()
                    mtp.close()

                    rmse = mean_squared_error(y_test, [values[int(iter - np.amin(x))] for iter in X_test.flatten().tolist()],
                                              squared=False)
                    mae = mean_absolute_error(y_test, [values[int(iter - np.amin(x))] for iter in X_test.flatten().tolist()])

                    metric_row = pd.DataFrame({
                        "algorithm: ": ["mlp_regressor"],
                        "field_of_study": [sheetname],
                        "indicator": [field],
                        "country": [country],
                        "years": [np.transpose(X_train)[0].tolist() + np.transpose(X_test)[0].tolist() + to_predict_x],
                        "score": [y_train.tolist() + [values[int(iter - np.amin(x))] for iter in X_test.flatten().tolist()] + values[-len(to_predict_x):]],
                        "SmoothL1Loss": ["NA"],
                        "MAE": [mae],
                        "RMSE": [rmse]})

                    errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

    Path(metrics_file_path).mkdir(parents=True, exist_ok=True)
    errorDataFrame.to_excel(metrics_file_path + 'metrics' + str(iteration) + '.xlsx', index=False)
