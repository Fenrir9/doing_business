import lib.calculate_starting_business_random
import logging
from openpyxl import load_workbook
import pandas as pd
from pathlib import Path


def run_linear_regression_ind_separated_random(data_version, iteration):
    if data_version == "first_version":
        sheet_file_path = '../data/all_data_common_denominator/' + data_version + '.xlsx'
        metrics_file_path = '../data/all_data_common_denominator/linear_regression_ind_separated_random/'
    elif data_version == "second_version":
        sheet_file_path = '../data/new_methodology/' + data_version + '.xlsx'
        metrics_file_path = '../data/new_methodology/linear_regression_ind_separated_random/'
    else:
        logging.exception("Invalid data_version")
        quit()

    sheetnames = load_workbook(sheet_file_path, read_only=True, keep_links=False).sheetnames

    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
        sheetnames.remove(sheetname)

    for sheetname in sheetnames:
        fieldData = pd.read_excel(sheet_file_path, sheet_name=sheetname)
        print(sheetname)
        countries = {country for country in fieldData['Economy'].values}

        for country in sorted(countries): #["Poland", "Zimbabwe"]: #
            fieldCountry = fieldData.loc[fieldData['Economy'] == country]
            fieldCountry = fieldCountry.sort_values(by=['DB.Year'])

            errorDataFrame = pd.concat([errorDataFrame, lib.calculate_starting_business_random.calculate_starting_business_random(fieldCountry, sheetname, data_version, iteration)], ignore_index=True, axis=0)

    Path(metrics_file_path).mkdir(parents=True, exist_ok=True)
    errorDataFrame.to_excel(metrics_file_path + 'metrics' + str(iteration) + '.xlsx', index=False)
    # errorDataFrame.to_excel("../data/plots/linear_regression_separated_ind/metrics.xlsx", index=False)