import logging
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as mtp
from neuralprophet import NeuralProphet, set_log_level
import numpy as np
from openpyxl import load_workbook
import pandas as pd
from pathlib import Path
import re
# set_log_level("ERROR")


def run_neural_prophet_time_based_features_only(data_version, iteration):
    if data_version == "first_version":
        sheet_file_path = '../data/all_data_common_denominator/' + data_version + '.xlsx'
        metrics_file_path = '../data/all_data_common_denominator/neural_prophet_time_based_features_only/'
        # metrics_file_path = '../data/all_data_common_denominator/neural_prophet_time_based_features_only/'
    elif data_version == "second_version":
        sheet_file_path = '../data/new_methodology/' + data_version + '.xlsx'
        metrics_file_path = '../data/new_methodology/neural_prophet_time_based_features_only/'
    else:
        logging.exception("Invalid data_version")
        quit()

    sheetnames = load_workbook(sheet_file_path, read_only=True, keep_links=False).sheetnames

    number_of_years_to_forecast = 4

    for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
        sheetnames.remove(sheetname)

    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "SmoothL1Loss", "MAE", "RMSE"])

    for sheetname in sheetnames: #["gettingElectricityData"]: #
        if sheetname == "gettingElectricityData":
            number_of_forecast = 2
        else:
            number_of_forecast = 4
        fieldData = pd.read_excel(sheet_file_path, sheet_name=sheetname)
        print(sheetname)

        countries = {country for country in fieldData['Economy'].values}

        columns = list(fieldData.columns)
        for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
            columns.remove(meta_data)

        for country in sorted(countries): #["Poland", "Zimbabwe"]: #["Venezuela, RB"]:#
            print(country)
            plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                              '/neural_prophet_time_based_features_only/' + sheetname + '/'

            Path(plots_directory).mkdir(parents=True, exist_ok=True)

            with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
                for field in columns: #["Procedures...Men..number."]: #["Score.Cost....of.income.per.capita."]:#

                #fieldData = pd.read_excel('../data/all_data_common_denominator/first_version.xlsx', sheet_name="startingBusinessData")

                    sf_load_df = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[["DB.Year", field]]

                    sf_load_df = sf_load_df.rename(columns={"DB.Year": "ds"})
                    sf_load_df = sf_load_df.rename(columns={field: "y"})
                    #sf_load_df = fieldData

                    sf_load_df["ds"] = pd.to_datetime(sf_load_df["ds"].astype("int").astype('str'), format='%Y')
                    sf_load_df["ds"] = sf_load_df["ds"].apply(lambda dt: dt.replace(month=12, day=31))
                    sf_load_df.head(3)

                    m = NeuralProphet(
                        n_forecasts=number_of_forecast,
                        growth='off',
                        # weekly_seasonality=6,
                        # daily_seasonality=10,
                        yearly_seasonality=False,
                        trend_reg=0,
                        learning_rate=0.0001,
                        normalize="standardize",
                        epochs=10000
                    )
                    df_train, df_test = m.split_df(sf_load_df, freq='Y')  # , valid_p = 0.3) #, valid_p = 1.0/12)

                    if len(df_train["y"].unique()) == 1:
                        forecat_timestamps = [
                            pd.Timestamp(min(sf_load_df["ds"]))
                            + year * pd.tseries.frequencies.to_offset('Y') for year in range(len(sf_load_df) + number_of_years_to_forecast)]
                        data = {"ds": forecat_timestamps, "y": [sf_load_df["y"].iloc[0]] * (len(sf_load_df) + number_of_years_to_forecast)}
                        all_data = pd.DataFrame(data=data)
                        #all_data = pd.concat([sf_load_df, forecast], ignore_index=True)

                        regex = re.compile(r'Score.*')

                        if regex.match(field):
                            all_data.loc[all_data['y'] > 100, 'y'] = 100
                            all_data.loc[all_data['y'] < 0, 'y'] = 0
                        else:
                            all_data.loc[all_data['y'] < 0, 'y'] = 0

                        metric_row = pd.DataFrame({
                            "algorithm: ": ["neural_prophet_time_based_features_only"],
                            "field_of_study": [sheetname],
                            "indicator": [field],
                            "country": [country],
                            "years": [[x.year for x in all_data["ds"].tolist()]],
                            "score": [all_data["y"].tolist()],
                            "SmoothL1Loss": ["NA"],
                            "MAE": ["NA"],
                            "RMSE": ["NA"]})

                        errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

                    else:
                        metrics = m.fit(df_train, freq='Y', validation_df=df_test, progress='plot')
                        # metrics = m.fit(df_train, freq='Y', validation_df=df_test, progress='bar')
                        metrics.tail(1)

                        forecast = m.predict(df_train)
                        # fig = m.plot(forecast)
                        #
                        # mtp.show()
    #
                        test_forecast = m.predict(df_test)
                        m = m.highlight_nth_step_ahead_of_each_forecast(1)
                        # fig = m.plot(forecast[-7*24:])
                        # fig = m.plot(forecast)
                        # mtp.show()

                        # fig_param = m.plot_parameters()
                        # mtp.show()

                        # future = df_test
                        # for prediction_number in range(3):
                        #     future = m.make_future_dataframe(future, periods=6, n_historic_predictions=len(sf_load_df))
                        #     forecast = m.predict(future)
                        #     future["y"].iloc[-1:] = forecast.tail(1)["yhat1"].item()

                        future = m.make_future_dataframe(df_test, periods=4, n_historic_predictions=len(sf_load_df))
                        forecast = m.predict(future)

                        #fig = mtp.scatter(sf_load_df["ds"], sf_load_df["y"], color="blue")

                        # forecast["ds"] = forecast["ds"] - pd.DateOffset(years=1)
                        #sf_load_df = sf_load_df.rename(columns={'y': 'yhat1'})
                        all_data = pd.concat([df_train, test_forecast[['ds', 'yhat1']], forecast[['ds', 'yhat1']].iloc[-number_of_years_to_forecast:]], ignore_index=True)
                        #all_data = all_data.rename(columns={'yhat1': 'y'})
                        all_data.loc[all_data['y'].isnull(), 'y'] = all_data['yhat1']
                        met_test = m.test(df=df_test)

                        # alg = ["neural_prophet_time_based_features_only"]
                        # fos = [sheetname]
                        # ind = [field]
                        # country = [country]
                        # y = [[x.year for x in all_data["ds"].tolist()][-len(df_test) - number_of_forecast:]]
                        # s = [all_data["y"].tolist()[-len(df_test) - number_of_forecast:]]
                        # sloss = [all_data["y"].tolist()[-len(df_test) - number_of_forecast:]]
                        # mae = [met_test.iloc[0]['MAE']]
                        # rmse = [met_test.iloc[0]['RMSE']]

                        regex = re.compile(r'Score.*')

                        if regex.match(field):
                            all_data.loc[all_data['y'] > 100, 'y'] = 100
                            all_data.loc[all_data['y'] < 0, 'y'] = 0
                        else:
                            all_data.loc[all_data['y'] < 0, 'y'] = 0

                        metric_row = pd.DataFrame({
                            "algorithm: ": ["neural_prophet_time_based_features_only"],
                            "field_of_study": [sheetname],
                            "indicator": [field],
                            "country": [country],
                            "years": [[x.year for x in all_data["ds"].tolist()]],
                            "score": [all_data["y"].tolist()],
                            "SmoothL1Loss": [met_test.iloc[0]['SmoothL1Loss']],
                            "MAE": [met_test.iloc[0]['MAE']],
                            "RMSE": [met_test.iloc[0]['RMSE']]})

                        # errorDataFrame = errorDataFrame.append(metric_row, ignore_index=True)
                        errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

                    # all_data["ds"] = all_data["ds"] - pd.DateOffset(years=1)

                    # fig, ax = mtp.subplots(figsize=(10, 10))
                    # m.plot(forecast, xlabel="Year", ylabel="Value", ax=ax)
                    # ax.set_title(f"{field}")
                    #mtp.show()
                    #print(future)
                    all_data["y"] = all_data["y"].clip(lower=0)

                    mtp.scatter(
                        [x.year for x in all_data["ds"].tolist()][:-len(df_test) -number_of_years_to_forecast],
                        all_data["y"].tolist()[:-len(df_test) -number_of_years_to_forecast],
                        color="blue",
                        marker='x')
                    mtp.scatter(
                        [x.year for x in all_data["ds"].tolist()][-len(df_test) -number_of_years_to_forecast: -len(df_test)],
                        all_data["y"].tolist()[-len(df_test) -number_of_years_to_forecast: -len(df_test)],
                        color="black",
                        marker='x')
                    mtp.scatter(
                        [x.year for x in df_test["ds"].tolist()],
                        df_test["y"].tolist(),
                        color="red",
                        marker='+')
                    mtp.scatter(
                        [x.year for x in all_data["ds"].tolist()][-number_of_years_to_forecast:],
                        all_data["y"].tolist()[-number_of_years_to_forecast:],
                        color="green",
                        marker='x')

                    a = np.arange(int(min(all_data["ds"]).year), int(max(all_data["ds"]).year) + 1, 1)
                    mtp.xticks(np.arange(int(min(all_data["ds"]).year), int(max(all_data["ds"]).year) + 1, 1))
                    mtp.xticks(rotation=90)
                    mtp.title(f"{field}")
                    mtp.xlabel("Year")
                    mtp.ylabel("Value")
                    mtp.legend(['training data', 'approx of test data', 'test data', 'predicted', 'regression'])
                    mtp.grid()
                    pdf.savefig()
                    mtp.close()

    Path(metrics_file_path).mkdir(parents=True, exist_ok=True)
    errorDataFrame.to_excel(metrics_file_path + 'metrics' + str(iteration) + '.xlsx', index=False)
    # errorDataFrame.to_excel("../data/plots/neural_prophet_time_based_features_only/metrics.xlsx", index=False)