from lib.polynomila_degree_plot import PolynomialDegreePlot
import logging
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as mtp
import numpy as np
import pandas as pd
from pathlib import Path
import re
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
import sys


def save_plots_for_country(country, sheetname, columns, fieldData, data_version, iteration):
    errorDataFrame = pd.DataFrame(
        columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                      '/polynomial_regression_sklearn/' + sheetname + '/'

    Path(plots_directory).mkdir(parents=True, exist_ok=True)

    with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
        for field in columns:

            x = np.array(
                fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')["DB.Year"].values).reshape(
                (-1, 1))
            y = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[field].values

            X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, shuffle=False)
            to_predict_x = [year for year in range(4) + x.max() + 1]

            polynomial_plots = list()

            for degree in range(1, 6):
                poly_regs = PolynomialFeatures(degree=degree)
                x_poly = poly_regs.fit_transform(X_train)
                lin_reg = LinearRegression()
                lin_reg.fit(x_poly, y_train)
                y = lin_reg.predict(poly_regs.fit_transform(x))
                all_x = np.array(np.append(x, to_predict_x)).reshape((-1, 1))
                values = lin_reg.predict(poly_regs.fit_transform(all_x))

                values = [value for value in values]

                regex = re.compile(r'Score.*')
                if regex.match(field):
                    values = [100 if value > 100 else value for value in values]
                    values = [0 if value < 0 else value for value in values]
                else:
                    values = [0 if value < 0 else value for value in values]

                rmse = mean_squared_error(y_test, values[-len(to_predict_x)-len(y_test):-len(to_predict_x)], squared=False)
                mae = mean_absolute_error(y_test, values[-len(to_predict_x)-len(y_test):-len(to_predict_x)])

                polynomial_plots.append(PolynomialDegreePlot(degree, y, values, rmse, mae))

            best_polynomial_plot = get_best_plot(polynomial_plots)

            mtp.scatter(X_train, y_train, color="blue", marker='x')
            mtp.scatter(X_test, y_test, color="red", marker='x')
            mtp.scatter(to_predict_x, best_polynomial_plot.get_values()[-len(to_predict_x):], color="green", marker='x')
            mtp.plot(x, best_polynomial_plot.get_regression_value())

            mtp.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
            mtp.xticks(rotation=90)
            mtp.title(f"{field} degree: {best_polynomial_plot.get_degree()}")
            mtp.xlabel("Year")
            mtp.ylabel("Value")
            mtp.legend(['training data', 'test data', 'predicted score', 'regression'])
            mtp.grid()
            # mtp.show()
            pdf.savefig()
            mtp.close()

            metric_row = pd.DataFrame({
                "algorithm: ": ["polynomial_regression_sklearn"],
                "field_of_study": [sheetname],
                "indicator": [field],
                "country": [country],
                "years": [np.transpose(X_train)[0].tolist() + np.transpose(X_test)[0].tolist() + to_predict_x],
                "score": [np.transpose(y_train).tolist() + best_polynomial_plot.get_values()[-len(to_predict_x) - len(y_test): -len(to_predict_x)] + best_polynomial_plot.get_values()[-len(to_predict_x):]],
                "SmoothL1Loss": ["NA"],
                "MAE": [best_polynomial_plot.get_mae()],
                "RMSE": [best_polynomial_plot.get_rmse()]})

            errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

    return errorDataFrame


def get_best_plot(polynomial_plots, indicator='rmse'):
    error = sys.float_info.max
    for polynomial_plot in polynomial_plots:
        if indicator == 'rmse':
            if error > polynomial_plot.get_rmse():
                error = polynomial_plot.get_rmse()
        elif indicator == 'mae':
            if error > polynomial_plot.get_mae():
                error = polynomial_plot.get_mae()
        else:
            logging.exception("undefined error indicator")
    return identify_plot(polynomial_plots, error, indicator)


def identify_plot(polynomial_plots, error, indicator):
    for polynomial_plot in polynomial_plots:
        if indicator == 'rmse':
            if polynomial_plot.get_rmse() == error:
                return polynomial_plot
        elif indicator == 'mae':
            if polynomial_plot.get_mae() == error:
                return polynomial_plot
        else:
            logging.exception("undefined error indicator")
