from lib.polynomila_degree_plot import PolynomialDegreePlot
import logging
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as mtp
import numpy as np
import pandas as pd
from pathlib import Path
import re
from scipy import linalg
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PolynomialFeatures
from statsmodels.tsa.arima.model import ARIMA
import sys


def arima_save_plots_for_country(country, sheetname, columns, fieldData, data_version, iteration):
    errorDataFrame = pd.DataFrame(
        columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                      '/arima/' + sheetname + '/'

    Path(plots_directory).mkdir(parents=True, exist_ok=True)

    with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
        for field in columns: #["Time...Men..days."]:
            x = np.array(
                fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')["DB.Year"].values).reshape(
                (-1, 1))
            y = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[field].values

            X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, shuffle=False)
            to_predict_x = [year for year in range(4) + x.max() + 1]

            polynomial_plots = list()

            if data_version == "first_version":
                max_degree = 5
            else:
                max_degree = 1

            for degree in range(1, max_degree+1):
                poly_regs = PolynomialFeatures(degree=degree)
                x_poly = poly_regs.fit_transform(X_train)
                lin_reg = LinearRegression()
                lin_reg.fit(x_poly, y_train)
                #y = lin_reg.predict(poly_regs.fit_transform(x))
                all_x = np.array(np.append(x, to_predict_x)).reshape((-1, 1))
                values = lin_reg.predict(poly_regs.fit_transform(all_x))

                values = [value for value in values]

                regex = re.compile(r'Score.*')
                if regex.match(field):
                    values = [100 if value > 100 else value for value in values]
                    values = [0 if value < 0 else value for value in values]
                else:
                    values = [0 if value < 0 else value for value in values]

                rmse = mean_squared_error(y_test, values[-len(to_predict_x)-len(y_test):-len(to_predict_x)], squared=False)
                mae = mean_absolute_error(y_test, values[-len(to_predict_x)-len(y_test):-len(to_predict_x)])

                polynomial_plots.append(PolynomialDegreePlot(degree, y, values, rmse, mae))

            best_polynomial_plot = get_best_plot(polynomial_plots)


            degree = best_polynomial_plot.get_degree()
            arima_predictions_test = list()

            scaler = preprocessing.StandardScaler().fit(y.reshape(-1, 1))

            history = [y for y in scaler.transform(y_train.reshape(-1, 1)).ravel()]
            y_test = scaler.transform(y_test.reshape(-1, 1)).ravel()

            # history = [y for y in y_train]
            # print("test prediction")

            for t in range(len(y_test)):
                arima_model = ARIMA(history, order=(2, degree, 0), enforce_stationarity=False)
                arima_model_fit = arima_model.fit()
                output = arima_model_fit.forecast()
                # arima_model = make_pipeline(StandardScaler(), ARIMA(history, order=(2, degree, 0)))
                # arima_model.fit(x.ravel()[:len(history)].reshape(-1, 1), np.array(history).reshape(-1, 1))
                # output = arima_model.predict(x.ravel()[:len(history)-1].reshape(-1, 1))
                arima_predictions_test.append(output[0])
                history.append(y_test.ravel()[t])
                # residuals = pd.DataFrame(arima_model_fit.resid)
                # residuals.plot()
                # mtp.show()
                #
                # residuals.plot(kind='kde')
                # mtp.show()
                # print(residuals.describe())

            # print("future prediction")
            arima_prediction_future = list()
            for t in range(len(to_predict_x)):
                arima_model = ARIMA(history, order=(2, degree, 0), enforce_stationarity=False)
                arima_model_fit = arima_model.fit()
                # arima_model = make_pipeline(StandardScaler(), ARIMA(history, order=(2, degree, 0)))
                # arima_model_fit = arima_model.fit(y.ravel()[:len(history)].reshape(-1, 1))
                output = arima_model_fit.forecast()
                arima_prediction_future.append(output[0])
                history.append(arima_prediction_future[t])

            arima_prediction_future = scaler.inverse_transform(np.array(arima_prediction_future).reshape(-1, 1)).ravel()
            arima_predictions_test = scaler.inverse_transform(np.array(arima_predictions_test).reshape(-1, 1)).ravel()
            y_test = scaler.inverse_transform(y_test.reshape(-1, 1)).ravel()

            regex = re.compile(r'Score.*')
            if regex.match(field):
                arima_prediction_future = [100 if value > 100 else value for value in arima_prediction_future]
                arima_prediction_future = [0 if value < 0 else value for value in arima_prediction_future]
                arima_predictions_test = [100 if value > 100 else value for value in arima_predictions_test]
                arima_predictions_test = [0 if value < 0 else value for value in arima_predictions_test]
                y_test = [100 if value > 100 else value for value in y_test]
                y_test = [0 if value < 0 else value for value in y_test]
                y_train = [100 if value > 100 else value for value in y_train]
                y_train = [0 if value < 0 else value for value in y_train]
            else:
                arima_prediction_future = [0 if value < 0 else value for value in arima_prediction_future]
                arima_predictions_test = [0 if value < 0 else value for value in arima_predictions_test]
                y_test = [0 if value < 0 else value for value in y_test]
                y_train = [0 if value < 0 else value for value in y_train]

            mtp.scatter(X_train, y_train, color="blue", marker='x')
            mtp.scatter(X_test, arima_predictions_test, color="black", marker='x')
            mtp.scatter(X_test, y_test, color="red", marker='+')
            mtp.scatter(to_predict_x, arima_prediction_future, color="green", marker='x')

            mtp.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
            mtp.xticks(rotation=90)
            mtp.title(f"{field} degree: {best_polynomial_plot.get_degree()}")
            mtp.xlabel("Year")
            mtp.ylabel("Value")
            mtp.legend(['training data', 'approx of test data', 'test data', 'predicted score'])
            mtp.grid()
            # mtp.show()
            pdf.savefig()
            mtp.close()

            metric_row = pd.DataFrame({
                "algorithm: ": ["arima"],
                "field_of_study": [sheetname],
                "indicator": [field],
                "country": [country],
                "years": [np.transpose(X_train)[0].tolist() + np.transpose(X_test)[0].tolist() + to_predict_x],
                "score": [y_train + arima_predictions_test + arima_prediction_future],
                "SmoothL1Loss": ["NA"],
                "MAE": [mean_absolute_error(y_test, arima_predictions_test)],
                "RMSE": [np.sqrt(mean_squared_error(y_test, arima_predictions_test))]})

            errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

    return errorDataFrame


def get_best_plot(polynomial_plots, indicator='rmse'):
    error = sys.float_info.max
    for polynomial_plot in polynomial_plots:
        if indicator == 'rmse':
            if error > polynomial_plot.get_rmse():
                error = polynomial_plot.get_rmse()
        elif indicator == 'mae':
            if error > polynomial_plot.get_mae():
                error = polynomial_plot.get_mae()
        else:
            logging.exception("undefined error indicator")
    return identify_plot(polynomial_plots, error, indicator)


def identify_plot(polynomial_plots, error, indicator):
    for polynomial_plot in polynomial_plots:
        if indicator == 'rmse':
            if polynomial_plot.get_rmse() == error:
                return polynomial_plot
        elif indicator == 'mae':
            if polynomial_plot.get_mae() == error:
                return polynomial_plot
        else:
            logging.exception("undefined error indicator")
