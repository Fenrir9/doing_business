class PolynomialDegreePlot:
    def __init__(self, degree, regression_value, values, rmse, mae):
        self.degree = degree
        self.regression_value = regression_value
        self.values = values
        self.rmse = rmse
        self.mae = mae

    def get_degree(self):
        return self.degree

    def get_regression_value(self):
        return self.regression_value

    def get_values(self):
        return self.values

    def get_rmse(self):
        return self.rmse

    def get_mae(self):
        return self.mae
