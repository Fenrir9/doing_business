import numpy as np


def compute_cost_multi(x_factors, y_results, theta):
    no_training_examples = len(y_results)

    cost = (1 / (2 * no_training_examples)) * sum(np.power(((x_factors * theta) - y_results), 2))
    return cost
