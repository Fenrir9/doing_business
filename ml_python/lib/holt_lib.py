import matplotlib.pyplot as plt
import numpy as np
from statsmodels.tsa.api import Holt


def holt(y, y_to_train, y_to_test, smoothing_level, smoothing_slope, predict_date):
    # y.plot(marker='o', color='black', legend=True, figsize=(14, 7))

    holt_fit_linear = Holt(y_to_train).fit(smoothing_level, smoothing_slope, optimized=False)
    holt_fcast_linear = holt_fit_linear.forecast(predict_date).rename("Holt's linear trend")
    # mse1 = ((holt_fcast_linear - y_to_test) ** 2).mean()
    # print('The Root Mean Squared Error of Holt''s Linear trend {}'.format(round(np.sqrt(mse1), 2)))

    # holt_fit_exponential = Holt(y_to_train, damped_trend=True).fit(smoothing_level, smoothing_slope, optimized=False)
    # holt_fcast_exponential = holt_fit_exponential.forecast(predict_date).rename("Exponential trend")
    # mse2 = ((holt_fcast_exponential - y_to_test) ** 2).mean()
    # print('The Root Mean Squared Error of Holt''s Exponential trend {}'.format(round(np.sqrt(mse2), 2)))

    # holt_fit_linear.fittedvalues.plot(marker="o", color='blue')
    # holt_fcast_linear.plot(color='blue', marker="o", legend=True)
    # holt_fit_exponential.fittedvalues.plot(marker="o", color='red')
    # holt_fcast_exponential.plot(color='red', marker="o", legend=True)

    # plt.show()

    return holt_fcast_linear