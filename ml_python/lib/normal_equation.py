import pandas as pd
import numpy as np


def normal_equation(x_feature, y_results):
    theta = np.matmul(np.linalg.pinv(
        np.matmul(np.transpose(x_feature), x_feature)), np.matmul(np.transpose(x_feature), y_results))
    return theta
