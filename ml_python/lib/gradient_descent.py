from lib import compute_cost
import numpy as np


def gradient_descent(x_factors, y_results, theta, alphaLearningRate, num_iters):
    noTrainingExamples = len(y_results)
    J_history = np.zeros(num_iters)

    for no_calculation in range(num_iters):

        # first version
        # error = (x_factors * theta) - y_results
        # temp_theta0 = (
        #         theta[0] -
        #         ((alphaLearningRate / noTrainingExamples) * sum(np.multiply(error, x_factors[:, 0])))).item(0)
        # temp_theta1 = (
        #         theta[1] -
        #         ((alphaLearningRate / noTrainingExamples) * sum(np.multiply(error, x_factors[:, 1])))).item(0)
        # theta = np.matrix(
        #     [[temp_theta0], [temp_theta1]]
        # )

        # # second version
        # error = (x_factors * theta) - y_results
        # temp_theta0 = (
        #         theta[0] - ((alphaLearningRate / noTrainingExamples) * np.transpose(x_factors[:, 0]) * error)).item(0)
        # temp_theta1 = (
        #         theta[1] - ((alphaLearningRate / noTrainingExamples) * np.transpose(x_factors[:, 1]) * error)).item(0)
        # theta = np.matrix(
        #     [[temp_theta0], [temp_theta1]]
        # )

        # # third version vectorized
        error = (x_factors * theta) - y_results
        theta = theta - ((alphaLearningRate / noTrainingExamples) * np.transpose(x_factors) * error)

        # # Save the cost J in every iteration
        J_history[no_calculation] = compute_cost.compute_cost(x_factors, y_results, theta)
    print(J_history)

# test
# x = np.matrix(
#     [[1, 2014], [1, 2015], [1, 2016], [1, 2017], [1, 2018], [1, 2019], [1, 2020]]
# )
#
# theta = np.matrix(
#     [[-3948], [2]]
# )
#
# y = np.matrix(
#     [[80], [82], [83], [84], [87], [86], [92]]
# )
#
# gd.gradient_descent(x, y, theta, 0.05, 20)
