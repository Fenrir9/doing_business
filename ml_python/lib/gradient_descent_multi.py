from lib import compute_cost_multi
import numpy as np


def gradient_descent_multi(x_factors, y_results, theta, alphaLearningRate, num_iters):
    no_training_examples = len(y_results)
    cost_history = np.zeros(num_iters)

    for no_calculation in range(num_iters):
        error = (x_factors * theta) - y_results
        theta = theta - ((alphaLearningRate / no_training_examples) * np.transpose(x_factors) * error)

        # # Save the cost J in every iteration
        cost_history[no_calculation] = compute_cost_multi.compute_cost_multi(x_factors, y_results, theta)
    # print(cost_history)
    return cost_history
