import datetime
import lib.gradient_descent as gd
import lib.gradient_descent_multi as gdm
import lib.normal_equation as ne
import lib.plot_data
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import re
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split


def calculate_starting_business(fieldCountry, sheetname, data_version, iteration):

    columns = list(fieldCountry.columns)
    country = fieldCountry['Economy'].values[0]
    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
        columns.remove(meta_data)

    plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                      '/linear_regression_ind_separated/' + sheetname + '/'

    Path(plots_directory).mkdir(parents=True, exist_ok=True)

    with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
        for index, field in enumerate(columns):

            x = np.asanyarray(
                np.transpose(
                    [[1] * len(fieldCountry["DB.Year"].values),
                     np.stack(fieldCountry["DB.Year"].values)]))

            y = np.asanyarray(fieldCountry[field].values).transpose()

            x, y = removeNaOnX(x, y)

            X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, shuffle=False)

            theta = ne.normal_equation(X_train, y_train)

            x = pd.DataFrame(data=x, columns=["intercept of a polynomial", "year"])["year"]
            X_train = pd.DataFrame(data=X_train, columns=["intercept of a polynomial", "year"])["year"]
            X_test = pd.DataFrame(data=X_test, columns=["intercept of a polynomial", "year"])["year"]

            to_predict_x = [year for year in range(4) + x.max() + 1]
            predicted_y = [theta.item(1) * x + theta.item(0) for x in to_predict_x]

            regex = re.compile(r'Score.*')
            if regex.match(field):
                predicted_y = [100 if score > 100 else score for score in predicted_y]
                predicted_y = [0 if score < 0 else score for score in predicted_y]
            else:
                predicted_y = [0 if score < 0 else score for score in predicted_y]

            plt.figure(figsize=(10, 10))
            plt.scatter(X_train, pd.DataFrame(data=y_train, columns=["values"])["values"], color="blue", marker='x')
            plt.scatter(X_test, pd.DataFrame(data=y_test, columns=["values"])["values"], color="red", marker='x')
            plt.scatter(to_predict_x, predicted_y, color="green", marker='x')
            plt.plot(x, theta.item(1) * x + theta.item(0))
            plt.grid()

            plt.title(country + "-" + sheetname + field)
            plt.xlabel("year")
            plt.ylabel(field)
            plt.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
            plt.xticks(rotation=90)
            plt.legend(['training data', 'test data', 'predicted score', 'regression'])
            pdf.savefig()
            plt.close()

            test_predicted = [theta.item(1) * x + theta.item(0) for x in X_test]
            rmse = mean_squared_error(y_test, test_predicted, squared=False)
            mae = mean_absolute_error(y_test, test_predicted)

            metric_row = pd.DataFrame({
                "algorithm: ": ["linear_regression_ind_separated"],
                "field_of_study": [sheetname],
                "indicator": [field],
                "country": [country],
                "years": [X_train.tolist() + X_test.tolist() + to_predict_x],
                "score": [np.transpose(y_train)[0].tolist() + test_predicted + predicted_y],
                "SmoothL1Loss": ["NA"],
                "MAE": [mae],
                "RMSE": [rmse]})

            # errorDataFrame = errorDataFrame.append(metric_row, ignore_index=True)
            #errorDataFrame = pd.concat([errorDataFrame, pd.DataFrame(metric_row.values(), columns=errorDataFrame.columns)], ignore_index=True)

            errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)


        d = pdf.infodict()
        d['Title'] = 'best business country prediction - linear regression'
        d['Author'] = 'Karol Spórna'
        d['Subject'] = 'Aplying ML in prediction of economic indexes'
        d['Keywords'] = 'PdfPages multipage keywords author title subject'
        d['CreationDate'] = datetime.datetime(2022, 4, 27)
        d['ModDate'] = datetime.datetime.today()

    return errorDataFrame

    ##### Gradient descent
    #  theta = np.matrix(
    #      [[100000], [-2]]
    #  )
    #
    # y = np.matrix(startingBusinessCountry["Time...Men..days."].values).transpose()
    #
    # # alpha = 0.0000001
    # # no_iteration = 6
    # # cost_history = gdm.gradient_descent_multi(x, y, theta, alpha, no_iteration)
    # #
    # # plt.plot(np.arange(no_iteration) + 1, cost_history)
    # # plt.show()
    # #
    # theta = ne.normal_equation(x, y)
    #
    # fig, ax = plt.subplots()
    # ax.scatter(np.stack(startingBusinessCountry["DB.Year"].values), startingBusinessCountry["Time...Men..days."])
    #
    # plt.plot(
    #     np.stack(startingBusinessCountry["DB.Year"].values),
    #     theta.item(1) * np.stack(startingBusinessCountry["DB.Year"].values) + theta.item(0))
    # fig.suptitle(country + "- starting business - Time...Men..days.")
    # plt.show()


def removeNaOnX(x_feature, y_results):
    features = pd.DataFrame(data=x_feature, columns=["intercept of a polynomial", "year"])
    features2 = pd.DataFrame(data=y_results, columns=["result"])
    concatenated_feautures = pd.concat([features, features2], axis=1)

    concatenated_feautures = concatenated_feautures[concatenated_feautures["result"].notna()]

    # x_feature = np.matrix(concatenated_feautures[['intercept of a polynomial', 'year']].values)
    # y_results = np.matrix(concatenated_feautures[['result']].values)

    x_feature = np.asanyarray(concatenated_feautures[['intercept of a polynomial', 'year']].values)
    y_results = np.asanyarray(concatenated_feautures[['result']].values)

    return x_feature, y_results
