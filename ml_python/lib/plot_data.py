import matplotlib.pyplot as plt


def plot_data(x, y):
    fig, ax = plt.subplots()
    ax.scatter(x, y)
