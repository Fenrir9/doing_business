import numpy as np


def compute_cost(x_factors, y_results, theta):
    no_training_examples = len(y_results)

    # Iterative

    # prediction = x_factors * theta
    # diff_sum = 0
    #
    # for training_example in range(no_training_examples):
    #     diff_sum = diff_sum + pow((prediction[training_example] - y_results[training_example]), 2)
    #
    # cost = (1 / (2 * no_training_examples)) * diff_sum

    # Vectorized
    cost = (1 / (2 * no_training_examples)) * sum(np.power(((x_factors * theta) - y_results), 2))
    return cost


# tests

# x = np.matrix(
#     [[1, 2014], [1, 2015], [1, 2016], [1, 2017]]
# )
#
# theta = np.matrix(
#     [[-3948], [2]]
# )
#
# y = np.matrix(
#     [[80], [82], [83], [84]]
# )
#
# compute_cost(x, y, theta)
