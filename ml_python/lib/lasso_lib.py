from lib.lasso_alpha_plot import LassoAlphaPlot
import logging
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as mtp
import numpy as np
import pandas as pd
from pathlib import Path
from sklearn import preprocessing
from sklearn.linear_model import Lasso
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
import re
import sys


def save_plots_for_country_lasso(country, sheetname, columns, fieldData, data_version, iteration):
    errorDataFrame = pd.DataFrame(
        columns=["field_of_study", "indicator", "country", "years", "score", "SmoothL1Loss", "MAE", "RMSE"])

    plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                      '/least_absolute_shrinkage_and_selection_operator/' + sheetname + '/'

    Path(plots_directory).mkdir(parents=True, exist_ok=True)

    with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
        for field in columns:

            x = np.array(
                fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')["DB.Year"].values).reshape(
                (-1, 1))
            y = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[field].values

            X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, shuffle=False)
            to_predict_x = [year for year in range(4) + x.max() + 1]

            scaler = preprocessing.StandardScaler().fit(y.reshape(-1, 1))
            y_train_scalled = scaler.transform(y_train.reshape(-1, 1))
            y_test_scalled = scaler.transform(y_test.reshape(-1, 1))

            lasso_plots = list()

            for alpha in np.arange(0.01, 200, 10):

                all_x = np.array(np.append(x, to_predict_x)).reshape((-1, 1))

                lasso_reg = make_pipeline(StandardScaler(), Lasso(alpha=alpha, max_iter=30000))
                lasso_reg.fit(X_train, y_train_scalled.ravel())

                # test_predict = reg.predict(X_test)
                values = lasso_reg.predict(all_x)
                # rmse = mean_squared_error(y_test, test_predict, squared=True)

                values = scaler.inverse_transform(values.reshape(-1, 1)).ravel()
                y_train = scaler.inverse_transform(y_train_scalled)
                y_test = scaler.inverse_transform(y_test_scalled)

                # values = [value for value in values]



                regex = re.compile(r'Score.*')
                if regex.match(field):
                    values = [100 if value > 100 else value for value in values]
                    values = [0 if value < 0 else value for value in values]
                else:
                    values = [0 if value < 0 else value for value in values]

                rmse = mean_squared_error(y_test, values[-len(to_predict_x)-len(y_test):-len(to_predict_x)], squared=False)
                mae = mean_absolute_error(y_test, values[-len(to_predict_x)-len(y_test):-len(to_predict_x)])

                r2_test = round(lasso_reg.score(X_test, y_test)*100, 2)

                lasso_plots.append(LassoAlphaPlot(alpha, y, r2_test, values, rmse, mae))

            best_lasso_plot = get_best_plot(lasso_plots)

            mtp.scatter(X_train, y_train, color="blue", marker='x')
            mtp.scatter(X_test, best_lasso_plot.get_values()[-len(to_predict_x)-len(y_test):-len(to_predict_x)], color="black", marker='x')
            mtp.scatter(X_test, y_test, color="red", marker='+')
            mtp.scatter(to_predict_x, best_lasso_plot.get_values()[-len(to_predict_x):], color="green", marker='x')


            mtp.xticks(np.arange(min(x), max(to_predict_x) + 1, 1.0))
            mtp.xticks(rotation=90)
            mtp.title(f"{field} alpha: {best_lasso_plot.get_alpha()}")
            mtp.xlabel("Year")
            mtp.ylabel("Value")
            mtp.legend(['training data', 'approx of test data', 'test data', 'predicted score'])
            mtp.grid()
            # mtp.show()
            pdf.savefig()
            mtp.close()

            metric_row = pd.DataFrame({
                "algorithm: ": ["lasso"],
                "field_of_study": [sheetname],
                "indicator": [field],
                "country": [country],
                "years": [np.transpose(X_train)[0].tolist() + np.transpose(X_test)[0].tolist() + to_predict_x],
                "score": [np.transpose(y_train).tolist() + best_lasso_plot.get_values()[-len(to_predict_x) - len(y_test): -len(to_predict_x)] + best_lasso_plot.get_values()[-len(to_predict_x):]],
                "SmoothL1Loss": ["NA"],
                "MAE": [best_lasso_plot.get_mae()],
                "RMSE": [best_lasso_plot.get_rmse()]})

            errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

    return errorDataFrame


def get_best_plot(lasso_plots, indicator='rmse'):
    error = sys.float_info.max
    for polynomial_plot in lasso_plots:
        if indicator == 'rmse':
            if error > polynomial_plot.get_rmse():
                error = polynomial_plot.get_rmse()
        elif indicator == 'mae':
            if error > polynomial_plot.get_mae():
                error = polynomial_plot.get_mae()
        else:
            logging.exception("undefined error indicator")
    return identify_plot(lasso_plots, error, indicator)


def identify_plot(lasso_plots, error, indicator):
    for lasso_plot in lasso_plots:
        if indicator == 'rmse':
            if lasso_plot.get_rmse() == error:
                return lasso_plot
        elif indicator == 'mae':
            if lasso_plot.get_mae() == error:
                return lasso_plot
        else:
            logging.exception("undefined error indicator")
