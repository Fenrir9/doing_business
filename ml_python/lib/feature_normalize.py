import numpy as np


def feature_normalize(x_feature):
    mean = np.mean(x_feature)
    sigma = np.std(x_feature)
    x_normalized = (x_feature - mean) / sigma

    return x_normalized
