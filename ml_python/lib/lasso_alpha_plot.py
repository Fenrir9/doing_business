class LassoAlphaPlot:
    def __init__(self, alpha, regression_value, r2_score, values, rmse, mae):
        self.alpha = alpha
        self.regression_value = regression_value
        self.r2_score = r2_score
        self.values = values
        self.rmse = rmse
        self.mae = mae

    def get_alpha(self):
        return self.alpha

    def get_regression_value(self):
        return self.regression_value

    def get_r2_score(self):
        return self.r2_score

    def get_values(self):
        return self.values

    def get_rmse(self):
        return self.rmse

    def get_mae(self):
        return self.mae
