import logging
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as mtp
from neuralprophet import NeuralProphet, set_log_level
import numpy as np
from openpyxl import load_workbook
import pandas as pd
from pathlib import Path
import re
# set_log_level("ERROR")


def run_neural_prophet_auto_regression(data_version, iteration):
    if data_version == "first_version":
        sheet_file_path = '../data/all_data_common_denominator/' + data_version + '.xlsx'
        metrics_file_path = '../data/all_data_common_denominator/neural_prophet_auto_regression/'
        # metrics_file_path = '../../../temp_data/plots/all_data_common_denominator/neural_prophet_auto_regression/'
    elif data_version == "second_version":
        sheet_file_path = '../data/new_methodology/' + data_version + '.xlsx'
        metrics_file_path = '../data/new_methodology/neural_prophet_auto_regression/'
    else:
        logging.exception("Invalid data_version")
        quit()

    number_of_forecast = 4
    sheetnames = load_workbook(sheet_file_path, read_only=True, keep_links=False).sheetnames
    #sheetnames = load_workbook('../data/all_data_common_denominator/first_version.xlsx', read_only=True, keep_links=False)\
     #   .sheetnames

    for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
        sheetnames.remove(sheetname)

    errorDataFrame = pd.DataFrame(columns=["field_of_study", "indicator", "country", "SmoothL1Loss", "MAE", "RMSE"])

    for sheetname in sheetnames: #["payingTaxesData"]:#
        fieldData = pd.read_excel(sheet_file_path, sheet_name=sheetname)
        print(sheetname)

        countries = {country for country in fieldData['Economy'].values}

        columns = list(fieldData.columns)
        for meta_data in ['Country.code', 'Economy', 'Region', 'Income.group', 'DB.Year']:
            columns.remove(meta_data)

        for country in sorted(countries): #["Poland", "Zimbabwe"]: #["Belarus"]:#

            plots_directory = '../data/plots/' + data_version + '/iteration' + str(iteration) + \
                              '/neural_prophet_auto_regression/' + sheetname + '/'

            Path(plots_directory).mkdir(parents=True, exist_ok=True)

            with PdfPages(plots_directory + country + '_' + sheetname + '.pdf') as pdf:
                for field in columns: #["Score.Paying.Taxes."]:#

                #fieldData = pd.read_excel('../data/all_data_common_denominator/first_version.xlsx', sheet_name="startingBusinessData")

                    sf_load_df = fieldData.loc[fieldData['Economy'] == country].sort_values(by='DB.Year')[["DB.Year", field]]

                    if len(sf_load_df) < 5:
                        break

                    sf_load_df = sf_load_df.rename(columns={"DB.Year": "ds"})
                    sf_load_df = sf_load_df.rename(columns={field: "y"})
                    #sf_load_df = fieldData


                    sf_load_df["ds"] = pd.to_datetime(sf_load_df["ds"].astype("int").astype('str'), format='%Y')
                    sf_load_df["ds"] = sf_load_df["ds"].apply(lambda dt: dt.replace(month=12, day=31))

                    sf_load_df.head(3)

                    m = NeuralProphet(
                        n_forecasts=number_of_forecast,
                        growth='off',
                        yearly_seasonality=False,
                        weekly_seasonality=False,
                        daily_seasonality=False,
                        n_lags=3,
                        ar_reg=40,
                        normalize="standardize",
                        learning_rate=0.0001,
                        epochs=10000
                    )

                    df_train, df_test = m.split_df(sf_load_df, freq='Y') #, valid_p = 1.0/12)

                    # df_train = sf_load_df.sample(frac=0.8).sort_values(by="ds")
                    # df_test = sf_load_df.drop(df_train.index).sort_values(by="ds")

                    if len(df_train["y"].unique()) == 1:
                        # print(sf_load_df["y"].iloc[0])
                        # print(sf_load_df["y"].max())
                        # to_predict_x = [2022, 2023, 2024]
                        # predicted_y = [sf_load_df["y"].iloc[0]] * 3
                        #
                        # mtp.scatter(to_predict_x, predicted_y, color="blue")

                        forecat_timestamps = [
                            pd.Timestamp(min(sf_load_df["ds"]))
                            + year * pd.tseries.frequencies.to_offset('Y') for year in range(len(sf_load_df) + number_of_forecast)]
                        data = {"ds": forecat_timestamps, "y": [sf_load_df["y"].iloc[0]] * (len(sf_load_df) + number_of_forecast)}
                        all_data = pd.DataFrame(data=data)

                        regex = re.compile(r'Score.*')
                        if regex.match(field):
                            all_data.loc[all_data['y'] > 100, 'y'] = 100
                            all_data.loc[all_data['y'] < 0, 'y'] = 0
                        else:
                            all_data.loc[all_data['y'] < 0, 'y'] = 0

                        #all_data = pd.concat([sf_load_df, forecast], ignore_index=True)
                        metric_row = pd.DataFrame({
                            "algorithm: ": ["neural_prophet_auto_regression"],
                            "field_of_study": [sheetname],
                            "indicator": [field],
                            "country": [country],
                            "years": [[x.year for x in all_data["ds"].tolist()]],
                            "score": [all_data["y"].tolist()],
                            "SmoothL1Loss": ["NA"],
                            "MAE": ["NA"],
                            "RMSE": ["NA"]})

                        errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)
                    else:
                        # metrics = m.fit(df_train, freq='Y', validation_df=df_test, progress='plot')
                        metrics = m.fit(df_train, freq='Y', validation_df=df_test, progress='bar')

                        metrics.tail(1)
                        forecast = m.predict(df_train)
                        # fig = m.plot(forecast)
                        #
                        # mtp.show()
    #
                        test_forecast = m.predict(df_test)
                        m = m.highlight_nth_step_ahead_of_each_forecast(4)
                        # fig = m.plot(forecast[-7*24:])
                        # fig = m.plot(forecast)
                        # mtp.show()


                        # fig_param = m.plot_parameters()
                        # mtp.show()

                        # future = df_test
                        # for prediction_number in reversed(range(1, number_of_forecast + 1)):
                        #     future = m.make_future_dataframe(future, periods=number_of_forecast, n_historic_predictions=len(sf_load_df))
                        #     forecast = m.predict(future)
                        #     a = future["y"].iloc[-prediction_number]
                        #     b = forecast["yhat1"].iloc[-prediction_number]
                        #     future["y"].iloc[-prediction_number] = forecast["yhat1"].iloc[-prediction_number]

                        future = m.make_future_dataframe(df_test, periods=number_of_forecast, n_historic_predictions=len(sf_load_df))
                        forecast = m.predict(future)


                        #fig = mtp.scatter(sf_load_df["ds"], sf_load_df["y"], color="blue")

                        # forecast["ds"] = forecast["ds"] - pd.DateOffset(years=1)
                        #
                        # fig, ax = mtp.subplots(figsize=(10, 10))
                        # m.plot(forecast, xlabel="Year", ylabel="Value", ax=ax)
                        # ax.set_title(f"{field}")
                        # #mtp.show()
                        # #print(future)
                        #sf_load_df = sf_load_df.rename(columns={'y': 'yhat4'})

                        all_data = pd.concat([df_train, test_forecast[['ds', 'yhat1']].iloc[-number_of_forecast:, :], forecast[['ds', 'yhat4']].iloc[-number_of_forecast:, :]], ignore_index=True)
                        all_data.loc[all_data['y'].isnull(), 'y'] = all_data['yhat1']
                        all_data.loc[all_data['y'].isnull(), 'y'] = all_data['yhat4']
                        # all_data.y = all_data.fillna(all_data.yhat1, inplace=True)
                        # all_data.y = all_data.fillna(all_data.yhat4, inplace=True)
                        #all_data = all_data.rename(columns={'yhat4': 'y'})

                        met_test = m.test(df=df_test)

                        regex = re.compile(r'Score.*')
                        if regex.match(field):
                            all_data.loc[all_data['y'] > 100, 'y'] = 100
                            all_data.loc[all_data['y'] < 0, 'y'] = 0
                        else:
                            all_data.loc[all_data['y'] < 0, 'y'] = 0

                        metric_row = pd.DataFrame({
                            "algorithm: ": ["neural_prophet_auto_regression"],
                            "field_of_study": [sheetname],
                            "indicator": [field],
                            "country": [country],
                            "years": [[x.year for x in all_data["ds"].tolist()]],
                            "score": [all_data["y"].tolist()],
                            "SmoothL1Loss": [met_test.iloc[0]['SmoothL1Loss']],
                            "MAE": [met_test.iloc[0]['MAE']],
                            "RMSE": [met_test.iloc[0]['RMSE']]})

                        errorDataFrame = pd.concat([errorDataFrame, metric_row], ignore_index=True, axis=0)

                    all_data["y"] = all_data["y"].clip(lower=0)

                    mtp.scatter(
                        [x.year for x in all_data["ds"].tolist()][:len(df_train)],
                        all_data["y"].tolist()[:len(df_train)],
                        color="blue",
                        marker='x')
                    mtp.scatter(
                        [x.year for x in all_data["ds"].tolist()][len(df_train): - number_of_forecast],
                        all_data["y"].tolist()[len(df_train): - number_of_forecast],
                        color="black",
                        marker='x')
                    mtp.scatter(
                        [x.year for x in df_test["ds"].tolist()][-(len(all_data) - len(df_train) - number_of_forecast):],
                        df_test["y"].tolist()[-(len(all_data) - len(df_train) - number_of_forecast):],
                        color="red",
                        marker='+')
                    mtp.scatter(
                        [x.year for x in all_data["ds"].tolist()][-number_of_forecast:],
                        all_data["y"].tolist()[- number_of_forecast:],
                        color="green",
                        marker='x')

                    a = np.arange(int(min(all_data["ds"]).year), int(max(all_data["ds"]).year) + 1, 1)
                    mtp.xticks(np.arange(int(min(all_data["ds"]).year), int(max(all_data["ds"]).year) + 1, 1))
                    mtp.xticks(rotation=90)
                    mtp.title(f"{field}")
                    mtp.xlabel("Year")
                    mtp.ylabel("Value")
                    mtp.legend(['training data', 'approx of test data', 'test data', 'predicted', 'regression'])
                    mtp.grid()

                    pdf.savefig()
                    mtp.close()

    Path(metrics_file_path).mkdir(parents=True, exist_ok=True)
    errorDataFrame.to_excel(metrics_file_path + 'metrics' + str(iteration) + '.xlsx', index=False)
    # errorDataFrame.to_excel("../data/plots/neural_prophet_auto_regression/metrics.xlsx", index=False)
