source("lib/constants/constant_data16.R")

handleScores16 <- function(gruppedDataByField)
{
  gruppedDataByField@startingBusinessData <- handleScoreStartingBusiness16(gruppedDataByField@startingBusinessData);
  gruppedDataByField@dealingWithConstructionPermitsData <- handleScoreDealingWithConstructionPermits16(gruppedDataByField@dealingWithConstructionPermitsData);
  gruppedDataByField@registeringPropertyData <- handleScoreRegisteringProperty16(gruppedDataByField@registeringPropertyData);
  gruppedDataByField@gettingElectricityData <- handleScoreGettingElectricity16(gruppedDataByField@gettingElectricityData);
  gruppedDataByField@gettingCreditData <- handleScoreGettingCredit16(gruppedDataByField@gettingCreditData);
  gruppedDataByField@protectingMiniorityInvestorsData <- handleScoreProtectingMinorityInvestors16(gruppedDataByField@protectingMiniorityInvestorsData);
  gruppedDataByField@payingTaxesData <- handleScorePayingTaxes16(gruppedDataByField@payingTaxesData);
  gruppedDataByField@tradingAcrossBordersData <- handleTradingAcrossBorders16(gruppedDataByField@tradingAcrossBordersData);
  gruppedDataByField@enforcingContractsData <- handleScoreEnforcingContracts16(gruppedDataByField@enforcingContractsData);
  gruppedDataByField@resolvingInsolvencyData <- handleScoreResolvingInsolvencyData16(gruppedDataByField@resolvingInsolvencyData);
  gruppedDataByField@easeOfDoingBusinessData <- handleScoreEaseofDoingBussinesData16(gruppedDataByField);
  
  return(gruppedDataByField)
}

handleScoreStartingBusiness16 <- function(startingBusiness)
{
  startingBusiness <- data.frame(startingBusiness);
  
  startingBusiness <- assignIndicatorsScoresStartingBusiness16(startingBusiness);
  
  startingBusiness["Score.Starting.a.business"] = 
    data.frame(rowMeans(data.frame(
      rowMeans(
        startingBusiness[
          , 
          c("Score.Procedures...Men..number.",
            "Score.Procedures...Women..number.")]),
      rowMeans(
        startingBusiness[
          , 
          c("Score.Time...Men..days.",
            "Score.Time...Women..days.")]),
      rowMeans(
        startingBusiness[
          , 
          c("Score.Cost...Men....of.income.per.capita.",
            "Score.Cost...Women....of.income.per.capita.")]),
      startingBusiness[
        ,
        c("Score.Paid.in.Minimum.capital....of.income.per.capita.")]
      )));
  
  return(startingBusiness)
}

handleScoreDealingWithConstructionPermits16 <- function(dealingWithConstructionPermits)
{
  dealingWithConstructionPermits <- data.frame(dealingWithConstructionPermits);
  
  dealingWithConstructionPermits <- 
    assignIndicatorsDealingWithConstructionPermits16(dealingWithConstructionPermits);
  
  dealingWithConstructionPermits["Score.Dealing.with.construction.permits."] = 
    rowMeans(dealingWithConstructionPermits[
      ,
      c("Score.Procedures..number....Dealing.with.construction.permits",
        "Score.Time..days....Dealing.with.construction.permits",
        "Score.Cost....of.Warehouse.value.",
        "Score.Building.quality.control.index..0.15...DB16.20.methodology.")
      ]);

  return(dealingWithConstructionPermits)
}

handleScoreRegisteringProperty16 <- function(registeringProperty)
{
  registeringProperty <- data.frame(registeringProperty);
  
  registeringProperty <- assignIndicatorsRegisteringProperty16(registeringProperty);
  
  registeringProperty["Score.Registering.property."] = 
    rowMeans(registeringProperty[
      ,
      c("Score.Procedures..number....Registering.property",
        "Score.Time..days....Registering.property",
        "Score.Cost....of.property.value.",
        "Score.Quality.of.land.administration.index..0.30...DB17.20.methodology.")
    ]);

  return(registeringProperty)
}

handleScoreGettingElectricity16 <- function(gettingElectricity)
{
  gettingElectricity <- data.frame(gettingElectricity);
  
  gettingElectricity <- assignIndicatorsGettingElectricity16(gettingElectricity);
  
  gettingElectricity["Score.Getting.electricity."] = 
    rowMeans(gettingElectricity[
      ,
      c("Score.Procedures..number....Getting.electricity",
        "Score.Time..days....Getting.electricity",
        "Score.Cost....of.income.per.capita.",
        "Score.Reliability.of.supply.and.transparency.of.tariff.index..0.8...DB16.20.methodology.")
      ]);

  return(gettingElectricity)
}

handleScoreGettingCredit16 <- function(gettingCredit)
{
  gettingCredit <- data.frame(gettingCredit);
  
  gettingCredit <- assignIndicatorsGettingCredit16(gettingCredit);
  
  gettingCredit["Score.Getting.credit."] = 
    rowMeans(gettingCredit[
      ,
      c("Score.Strength.of.legal.rights.index..0.12...DB15.20.methodology.",
        "Score.Depth.of.credit.information.index..0.8...DB15.20.methodology.")
    ]);

  return(gettingCredit)
}

handleScoreProtectingMinorityInvestors16 <- function(protectingMinorityInvestors)
{
  protectingMinorityInvestors <- data.frame(protectingMinorityInvestors);
  
  protectingMinorityInvestors <-
    assignIndicatorsProtectingMinorityInvestors16(protectingMinorityInvestors);
  
  protectingMinorityInvestors["Score.Protecting.minority.investors..DB15.20.methodology."] = 
    rowMeans(protectingMinorityInvestors[
      ,
      c("Score.Extent.of.disclosure.index..0.10.",
        "Score.Extent.of.director.liability.index..0.10.",
        "Score.Ease.of.shareholder.suits.index..0.10...DB15.20.methodology.",
        "Score.Extent.of.shareholder.rights.index..0.6...DB15.20.methodology.",
        "Score.Extent.of.ownership.and.control.index..0.7...DB15.20.methodology.",
        "Score.Extent.of.corporate.transparency.index..0.7...DB15.20.methodology.",
        "Score.Strength.of.minority.investor.protection.index..0.50...DB15.20.methodology.")
    ]);

  return(protectingMinorityInvestors)
}

handleScorePayingTaxes16 <- function(payingTaxes)
{
  payingTaxes <- data.frame(payingTaxes);
  
  payingTaxes <- assignIndicatorsPayingTaxes16(payingTaxes);
  
  payingTaxes["Score.Postfiling.index..0.100...DB17.20.methodology."] = 
    rowMeans(payingTaxes[
      ,
      c(
        "Score.Time.to.comply.with.VAT.refund..hours...DB17.20.methodology.",
        "Score.Time.to.obtain.VAT.refund..weeks...DB17.20.methodology.",
        "Score.Time.to.comply.with.a.corporate.income.tax.correction..hours...DB17.20.methodology.",
        "Score.Time.to.complete.a.corporate.income.tax.correction..weeks...DB17.20.methodology."
      )
    ])
  
  payingTaxes["Score.Paying.Taxes."] = 
    rowMeans(payingTaxes[
      ,
      c("Score.Paying.taxes..DB17.20.methodology.",
        "Score.Time..hours.per.year.",
        "Score.Total.tax.and.contribution.rate....of.profit.",
        "Score.Postfiling.index..0.100...DB17.20.methodology.")
    ])
  
  return(payingTaxes)
}

handleTradingAcrossBorders16 <- function(tradingAcrossBorders)
{
  tradingAcrossBorders <- data.frame(tradingAcrossBorders);
  
  tradingAcrossBorders <- assignIndicatorsTradingAcrossBorders16(tradingAcrossBorders);
   
  tradingAcrossBorders["Score.Trading.across.borders..DB16.20.methodology."] =
    rowMeans(tradingAcrossBorders[
      ,
      c("Score.Time.to.export..Documentary.compliance..hours...DB16.20.methodology.",
        "Score.Time.to.import..Documentary.compliance..hours...DB16.20.methodology.",
        "Score.Time.to.export..Border.compliance..hours...DB16.20.methodology.",
        "Score.Time.to.import..Border.compliance..hours...DB16.20.methodology.",
        "Score.Cost.to.export..Documentary.compliance..USD...DB16.20.methodology.",
        "Score.Cost.to.import..Documentary.compliance..USD...DB16.20.methodology.",
        "Score.Cost.to.export..Border.compliance..USD...DB16.20.methodology.",
        "Score.Cost.to.import..Border.compliance..USD...DB16.20.methodology.")
    ])
  
  return(tradingAcrossBorders)
}

handleScoreEnforcingContracts16 <- function(enforcingContracts)
{
  enforcingContracts <- data.frame(enforcingContracts);
  
  enforcingContracts <- assignIndicatorsEnforcingContracts16(enforcingContracts);
  
  enforcingContracts["Score.Enforcing.contracts."] =
    rowMeans(enforcingContracts[
      ,
      c("Score.Time..days....Enforcing.contracts",
        "Score.Cost....of.claim.",
        "Score.Quality.of.judicial.processes.index..0.18...DB17.20.methodology.")
    ])

  return(enforcingContracts)
}

handleScoreResolvingInsolvencyData16 <- function(resolvingInsolvencyData)
{
  resolvingInsolvencyData <- data.frame(resolvingInsolvencyData);
  
  resolvingInsolvencyData <- assignIndicatorsResolvingInsolvency16(resolvingInsolvencyData);

  resolvingInsolvencyData["Score.Resolving.insolvency"] = 
    rowMeans(resolvingInsolvencyData[
      ,
      c("Score.Recovery.rate..cents.on.the.dollar.",
        "Score.Strength.of.insolvency.framework.index..0.16.")
    ])
  
  return(resolvingInsolvencyData)
}

handleScoreEaseofDoingBussinesData16 <- function(gruppedDataByField)
{
  easeofDoingBussines <- data.frame(gruppedDataByField@easeOfDoingBusinessData)
  
  dfScoreList <- createDfScoreList16(gruppedDataByField);
  
  easeofDoingBussines <- Reduce(
    function(x, y) merge(
      x, 
      y, 
      by.x=c("Economy", "DB.Year"), 
      by.y=c("Economy", "DB.Year")), 
    dfScoreList, accumulate = FALSE)
  
  
  easeofDoingBussines["Ease.of.doing.business.score."] <-
    rowMeans(
      easeofDoingBussines[
        , 
        c(
          "Score.Starting.a.business",
          "Score.Dealing.with.construction.permits..DB16.20.methodology.",
          "Score.Getting.electricity..DB16.20.methodology.",
          "Score.Registering.property..DB17.20.methodology.",
          "Score.Getting.credit..DB15.20.methodology.",
          "Score.Protecting.minority.investors..DB15.20.methodology.",
          "Score.Paying.taxes..DB17.20.methodology.",
          "Score.Trading.across.borders..DB16.20.methodology.",
          "Score.Enforcing.contracts..DB17.20.methodology.",
          "Score.Resolving.insolvency"
        )])
  
  return(easeofDoingBussines)
}

assignIndicatorsScoresStartingBusiness16 <- function(startingBusiness)
{
  for (name in names(scoresDictionaryStartingBusiness16))
  {
    startingBusiness[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          startingBusiness, 
          scoresDictionaryStartingBusiness16,
          name), 
        lowerLimitStartingBusiness16, 
        upperLimitStartingBusiness16)
  }
  
  return(startingBusiness)
}

assignIndicatorsDealingWithConstructionPermits16 <- function(dealingWithConstructionPermits)
{
  for (name in names(scoresDictionaryDealingWithConstructionPermits16))
  {
    dealingWithConstructionPermits[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          dealingWithConstructionPermits, 
          scoresDictionaryDealingWithConstructionPermits16,
          name), 
        lowerLimitDealingWithConstructionPermits16, 
        upperLimitDealingWithConstructionPermits16)
  }
  
  return(dealingWithConstructionPermits)
}

assignIndicatorsGettingElectricity16 <- function(gettingElectricity)
{
  for (name in names(scoresDictionaryGettingElectricity16))
  {
    gettingElectricity[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          gettingElectricity, 
          scoresDictionaryGettingElectricity16,
          name), 
        lowerLimitGettingElectricity16, 
        upperLimitGettingElectricity16)
  }
  
  return(gettingElectricity)
}

assignIndicatorsRegisteringProperty16 <- function(registeringProperty)
{
  for (name in names(scoresDictionaryRegisteringProperty16))
  {
    registeringProperty[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          registeringProperty, 
          scoresDictionaryRegisteringProperty16,
          name), 
        lowerLimitRegisteringProperty16, 
        upperLimitRegisteringProperty16)
  }
  
  return(registeringProperty)
}

assignIndicatorsGettingCredit16 <- function(gettingCredit)
{
  for (name in names(scoresDictionaryGettingCredit16))
  {
    gettingCredit[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          gettingCredit, 
          scoresDictionaryGettingCredit16,
          name), 
        lowerLimitGettingCredit16, 
        upperLimitGettingCredit16)
  }
  
  return(gettingCredit)
}

assignIndicatorsProtectingMinorityInvestors16 <- function(protectingMinorityInvestors)
{
  for (name in names(scoresDictionaryProtectingMinorityInvestors16))
  {
    protectingMinorityInvestors[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          protectingMinorityInvestors, 
          scoresDictionaryProtectingMinorityInvestors16,
          name), 
        lowerLimitProtectingMinorityInvestors16, 
        upperLimitProtectingMinorityInvestors16)
  }
  
  return(protectingMinorityInvestors)
}

assignIndicatorsPayingTaxes16 <- function(payingTaxes)
{
  for (name in names(scoresDictionaryPayingTaxes16))
  {
    payingTaxes[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          payingTaxes, 
          scoresDictionaryPayingTaxes16,
          name),
        lowerLimitPayingTaxes16,
        upperLimitPayingTaxes16)
  }
  
  return(payingTaxes)
}

assignIndicatorsTradingAcrossBorders16 <- function(tradingAcrossBorders)
{
  for (name in names(scoresDictionaryTradingAcrossBorders16))
  {
    tradingAcrossBorders[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          tradingAcrossBorders, 
          scoresDictionaryTradingAcrossBorders16,
          name),
        lowerLimitTradingAcrossBorders16,
        upperLimitTradingAcrossBorders16)
  }
  
  return(tradingAcrossBorders)
}

assignIndicatorsEnforcingContracts16 <- function(enforcingContracts)
{
  for (name in names(scoresDictionaryEnforcingContracts16))
  {
    enforcingContracts[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          enforcingContracts, 
          scoresDictionaryEnforcingContracts16,
          name),
        lowerLimitEnforcingContracts16,
        upperLimitEnforcingContracts16)
  }
  
  return(enforcingContracts)
}

assignIndicatorsResolvingInsolvency16 <- function(resolvingInsolvency)
{
  
  for (name in names(scoresDictionaryResolvingInsolvency16))
  {
    resolvingInsolvency[, name] <- 
      constrainIndicatorsScores16(
        calculateIndicatorsWithConstant16(
          resolvingInsolvency, 
          scoresDictionaryResolvingInsolvency16,
          name),
        lowerLimitResolvingInsolvency16,
        upperLimitResolvingInsolvency16)
  }
  
  return(resolvingInsolvency)
}

createDfScoreList16 <- function(gruppedDataByField)
{
  dfScoreList <- list(
    gruppedDataByField@easeOfDoingBusinessData, 
    select(
      gruppedDataByField@startingBusinessData,
      c(
        'Score.Starting.a.business',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@dealingWithConstructionPermitsData,
      c(
        'Score.Dealing.with.construction.permits..DB16.20.methodology.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@gettingElectricityData,
      c(
        'Score.Getting.electricity..DB16.20.methodology.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@registeringPropertyData,
      c(
        'Score.Registering.property..DB17.20.methodology.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@gettingCreditData,
      c(
        'Score.Getting.credit..DB15.20.methodology.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@protectingMiniorityInvestorsData,
      c(
        'Score.Protecting.minority.investors..DB15.20.methodology.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@payingTaxesData,
      c(
        'Score.Paying.taxes..DB17.20.methodology.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@tradingAcrossBordersData,
      c(
        'Score.Trading.across.borders..DB16.20.methodology.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@enforcingContractsData,
      c(
        'Score.Enforcing.contracts..DB17.20.methodology.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@resolvingInsolvencyData,
      c(
        'Score.Resolving.insolvency',
        'Economy',
        'DB.Year')
    )
  )
  
  return(dfScoreList)
}

constrainIndicatorsScores16 <- function(calulatedScore, lowerLimit, upperLimit)
{
  return(pmin(upperLimit, pmax(lowerLimit, calulatedScore)))
}

calculateIndicatorsWithConstant16 <- function(field, indicators, name)
{
  result <-
    upperLimitStartingBusiness16 -
    as.numeric(indicators[[name]][2]) *
    (field[
      ,
      indicators[[name]][1]] - as.numeric(indicators[[name]][3]))
  
  return(result)
}

calculateIndicatorsWithoutConstant16 <- function(field, indicators, name)
{
  result <- as.numeric(indicators[[name]][2]) * field[, indicators[[name]][1]]
  
  return(result)
}