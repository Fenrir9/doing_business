economiesToRemoveStartingBussines16 <- c(
  "Liechtenstein"
)


economiesToRemoveDealingWithConstructionPermits16 <- c(
  "Albania",
  "Eritrea",
  "Libya",
  "Liechtenstein",
  "Somalia",
  "Syrian Arab Republic",
  "Yemen, Rep."
)

economiesToRemovegettingElectricity16 <- c(
  "Afghanistan",
  # "Albania",
  # "Angola",
  # "Bahamas, The",
  # "Bangladesh",
  # "Bangladesh Chittagong",
  # "Bangladesh Dhaka",
  # "Benin",
  # "Botswana",
  # "Burkina Faso",
  # "Burundi",
  # "Cameroon",
  # "Central African Republic",
  # "Chad",
  # "Comoros",
  # "Congo, Dem. Rep.",
  # "Congo, Rep.",
  # "Djibouti",
  # "Equatorial Guinea",
  "Eritrea",
  # "Eswatini",
  # "Ethiopia",
  # "Gabon",
  # "Gambia, The",
  # "Ghana",
  # "Guinea",
  # "Guinea-Bissau",
  # "Haiti",
  # "Honduras",
  # "Iraq",
  # "Kiribati",
  # "Kyrgyz Republic",
  # "Lao PDR",
  # "Lebanon",
  # "Lesotho",
  "Liechtenstein",
  # "Madagascar",
  # "Malawi",
  # "Maldives",
  # "Mali",
  # "Marshall Islands",
  # "Mauritania",
  # "Micronesia, Fed. Sts.",
  # "Montenegro",
  # "Mozambique",
  # "Myanmar",
  # "Nepal",
  # "Nigeria",
  # "Nigeria Lagos",
  # "Pakistan",
  # "Pakistan Karachi",
  # "Pakistan Lahore",
  # "Rwanda",
  # "S\U00E3o Tom\U00E9 and Pr\U00EDncipe",
  # "Senegal",
  # "Sierra Leone",
  "Somalia",
  "South Sudan",
  # "St. Vincent and the Grenadines",
  # "Suriname",
  # "Syrian Arab Republic",
  # "Tajikistan",
  # "Timor-Leste",
  # "Togo",
  # "Venezuela, RB",
  "Yemen, Rep."
  #"Zimbabwe"
)

economiesToRemoveGettingElectricity16_2015 <- c(
  "Afghanistan",
  # "Albania",
  # "Angola",
  # "Bahamas, The",
  # "Bangladesh",
  # "Bangladesh Chittagong",
  # "Bangladesh Dhaka",
  # "Benin",
  # "Botswana",
  # "Burkina Faso",
  # "Burundi",
  # "Cameroon",
  # "Central African Republic",
  # "Chad",
  # "Comoros",
  # "Congo, Dem. Rep.",
  # "Congo, Rep.",
  # "Djibouti",
  # "Egypt, Arab Rep.",
  # "Equatorial Guinea",
  "Eritrea",
  # "Eswatini",
  # "Ethiopia",
  # "Gabon",
  # "Gambia, The",
  # "Ghana",
  # "Guinea",
  # "Guinea-Bissau",
  # "Haiti",
  # "Honduras",
  # "Iraq",
  # "Kiribati",
  # "Kyrgyz Republic",
  # "Lao PDR",
  # "Lebanon",
  # "Lesotho",
  "Liechtenstein",
  # "Madagascar",
  # "Malawi",
  # "Maldives",
  # "Mali",
  # "Marshall Islands",
  # "Mauritania",
  # "Micronesia, Fed. Sts.",
  # "Montenegro",
  # "Mozambique",
  # "Myanmar",
  # "Nepal",
  # "Nigeria",
  # "Nigeria Lagos",
  # "Oman",
  # "Pakistan",
  # "Pakistan Karachi",
  # "Pakistan Lahore",
  # "Rwanda",
  # "S\U00E3o Tom\U00E9 and Pr\U00EDncipe",
  # "Senegal",
  # "Sierra Leone",
  "Somalia",
  # "South Africa",
  "South Sudan",
  # "St. Kitts and Nevis",
  # "St. Vincent and the Grenadines",
  # "Suriname",
  # "Syrian Arab Republic",
  # "Tajikistan",
  # "Timor-Leste",
  # "Togo",
  # "Venezuela, RB",
  "Yemen, Rep."
  # "Zimbabwe"
)

economiesToRemoveRegisteringProperty16 <- c(
  "Libya",
  "Liechtenstein",
  "Marshall Islands",
  "Micronesia, Fed. Sts.",
  "Timor-Leste"
)

economiesToRemoveGettingCredit16 <- c(
  "Liechtenstein",
  "Somalia"
)

economiesToRemoveProtectingMiniorityInvestors16 <- c(
  "Liechtenstein"
)

economiesNoVAT16 <- c(
  "Afghanistan",
  "Angola",
  "Bahamas, The",
  "Bahrain",
  "Bhutan",
  "Brunei Darussalam",
  "Equatorial Guinea",
  "Eritrea",
  "Comoros",
  "Hong Kong SAR, China",
  "Iraq",
  "Kenya",
  "Kuwait",
  "Liberia",
  "Libya",
  "Malaysia",
  "Marshall Islands",
  "Micronesia, Fed. Sts.",
  "Morocco",
  "Oman",
  "Palau",
  "Puerto Rico",
  "Qatar",
  "San Marino",
  "S\U00E3o Tom\U00E9 and Pr\U00EDncipe",
  "Saudi Arabia",
  "Sierra Leone",
  "Solomon Islands",
  "South Sudan",
  "Syrian Arab Republic",
  "Timor-Leste",
  "Turkey", # <- 2020
  "United Arab Emirates",
  "United States",
  "United States Los Angeles",
  "United States New York City"
)

economiesNoVATRefund16 <- c(
  "Algeria",
  "Argentina",
  "Armenia",
  "Belarus",
  "Benin",
  "Bolivia",
  "Brazil",
  "Brazil Rio de Janeiro",
  "Brazil S\U00E3o Paulo",
  "Burkina Faso",
  "Burundi",
  "Cameroon",
  "Central African Republic",
  "Chad",
  "China",
  "China Beijing",
  "China Shanghai",
  "Colombia",
  "Comoros",
  "Congo, Dem. Rep.",
  "Congo, Rep.",
  "Djibouti",
  "Dominican Republic",
  "Ecuador",
  "Egypt, Arab Rep.",
  "El Salvador",
  "Gambia, The",
  "Ghana",
  "Grenada",
  "Guatemala",
  "Guinea",
  "Guinea-Bissau",
  "Haiti",
  "India",
  "India Delhi",
  "India Mumbai",
  "Kazakhstan",
  "Kyrgyz Republic",
  "Lao PDR",
  "Madagascar",
  "Maldives",
  "Mali",
  "Mauritania",
  "Mongolia",
  "Myanmar",
  "Niger",
  "Nigeria",
  "Nigeria Kano",
  "Nigeria Lagos",
  "Panama",
  "Paraguay",
  "Peru",
  "Philippines",
  "Sri Lanka",
  "Sudan",
  "Suriname",
  "Tajikistan",
  "Togo",
  "Turkey", #<- 2016=2019
  "Uruguay",
  "Uzbekistan",
  "Venezuela, RB",
  "Vietnam"
)

economiesNoCorporateIncomeTax <- c(
  "Bahamas, The",
  "Bahrain",
  "Kuwait",
  "Marshall Islands",
  "Micronesia, Fed. Sts.",
  "Palau",
  "Qatar",
  "United Arab Emirates",
  "Vanuatu"
)

ecoomiesToRemovePayingTaxes16 <- c(
  # "Bahamas, The",
  # "Bahrain",
  # "Brunei Darussalam",
  # "Comoros",
  # "Equatorial Guinea",
  # "Eritrea",
  # "Iraq",
  # "Kenya",
  # "Kuwait",
  # "Kyrgyz Republic",
  # "Lao PDR",
  # "Liberia",
  # "Libya",
  "Liechtenstein",
  # "Malaysia",
  # "Maldives",
  # "Mali",
  # "Marshall Islands",
  # "Mauritania",
  # "Micronesia, Fed. Sts.",
  # "Morocco",
  # "Myanmar",
  # "Oman",
  # "Palau",
  # "Panama",
  # "Paraguay",
  # "Peru",
  # "Philippines",
  # "Puerto Rico",
  # "Qatar",
  # "San Marino",
  # "S\U00E3o Tom\U00E9 and Pr\U00EDncipe",
  # "Sierra Leone",
  # "Solomon Islands",
   "Somalia"
  # "South Sudan",
  # "Sri Lanka",
  # "Sudan",
  # "Suriname",
  # "Syrian Arab Republic",
  # "Tajikistan",
  # "Timor-Leste",
  # "Togo",
  # "Turkey",
  # "United Arab Emirates",
  # "Uruguay",
  # "Uzbekistan",
  # "Vanatu",
  # "Venezuela, RB"
)

economiesToRemoveTradingAcrossBorders16 <- c(
  "Eritrea",
  "Liechtenstein",
  "Venezuela, RB",
  "Yemen, Rep."
)

economiesToRemoveEnforcingContracts16 <- c(
  "Liechtenstein"
)

economiesToRemoveResolvingInsolvency16 <- c(
  # "Angola",
  # "Bhutan",
  # "Cabo Verde",
  # "Comoros",
  # "Congo, Dem. Rep.",
  # "Equatorial Guinea",
  # "Eritrea",
  # "Grenada",
  # "Guinea-Bissau",
  # "Haiti",
  # "Iraq",
  # "Lao PDR",
  # "Libya",
  "Liechtenstein"
  # "Mauritania",
  # "S\U00E3o Tom\U00E9 and Pr\U00EDncipe",
  # "Saudi Arabia",
  # "Somalia",
  # "South Sudan",
  # "St. Kitts and Nevis",
  # "St. Vincent and the Grenadines",
  # "Timor-Leste",
  # "West Bank and Gaza"
)


lowerLimitStartingBusiness16 <- 0;
upperLimitStartingBusiness16 <- 100;

scoresStartingBusiness16 <- c(
  "Score.Procedures...Men..number.",
  "Score.Procedures...Women..number.",
  "Score.Time...Men..days.",
  "Score.Time...Women..days.",
  "Score.Cost...Men....of.income.per.capita.",
  "Score.Cost...Women....of.income.per.capita.",
  "Score.Paid.in.Minimum.capital....of.income.per.capita.")

indicatorsStartingBusiness16 <- list(
  c("Procedures...Men..number.", 5.88235, 1),
  c("Procedures...Women..number.", 5.88235, 1),
  c("Time...Men..days.", 1.00502, 0.5),
  c("Time...Women..days.", 1.00502, 0.5),
  c("Cost...Men....of.income.per.capita.", 0.5, 0.1),
  c("Cost...Women....of.income.per.capita.", 0.5, 0.1),
  c("Paid.in.Minimum.capital....of.income.per.capita.", 0.25, 0.1)
)

scoresDictionaryStartingBusiness16 <-
  setNames(as.list(indicatorsStartingBusiness16), scoresStartingBusiness16)

lowerLimitDealingWithConstructionPermits16 <- 0;
upperLimitDealingWithConstructionPermits16 <- 100;

scoresDealingWithConstructionPermits16 <- c(
  "Score.Procedures..number....Dealing.with.construction.permits",
  "Score.Time..days....Dealing.with.construction.permits",
  "Score.Cost....of.Warehouse.value.",
  "Score.Building.quality.control.index..0.15...DB16.20.methodology.")

indicatorsDealingWithConstructionPermits16 <- list(
  c("Procedures..number....Dealing.with.construction.permits", 4, 5),
  c("Time..days....Dealing.with.construction.permits", 0.28818, 26),
  c("Cost....of.Warehouse.value.", 5, 0),
  c("Building.quality.control.index..0.15...DB16.20.methodology.", -6.66666, 15)
)

scoresDictionaryDealingWithConstructionPermits16 <-
  setNames(as.list(indicatorsDealingWithConstructionPermits16), scoresDealingWithConstructionPermits16)

lowerLimitGettingElectricity16 <- 0;
upperLimitGettingElectricity16 <- 100;

scoresGettingElectricity16 <- c(
  "Score.Procedures..number....Getting.electricity",
  "Score.Time..days....Getting.electricity",
  "Score.Cost....of.income.per.capita.",
  "Score.Reliability.of.supply.and.transparency.of.tariff.index..0.8...DB16.20.methodology.")

indicatorsGettingElectricity16 <- list(
  c("Procedures..number....Getting.electricity", 16.666, 3),
  c("Time..days....Getting.electricity", 0.4348, 18),
  c("Cost....of.income.per.capita.", 0.01328, 0),
  c("Reliability.of.supply.and.transparency.of.tariff.index..0.8...DB16.20.methodology.", -12.5, 8)
)

scoresDictionaryGettingElectricity16 <-
  setNames(as.list(indicatorsGettingElectricity16), scoresGettingElectricity16)

lowerLimitRegisteringProperty16 <- 0;
upperLimitRegisteringProperty16 <- 100;

scoresRegisteringProperty16 <- c(
  "Score.Procedures..number....Registering.property",
  "Score.Time..days....Registering.property",
  "Score.Cost....of.property.value.",
  "Score.Quality.of.land.administration.index..0.30...DB17.20.methodology.")

indicatorsRegisteringProperty16 <- list(
  c("Procedures..number....Registering.property", 8.333, 1),
  c("Time..days....Registering.property", 0.4785, 1),
  c("Cost....of.property.value.", 6.667, 0),
  c("Quality.of.land.administration.index..0.30...DB17.20.methodology.", -3.333, 30)
)

scoresDictionaryRegisteringProperty16 <-
  setNames(as.list(indicatorsRegisteringProperty16), scoresRegisteringProperty16)

lowerLimitGettingCredit16 <- 0;
upperLimitGettingCredit16 <- 100;

scoresGettingCredit16 <- c(
  "Score.Strength.of.legal.rights.index..0.12...DB15.20.methodology.",
  "Score.Depth.of.credit.information.index..0.8...DB15.20.methodology.")

indicatorsGettingCredit16 <- list(
  c("Strength.of.legal.rights.index..0.12...DB15.20.methodology.", -8.333, 12),
  c("Depth.of.credit.information.index..0.8...DB15.20.methodology.", -12.5, 8)
)

scoresDictionaryGettingCredit16 <-
  setNames(as.list(indicatorsGettingCredit16), scoresGettingCredit16)


lowerLimitProtectingMinorityInvestors16 <- 0;
upperLimitProtectingMinorityInvestors16 <- 100;

scoresProtectingMinorityInvestors16 <- c(
  "Score.Extent.of.disclosure.index..0.10.",
  "Score.Extent.of.director.liability.index..0.10.",
  "Score.Ease.of.shareholder.suits.index..0.10...DB15.20.methodology.",
  "Score.Extent.of.shareholder.rights.index..0.6...DB15.20.methodology.",
  "Score.Extent.of.ownership.and.control.index..0.7...DB15.20.methodology.",
  "Score.Extent.of.corporate.transparency.index..0.7...DB15.20.methodology.",
  "Score.Strength.of.minority.investor.protection.index..0.50...DB15.20.methodology.")

indicatorsProtectingMinorityInvestors16 <- list(
  c("Extent.of.disclosure.index..0.10.", -10, 10),
  c("Extent.of.director.liability.index..0.10.", -10, 10),
  c("Ease.of.shareholder.suits.index..0.10...DB15.20.methodology.", -10, 10),
  c("Extent.of.shareholder.rights.index..0.6...DB15.20.methodology.", -16.66667, 6),
  c("Extent.of.ownership.and.control.index..0.7...DB15.20.methodology.", -14.2857, 7),
  c("Extent.of.director.liability.index..0.10.", -14.2857, 7),
  c("Strength.of.minority.investor.protection.index..0.50...DB15.20.methodology.", -2, 50)
)

scoresDictionaryProtectingMinorityInvestors16 <-
  setNames(as.list(indicatorsProtectingMinorityInvestors16), scoresProtectingMinorityInvestors16)


lowerLimitPayingTaxes16 <- 0;
upperLimitPayingTaxes16 <- 100;

scoresPayingTaxes16 <- c(
  "Score.Payments..number.per.year.",
  "Score.Time..hours.per.year.",
  "Score.Total.tax.and.contribution.rate....of.profit.",
  "Score.Time.to.comply.with.VAT.refund..hours...DB17.20.methodology.",
  "Score.Time.to.obtain.VAT.refund..weeks...DB17.20.methodology.",
  "Score.Time.to.comply.with.a.corporate.income.tax.correction..hours...DB17.20.methodology.",
  "Score.Time.to.complete.a.corporate.income.tax.correction..weeks...DB17.20.methodology.")

indicatorsPayingTaxes16 <- list(
  c("Payments..number.per.year.", 1.66, 3),
  c("Time..hours.per.year.", 0.15456, 49),
  c("Total.tax.and.contribution.rate....of.profit.", 1.37885, 26),
  c("Time.to.comply.with.VAT.refund..hours...DB17.20.methodology.", 2, 0),
  c("Time.to.obtain.VAT.refund..weeks...DB17.20.methodology.", 1.93, 0),
  c("Time.to.comply.with.a.corporate.income.tax.correction..hours...DB17.20.methodology.", 1.83486, 1),
  c("Score.Time.to.complete.a.corporate.income.tax.correction..weeks...DB17.20.methodology.", 3.125, 0)
)

scoresDictionaryPayingTaxes16 <-
  setNames(as.list(indicatorsPayingTaxes16), scoresPayingTaxes16)


lowerLimitTradingAcrossBorders16 <- 0;
upperLimitTradingAcrossBorders16 <- 100;

scoresTradingAcrossBorders16 <- c(
  "Score.Time.to.export..Documentary.compliance..hours...DB16.20.methodology.",
  "Score.Time.to.import..Documentary.compliance..hours...DB16.20.methodology.",
  "Score.Time.to.export..Border.compliance..hours...DB16.20.methodology.",
  "Score.Time.to.import..Border.compliance..hours...DB16.20.methodology.",
  "Score.Cost.to.export..Documentary.compliance..USD...DB16.20.methodology.",
  "Score.Cost.to.import..Documentary.compliance..USD...DB16.20.methodology.",
  "Score.Cost.to.export..Border.compliance..USD...DB16.20.methodology.",
  "Score.Cost.to.import..Border.compliance..USD...DB16.20.methodology.")

indicatorsTradingAcrossBorders16 <- list(
  c("Time.to.export..Documentary.compliance..hours...DB16.20.methodology.", 0.59171, 0),
  c("Time.to.import..Documentary.compliance..hours...DB16.20.methodology.", 0.41841, 1),
  c("Time.to.export..Border.compliance..hours...DB16.20.methodology.", 0.62893, 1),
  c("Time.to.import..Border.compliance..hours...DB16.20.methodology.", 0.35842, 3),
  c("Cost.to.export..Documentary.compliance..USD...DB16.20.methodology.", 0.25, 0),
  c("Cost.to.import..Documentary.compliance..USD...DB16.20.methodology.", 0.142858, 0),
  c("Cost.to.export..Border.compliance..USD...DB16.20.methodology.", 0.094339, 0),
  c("Cost.to.import..Border.compliance..USD...DB16.20.methodology.", 0.0833425, 0)
)

scoresDictionaryTradingAcrossBorders16 <-
  setNames(as.list(indicatorsTradingAcrossBorders16), scoresTradingAcrossBorders16)


lowerLimitEnforcingContracts16 <- 0;
upperLimitEnforcingContracts16 <- 100;

scoresEnforcingContracts16 <- c(
  "Score.Time..days....Enforcing.contracts",
  "Score.Cost....of.claim.",
  "Score.Quality.of.judicial.processes.index..0.18...DB17.20.methodology.")

indicatorsEnforcingContracts16 <- list(
  c("Time..days....Enforcing.contracts", 0.081967, 120),
  c("Cost....of.claim.", 1.1249, 0),
  c("Quality.of.judicial.processes.index..0.18...DB17.20.methodology.", -5.55555, 18)
)

scoresDictionaryEnforcingContracts16 <-
  setNames(as.list(indicatorsEnforcingContracts16), scoresEnforcingContracts16)

lowerLimitResolvingInsolvency16 <- 0;
upperLimitResolvingInsolvency16 <- 100;

scoresResolvingInsolvency16 <- c(
  "Score.Recovery.rate..cents.on.the.dollar.",
  "Score.Strength.of.insolvency.framework.index..0.16.")

indicatorsResolvingInsolvency16 <- list(
  c("Recovery.rate..cents.on.the.dollar.", -0.0966825, 93),
  c("Strength.of.insolvency.framework.index..0.16.", -6.25, 16)
)

scoresDictionaryResolvingInsolvency16 <-
  setNames(as.list(indicatorsResolvingInsolvency16), scoresResolvingInsolvency16)