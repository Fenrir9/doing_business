options(java.parameters = c("-XX:+UseConcMarkSweepGC", "-Xmx8192m"))
library("readxl")
options(java.parameters = c("-XX:+UseConcMarkSweepGC", "-Xmx8192m"))
gc()
library("xlsx")
#library("tidyverse")

readDataFromSheet <- function()
{
  heading = prepareColumnNames();
  
  economicData = read_excel(
    path = "../data/unprocessed/Historical_data_complete_dataset_with_scores.xlsx",
    col_types = c("text", "text", "text", "text", 
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric",
                  "numeric", "numeric", "numeric", "numeric", "numeric"),
    col_names = colnames(heading),
    range = cell_rows(5:3610))
  
  return(economicData)
  
}

saveFirstVersionData <- function(gruppedDataByField)
{
  filePath = "../data/all_data_common_denominator/first_version.xlsx"
  file.create(filePath)
  
  write.xlsx2(economicData,
              filePath,
              sheetName = "economicData",
              col.names = TRUE,
              row.names = FALSE,
              append = FALSE)
  
  write.xlsx2(gruppedDataByField@ranks,
              filePath,
              sheetName = "ranks",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@unrankedData,
              filePath,
              sheetName = "unrankedData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@scoreData,
              filePath,
              sheetName = "scoreData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@easeOfDoingBusinessData,
              filePath,
              sheetName = "easeOfDoingBusinessData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@startingBusinessData,
              filePath,
              sheetName = "startingBusinessData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@dealingWithConstructionPermitsData,
              filePath,
              sheetName = "dealingWithConstructionPermitsData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@gettingElectricityData,
              filePath,
              sheetName = "gettingElectricityData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@registeringPropertyData,
              filePath,
              sheetName = "registeringPropertyData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@gettingCreditData,
              filePath,
              sheetName = "gettingCreditData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@protectingMiniorityInvestorsData,
              filePath,
              sheetName = "protectingMiniorityInvestorsData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@payingTaxesData,
              filePath,
              sheetName = "payingTaxesData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@enforcingContractsData,
              filePath,
              sheetName = "enforcingContractsData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  # write.xlsx2(gruppedDataByField@resolvingInsolvencyData,
  #             filePath,
  #             sheetName = "resolvingInsolvencyData",
  #             col.names = TRUE,
  #             row.names = FALSE,
  #             append = TRUE)
}

saveSecondVersionData <- function(gruppedDataByField)
{
  filePath = "../data/new_methodology/second_version.xlsx"
  file.create(filePath)
  
  write.xlsx2(economicData,
              filePath,
              sheetName = "economicData",
              col.names = TRUE,
              row.names = FALSE,
              append = FALSE)
  
  write.xlsx2(gruppedDataByField@ranks,
              filePath,
              sheetName = "ranks",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@unrankedData,
              filePath,
              sheetName = "unrankedData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@scoreData,
              filePath,
              sheetName = "scoreData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@easeOfDoingBusinessData,
              filePath,
              sheetName = "easeOfDoingBusinessData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@startingBusinessData,
              filePath,
              sheetName = "startingBusinessData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@dealingWithConstructionPermitsData,
              filePath,
              sheetName = "dealingWithConstructionPermitsData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@gettingElectricityData,
              filePath,
              sheetName = "gettingElectricityData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@registeringPropertyData,
              filePath,
              sheetName = "registeringPropertyData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@gettingCreditData,
              filePath,
              sheetName = "gettingCreditData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@protectingMiniorityInvestorsData,
              filePath,
              sheetName = "protectingMiniorityInvestorsData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@payingTaxesData,
              filePath,
              sheetName = "payingTaxesData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@tradingAcrossBordersData,
              filePath,
              sheetName = "tradingAcrossBordersData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@enforcingContractsData,
              filePath,
              sheetName = "enforcingContractsData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
  
  write.xlsx2(gruppedDataByField@resolvingInsolvencyData,
              filePath,
              sheetName = "resolvingInsolvencyData",
              col.names = TRUE,
              row.names = FALSE,
              append = TRUE)
}
