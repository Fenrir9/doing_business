#options(java.parameters = c("-XX:+UseConcMarkSweepGC", "-Xmx8192m"))
library("dplyr")
#options(java.parameters = c("-XX:+UseConcMarkSweepGC", "-Xmx8192m"))
library("readxl")
options(java.parameters = c("-XX:+UseConcMarkSweepGC", "-Xmx8192m"))
gc()
library("xlsx")


removeIncopatibleFeatures16 <- function(economicData)
{
  economicData <- data.frame(economicData);
  
  economicData <- subset(
    economicData,
    select = -c(
      Ease.of.doing.business.score..DB15.methodology.,
      Ease.of.doing.business.score..DB10.14.methodology.,
      Score.Dealing.with.construction.permits..DB06.15.methodology.,
      Score.Getting.electricity..DB10.15.methodology.,
      System.average.interruption.duration.index..SAIDI...DB16.20.methodology.,
      System.average.interruption.frequency.index..SAIFI...DB16.20.methodology.,
      Minimum.outage.time..in.minutes....DB16.20.methodology.,
      Price.of.electricity..US.cents.per.kWh...DB16.20.methodology.,
      Score.Registering.property..DB05.15.methodology.,
      Equal.access.to.property.rights.index...2.0...DB17.20.methodology.,
      Score.Getting.credit..DB05.14.methodology.,
      Strength.of.legal.rights.index..0.10...DB05.14.methodology.,
      Score.Strength.of.legal.rights.index..0.10...DB05.14.methodology.,
      Depth.of.credit.information.index..0.6...DB05.14.methodology.,                         
      Score.Depth.of.credit.information.index..0.6...DB05.14.methodology.,                     
      Getting.Credit.total.score..DB05.14.methodology.,
      Credit.registry.coverage....of.adults.,                                      
      Credit.bureau.coverage....of.adults.,
      Score.Protecting.minority.investors..DB06.14.methodology.,
      Ease.of.shareholder.suits.index..0.10...DB06.14.methodology.,                        
      Score.Ease.of.shareholder.suits.index..0.10...DB06.14.methodology.,                   
      Strength.of.investor.protection.index..0.30...DB06.14.methodology.,
      Score.Paying.taxes..DB06.16.methodology.,
      Profit.tax....of.profit.,                                                                 
      Labor.tax.and.contributions....of.profit.,                                                
      Other.taxes....of.profit.,
      Score.Trading.across.borders..DB06.15.methodology.,
      Documents.to.export..number...DB06.15.methodology.,                                      
      Score.Documents.to.export..number...DB06.15.methodology.,                                 
      Documents.to.import..number...DB06.15.methodology.,                                       
      Score.Documents.to.import..number...DB06.15.methodology.,                                 
      Cost.to.export..US..per.container.deflated...DB06.15.methodology.,                   
      Score.Cost.to.export..US..per.container.deflated...DB06.15.methodology.,                  
      Cost.to.import..US..per.container.deflated...DB06.15.methodology.,                   
      Score.Cost.to.import..US..per.container.deflated...DB06.15.methodology.,                  
      Time.to.export..days...DB06.15.methodology.,                                         
      Score.Time.to.export..days...DB06.15.methodology.,                                        
      Time.to.import..days...DB06.15.methodology.,                                         
      Score.Time.to.import..days...DB06.15.methodology.,
      Score.Enforcing.contracts..DB04.15.methodology.,
      Procedures..number...DB04.15.methodology.,                                                
      Score.Procedures..number...DB04.15.methodology.,
      Filing.and.service..days.,                                                                
      Trial.and.judgment..days.,                                                                
      Enforcement.of.judgment..days.,
      Attorney.fees....of.claim.,                                                               
      Court.fees....of.claim.,                                                                  
      Enforcement.fees....of.claim.,
      Outcome..0.as.piecemeal.sale.and.1.as.going.concern.,                                    
      Time..years.,                                                                             
      Cost....of.estate.
   ))
  
  return (economicData)
}
