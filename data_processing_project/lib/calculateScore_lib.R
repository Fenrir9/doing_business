source("lib/constants/constant_data.R")

handleScores <- function(gruppedDataByField)
{
  gruppedDataByField@startingBusinessData <- handleScoreStartingBusiness(gruppedDataByField@startingBusinessData);
  gruppedDataByField@dealingWithConstructionPermitsData <- handleScoreDealingWithConstructionPermits(gruppedDataByField@dealingWithConstructionPermitsData);
  gruppedDataByField@registeringPropertyData <- handleScoreRegisteringProperty(gruppedDataByField@registeringPropertyData);
  gruppedDataByField@gettingElectricityData <- handleScoreGettingElectricity(gruppedDataByField@gettingElectricityData);
  gruppedDataByField@gettingCreditData <- handleScoreGettingCredit(gruppedDataByField@gettingCreditData);
  gruppedDataByField@protectingMiniorityInvestorsData <- handleScoreProtectingMinorityInvestors(gruppedDataByField@protectingMiniorityInvestorsData);
  gruppedDataByField@payingTaxesData <- handleScorePayingTaxes(gruppedDataByField@payingTaxesData);
  gruppedDataByField@enforcingContractsData <- handleScoreEnforcingContracts(gruppedDataByField@enforcingContractsData);
  gruppedDataByField@resolvingInsolvencyData <- handleScoreResolvingInsolvencyData(gruppedDataByField@resolvingInsolvencyData);
  gruppedDataByField@easeOfDoingBusinessData <- handleScoreEaseofDoingBussinesData(gruppedDataByField);
  
  return(gruppedDataByField)
}

handleScoreStartingBusiness <- function(startingBusiness)
{
  startingBusiness <- data.frame(startingBusiness);
  
  startingBusiness <- assignIndicatorsScoresStartingBusiness(startingBusiness);
  
  startingBusiness["Score.Starting.a.business"] = 
    data.frame(rowMeans(data.frame(
      rowMeans(
        startingBusiness[
          , 
          c("Score.Procedures...Men..number.",
            "Score.Procedures...Women..number.")]),
      rowMeans(
        startingBusiness[
          , 
          c("Score.Time...Men..days.",
            "Score.Time...Women..days.")]),
      rowMeans(
        startingBusiness[
          , 
          c("Score.Cost...Men....of.income.per.capita.",
            "Score.Cost...Women....of.income.per.capita.")]),
      startingBusiness[
        ,
        c("Score.Paid.in.Minimum.capital....of.income.per.capita.")]
      )));
  
  return(startingBusiness)
}

handleScoreDealingWithConstructionPermits <- function(dealingWithConstructionPermits)
{
  dealingWithConstructionPermits <- data.frame(dealingWithConstructionPermits);
  
  dealingWithConstructionPermits <- 
    assignIndicatorsDealingWithConstructionPermits(dealingWithConstructionPermits);
  
  dealingWithConstructionPermits["Score.Dealing.with.construction.permits."] = 
    rowMeans(dealingWithConstructionPermits[
      ,
      c("Score.Procedures..number....Dealing.with.construction.permits",
        "Score.Time..days....Dealing.with.construction.permits",
        "Score.Cost....of.Warehouse.value.")
      ]);
  
  
  
  dealingWithConstructionPermits <- subset(
    dealingWithConstructionPermits,
    select = -c(Score.Dealing.with.construction.permits..DB06.15.methodology.,
                Score.Dealing.with.construction.permits..DB16.20.methodology.)
    )
  return(dealingWithConstructionPermits)
}

handleScoreRegisteringProperty <- function(registeringProperty)
{
  registeringProperty <- data.frame(registeringProperty);
  
  registeringProperty <- assignIndicatorsRegisteringProperty(registeringProperty);
  
  registeringProperty["Score.Registering.property."] = 
    rowMeans(registeringProperty[
      ,
      c("Score.Procedures..number....Registering.property",
        "Score.Time..days....Registering.property",
        "Score.Cost....of.property.value.")
    ]);
  
  registeringProperty <- subset(
    registeringProperty,
    select = -c(Score.Registering.property..DB05.15.methodology.,
                Score.Registering.property..DB17.20.methodology.)
  )
  return(registeringProperty)
}

handleScoreGettingElectricity <- function(gettingElectricity)
{
  gettingElectricity <- data.frame(gettingElectricity);
  
  gettingElectricity <- assignIndicatorsGettingElectricity(gettingElectricity);
  
  gettingElectricity["Score.Getting.electricity."] = 
    rowMeans(gettingElectricity[
      ,
      c("Score.Procedures..number....Getting.electricity",
        "Score.Time..days....Getting.electricity",
        "Score.Cost....of.income.per.capita.")
      ]);
    
  gettingElectricity <- subset(
    gettingElectricity,
    select = -c(Score.Getting.electricity..DB10.15.methodology.,
                Score.Getting.electricity..DB16.20.methodology.)
  )
  return(gettingElectricity)
}

handleScoreGettingCredit <- function(gettingCredit)
{
  gettingCredit <- data.frame(gettingCredit);
  
  gettingCredit <- assignIndicatorsGettingCredit(gettingCredit);
  
  gettingCredit["Score.Getting.credit."] = 
    rowMeans(gettingCredit[
      ,
      c("Score.Strength.of.legal.rights.",
        "Score.Depth.of.credit.information.index.")
    ]);
  
  gettingCredit <- subset(
    gettingCredit,
    select = -c(Score.Getting.credit..DB05.14.methodology.,
                Score.Getting.credit..DB15.20.methodology.,
                Strength.of.legal.rights.index..0.10...DB05.14.methodology.,
                Score.Strength.of.legal.rights.index..0.10...DB05.14.methodology.,
                Strength.of.legal.rights.index..0.12...DB15.20.methodology.,
                Score.Strength.of.legal.rights.index..0.12...DB15.20.methodology.,
                Depth.of.credit.information.index..0.6...DB05.14.methodology.,
                Score.Depth.of.credit.information.index..0.6...DB05.14.methodology.,
                Depth.of.credit.information.index..0.8...DB15.20.methodology.,
                Score.Depth.of.credit.information.index..0.8...DB15.20.methodology.)
  )
  return(gettingCredit)
}

handleScoreProtectingMinorityInvestors <- function(protectingMinorityInvestors)
{
  protectingMinorityInvestors <- data.frame(protectingMinorityInvestors);
  
  protectingMinorityInvestors <-
    assignIndicatorsProtectingMinorityInvestors(protectingMinorityInvestors);
  
  protectingMinorityInvestors["Score.Protecting.minority.investors."] = 
    rowMeans(protectingMinorityInvestors[
      ,
      c("Score.Extent.of.disclosure.index..0.10.",
        "Score.Extent.of.director.liability.index..0.10.",
        "Score.Ease.of.shareholder.suits.index.",
        "Score.Strength.of.minority.investor.protection.index.")
    ])
  
  protectingMinorityInvestors <- subset(
    protectingMinorityInvestors,
    select = -c(Score.Protecting.minority.investors..DB15.20.methodology.,
                Score.Protecting.minority.investors..DB06.14.methodology.,
                Ease.of.shareholder.suits.index..0.10...DB06.14.methodology.,
                Score.Ease.of.shareholder.suits.index..0.10...DB06.14.methodology.,
                Ease.of.shareholder.suits.index..0.10...DB15.20.methodology.,
                Score.Ease.of.shareholder.suits.index..0.10...DB15.20.methodology.,
                Strength.of.investor.protection.index..0.30...DB06.14.methodology.,
                Strength.of.minority.investor.protection.index..0.50...DB15.20.methodology.)
  )
  return(protectingMinorityInvestors)
}

handleScorePayingTaxes <- function(payingTaxes)
{
  payingTaxes <- data.frame(payingTaxes);
  
  payingTaxes <- assignIndicatorsPayingTaxes(payingTaxes);
  
  payingTaxes <- subset(
    payingTaxes,
    select = -c(Score.Paying.taxes..DB17.20.methodology.,
                Score.Paying.taxes..DB06.16.methodology.)
  )
  
  payingTaxes["Score.Paying.Taxes."] = 
    rowMeans(payingTaxes[
      ,
      c("Score.Payments..number.per.year.",
        "Score.Time..hours.per.year.",
        "Score.Total.tax.and.contribution.rate....of.profit.")
    ])
  
  return(payingTaxes)
}

handleScoreEnforcingContracts <- function(enforcingContracts)
{
  enforcingContracts <- data.frame(enforcingContracts);
  
  enforcingContracts <- assignIndicatorsEnforcingContracts(enforcingContracts);
  
  enforcingContracts <- subset(
    enforcingContracts,
    select = -c(Score.Enforcing.contracts..DB04.15.methodology.,
                Score.Enforcing.contracts..DB17.20.methodology.)
  )
  
  enforcingContracts["Score.Enforcing.contracts."] =
    rowMeans(enforcingContracts[
      ,
      c("Score.Time..days....Enforcing.contracts",
        "Score.Cost....of.claim.")
    ])

  return(enforcingContracts)
}

handleScoreResolvingInsolvencyData <- function(resolvingInsolvencyData)
{
  resolvingInsolvencyData <- data.frame(resolvingInsolvencyData);
  
  resolvingInsolvencyData <- assignIndicatorsResolvingInsolvency(resolvingInsolvencyData);

  resolvingInsolvencyData["Score.Resolving.insolvency"] = 
    rowMeans(resolvingInsolvencyData[
      ,
      c("Score.Recovery.rate..cents.on.the.dollar.",
        "Score.Strength.of.insolvency.framework.index..0.16.")
    ])
  
  return(resolvingInsolvencyData)
}

handleScoreEaseofDoingBussinesData <- function(gruppedDataByField)
{
  easeofDoingBussines <- data.frame(gruppedDataByField@easeOfDoingBusinessData)
  
  dfScoreList <- createDfScoreList(gruppedDataByField);
  
  easeofDoingBussines <- Reduce(
    function(x, y) merge(
      x, 
      y, 
      by.x=c("Economy", "DB.Year"), 
      by.y=c("Economy", "DB.Year")), 
    dfScoreList, accumulate = FALSE)
  
  
  easeofDoingBussines["Ease.of.doing.business.score."] <-
    rowMeans(
      easeofDoingBussines[
        , 
        c(
          "Score.Starting.a.business",
          "Score.Dealing.with.construction.permits.",
          "Score.Getting.electricity.",
          "Score.Registering.property.",
          "Score.Getting.credit.",
          "Score.Protecting.minority.investors.",
          "Score.Paying.Taxes.",
          "Score.Enforcing.contracts.",
          "Score.Resolving.insolvency"
        )])
  
  return(easeofDoingBussines)
}

assignIndicatorsScoresStartingBusiness <- function(startingBusiness)
{
  for (name in names(scoresDictionaryStartingBusiness))
  {
    startingBusiness[, name] <- 
      constrainIndicatorsScores(
        calculateIndicatorsWithConstant(
          startingBusiness, 
          scoresDictionaryStartingBusiness,
          name), 
        lowerLimitStartingBusiness, 
        upperLimitStartingBusiness)
  }
  
  return(startingBusiness)
}

assignIndicatorsDealingWithConstructionPermits <- function(dealingWithConstructionPermits)
{
  for (name in names(scoresDictionaryDealingWithConstructionPermits))
  {
    dealingWithConstructionPermits[, name] <- 
      constrainIndicatorsScores(
        calculateIndicatorsWithConstant(
          dealingWithConstructionPermits, 
          scoresDictionaryDealingWithConstructionPermits,
          name), 
        lowerLimitDealingWithConstructionPermits, 
        upperLimitDealingWithConstructionPermits)
  }
  
  return(dealingWithConstructionPermits)
}

assignIndicatorsGettingElectricity <- function(gettingElectricity)
{
  for (name in names(scoresDictionaryGettingElectricity))
  {
    gettingElectricity[, name] <- 
      constrainIndicatorsScores(
        calculateIndicatorsWithConstant(
          gettingElectricity, 
          scoresDictionaryGettingElectricity,
          name), 
        lowerLimitGettingElectricity, 
        upperLimitGettingElectricity)
  }
  
  return(gettingElectricity)
}

assignIndicatorsRegisteringProperty <- function(registeringProperty)
{
  for (name in names(scoresDictionaryRegisteringProperty))
  {
    registeringProperty[, name] <- 
      constrainIndicatorsScores(
        calculateIndicatorsWithConstant(
          registeringProperty, 
          scoresDictionaryRegisteringProperty,
          name), 
        lowerLimitRegisteringProperty, 
        upperLimitRegisteringProperty)
  }
  
  return(registeringProperty)
}

assignIndicatorsGettingCredit <- function(gettingCredit)
{
  gettingCredit <- gettingCredit %>% 
    mutate(Strength.of.legal.rights. = ifelse(
      is.na(Strength.of.legal.rights.index..0.10...DB05.14.methodology.), 
      Strength.of.legal.rights.index..0.12...DB15.20.methodology. / 1.2, 
      Strength.of.legal.rights.index..0.10...DB05.14.methodology.))
  
  gettingCredit["Score.Strength.of.legal.rights."] = 10 * gettingCredit["Strength.of.legal.rights."]
  
  gettingCredit <- gettingCredit %>% 
    mutate(Depth.of.credit.information.index. = ifelse(
      is.na(Depth.of.credit.information.index..0.6...DB05.14.methodology.), 
      1.25 * Depth.of.credit.information.index..0.8...DB15.20.methodology., 
      1.6666667 * Depth.of.credit.information.index..0.6...DB05.14.methodology.))
  
  gettingCredit["Score.Depth.of.credit.information.index."] = 10 * gettingCredit["Depth.of.credit.information.index."]
  
  return(gettingCredit)
}

assignIndicatorsProtectingMinorityInvestors <- function(protectingMinorityInvestors)
{
  protectingMinorityInvestors <- protectingMinorityInvestors %>% 
    mutate(Strength.of.minority.investor.protection.index. = ifelse(
      is.na(Strength.of.investor.protection.index..0.30...DB06.14.methodology.), 
      Strength.of.minority.investor.protection.index..0.50...DB15.20.methodology., 
      1.6666667 * Strength.of.investor.protection.index..0.30...DB06.14.methodology.))
  
  protectingMinorityInvestors["Score.Strength.of.minority.investor.protection.index."] = 
    2 * protectingMinorityInvestors["Strength.of.minority.investor.protection.index."]
  
  protectingMinorityInvestors <- protectingMinorityInvestors %>% 
    mutate(Ease.of.shareholder.suits.index. = ifelse(
      is.na(Ease.of.shareholder.suits.index..0.10...DB15.20.methodology.), 
      Ease.of.shareholder.suits.index..0.10...DB06.14.methodology., 
      Ease.of.shareholder.suits.index..0.10...DB15.20.methodology.))
  
  protectingMinorityInvestors["Score.Ease.of.shareholder.suits.index."] = 
    10 * protectingMinorityInvestors["Ease.of.shareholder.suits.index."]
  
  protectingMinorityInvestors["Score.Extent.of.disclosure.index..0.10."] =
    10 * protectingMinorityInvestors["Extent.of.disclosure.index..0.10."]
  
  protectingMinorityInvestors["Score.Extent.of.director.liability.index..0.10."] =
    10 * protectingMinorityInvestors["Extent.of.director.liability.index..0.10."]
  
  return(protectingMinorityInvestors)
}

assignIndicatorsPayingTaxes <- function(payingTaxes)
{
  for (name in names(scoresDictionaryPayingTaxes))
  {
    payingTaxes[, name] <- 
      constrainIndicatorsScores(
        calculateIndicatorsWithConstant(
          payingTaxes, 
          scoresDictionaryPayingTaxes,
          name),
        lowerLimitPayingTaxes,
        upperLimitPayingTaxes)
  }
  
  return(payingTaxes)
}

assignIndicatorsEnforcingContracts <- function(enforcingContracts)
{
  for (name in names(scoresDictionaryEnforcingContracts))
  {
    enforcingContracts[, name] <- 
      constrainIndicatorsScores(
        calculateIndicatorsWithConstant(
          enforcingContracts, 
          scoresDictionaryEnforcingContracts,
          name),
        lowerLimitEnforcingContracts,
        upperLimitEnforcingContracts)
  }
  
  return(enforcingContracts)
}

assignIndicatorsResolvingInsolvency <- function(resolvingInsolvency)
{
  resolvingInsolvency["Score.Strength.of.insolvency.framework.index..0.16."] <-
    resolvingInsolvency["Strength.of.insolvency.framework.index..0.16."] * 6.25;
  
  for (name in names(scoresDictionaryResolvingInsolvency))
  {
    resolvingInsolvency[, name] <- 
      constrainIndicatorsScores(
        calculateIndicatorsWithConstant(
          resolvingInsolvency, 
          scoresDictionaryResolvingInsolvency,
          name),
        lowerLimitResolvingInsolvency,
        upperLimitResolvingInsolvency)
  }
  
  return(resolvingInsolvency)
}

createDfScoreList <- function(gruppedDataByField)
{
  dfScoreList <- list(
    gruppedDataByField@easeOfDoingBusinessData, 
    select(
      gruppedDataByField@startingBusinessData,
      c(
        'Score.Starting.a.business',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@dealingWithConstructionPermitsData,
      c(
        'Score.Dealing.with.construction.permits.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@gettingElectricityData,
      c(
        'Score.Getting.electricity.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@registeringPropertyData,
      c(
        'Score.Registering.property.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@gettingCreditData,
      c(
        'Score.Getting.credit.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@protectingMiniorityInvestorsData,
      c(
        'Score.Protecting.minority.investors.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@payingTaxesData,
      c(
        'Score.Paying.Taxes.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@enforcingContractsData,
      c(
        'Score.Enforcing.contracts.',
        'Economy',
        'DB.Year')
    ),
    select(
      gruppedDataByField@resolvingInsolvencyData,
      c(
        'Score.Resolving.insolvency',
        'Economy',
        'DB.Year')
    )
  )
  
  return(dfScoreList)
}

constrainIndicatorsScores <- function(calulatedScore, lowerLimit, upperLimit)
{
  return(pmin(upperLimit, pmax(lowerLimit, calulatedScore)))
}

calculateIndicatorsWithConstant <- function(field, indicators, name)
{
  result <-
    upperLimitStartingBusiness -
    as.numeric(indicators[[name]][2]) *
    (field[
      ,
      indicators[[name]][1]] - as.numeric(indicators[[name]][3]))
  
  return(result)
}

calculateIndicatorsWithoutConstant <- function(field, indicators, name)
{
  result <- as.numeric(indicators[[name]][2]) * field[, indicators[[name]][1]]
  
  return(result)
}