library(logging)
library("readxl")
source("lib/constants/constant_data.R")
source("lib/organizedData_class.R")

removeNotAssigned <- function(dataToCleanFromNA, firstYearOfData)
{
  dataToCleanFromNA@startingBusinessData <- 
    removeNotAssignedStartingBussines(gruppedDataByField, firstYearOfData);
  dataToCleanFromNA@dealingWithConstructionPermitsData <- 
    removeNotAssignedDealingWithConstructionPermits(
      gruppedDataByField, 
      2006);
  dataToCleanFromNA@gettingElectricityData <- 
    removeNotAssignedGettingElectricity(gruppedDataByField, 2010);
  dataToCleanFromNA@registeringPropertyData <- 
    removeNotAssignedRegisteringProperty(gruppedDataByField, 2005);
  dataToCleanFromNA@gettingCreditData <- 
    removeNotAssignedGettingCredit(gruppedDataByField, 2005);
  dataToCleanFromNA@protectingMiniorityInvestorsData <- 
    removeNotAssignedProtectingMinorityInvestors(
      gruppedDataByField, 
      2006);
  dataToCleanFromNA@payingTaxesData <- 
    removeNotAssignedPayingTaxes(gruppedDataByField, 2006);
  dataToCleanFromNA@enforcingContractsData <- 
    removeNotAssignedEnforcingContracts(gruppedDataByField, firstYearOfData);
  dataToCleanFromNA@resolvingInsolvencyData <- 
    removeNotAssignedResolvingInsolvency(gruppedDataByField, firstYearOfData);
  dataToCleanFromNA@easeOfDoingBusinessData <-
    removeNotAssignedEaseOfDoingBusiness(gruppedDataByField);

  return(dataToCleanFromNA)
}

removeNotAssignedStartingBussines <- function(
  dataToCleanFromNA, 
  yearToStart = 2004)
{
  if(yearToStart >= 2014)
  {
    loginfo("yearToStart >= 2014",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveStartingBussines <- economiesToRemoveStartingBussines[68:69];
  }else if(yearToStart >= 2006)
  {
    loginfo("2014 > yearToStart >= 2006",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveStartingBussines <- 
      economiesToRemoveStartingBussines[31:69];
  }else if(yearToStart >= 2004)
  {
    loginfo("2006 > yearToStart >= 2004",
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    economiesToRemoveStartingBussines <- list();
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in startingBussines isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  dataToCleanFromNA@startingBusinessData <- 
    removeYearsStartingBusiness(
      dataToCleanFromNA, 
      yearToStart);
  dataCleaned <- 
    removeEconomiesStartingBussines(
      dataToCleanFromNA, 
      economiesToRemoveStartingBussines);
  
  if (any(is.na(dataCleaned)))
  {
    logwarn("startingBusinessData hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("startingBusinessData has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (dataCleaned)
}

removeNotAssignedDealingWithConstructionPermits <- function(dataToCleanFromNA, yearToStart = 2006)
{
  if(yearToStart >= 2014)
  {
    loginfo("yearToStart >= 2014",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveDealingWithConstructionPermits <- economiesToRemoveDealingWithConstructionPermits[39:45];
  }else if(yearToStart >= 2006)
  {
    loginfo("2014 >= yearToStart >= 2006",
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    economiesToRemoveDealingWithConstructionPermits <- list();
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in dealingWithConstructionPermit isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
  }

  dataToCleanFromNA@dealingWithConstructionPermitsData <- removeYearsDealingWithConstructionPermits(dataToCleanFromNA, yearToStart);
  dataCleaned <- removeEconomiesDealingWithConstructionPermits(dataToCleanFromNA, economiesToRemoveDealingWithConstructionPermits);
  
  if (any(is.na(dataCleaned)))
  {
    logwarn("dealingWithConstructionPermits hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("dealingWithConstructionPermits has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (dataCleaned)
}

removeEconomiesStartingBussines <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@startingBusinessData <- data.frame(dataToCleanFromNA@startingBusinessData)
  startingBusinessData <- subset(dataToCleanFromNA@startingBusinessData,  !is.element(Economy, economiesToRemove))

  return(startingBusinessData);
}

removeEconomiesDealingWithConstructionPermits <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@dealingWithConstructionPermitsData <- data.frame(dataToCleanFromNA@dealingWithConstructionPermitsData)
  dealingWithConstructionPermitsData <- subset(dataToCleanFromNA@dealingWithConstructionPermitsData,  !is.element(Economy, economiesToRemove))
  
  return(dealingWithConstructionPermitsData);
}

removeYearsStartingBusiness <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@startingBusinessData <- data.frame(dataToCleanFromNA@startingBusinessData)
  cleanedData <- subset(dataToCleanFromNA@startingBusinessData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeYearsDealingWithConstructionPermits <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@dealingWithConstructionPermitsData <- data.frame(dataToCleanFromNA@dealingWithConstructionPermitsData)
  cleanedData <- subset(dataToCleanFromNA@dealingWithConstructionPermitsData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeNotAssignedGettingElectricity <- function(dataToCleanFromNA, yearToStart = 2006)
{
  if(yearToStart >= 2014)
  {
    loginfo("yearToStart >= 2014",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveGettingElectricity <- economiesToRemoveGettingElectricity2014;
  }else if(yearToStart >= 2010)
  {
    loginfo("2014 >= yearToStart >= 2010",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveGettingElectricity <- economiesToRemoveGettingElectricity2010;
  }else
  {
    economiesToRemoveGettingElectricity <- list();
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in gettingElectricity isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  dataToCleanFromNA@gettingElectricityData <- removeYearsGettingElectricity(dataToCleanFromNA, yearToStart);
  gettingElectricity <- removeEconomiesGettingElectricity(dataToCleanFromNA, economiesToRemoveGettingElectricity);
  
  if (any(is.na(gettingElectricity)))
  {
    logwarn("gettingElectricity hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("gettingElectricity has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (gettingElectricity)
}

removeEconomiesGettingElectricity <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@gettingElectricityData <- data.frame(dataToCleanFromNA@gettingElectricityData)
  gettingElectricityData <- subset(dataToCleanFromNA@gettingElectricityData,  !is.element(Economy, economiesToRemove))
  
  return(gettingElectricityData);
}

removeYearsGettingElectricity <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@gettingElectricityData <- data.frame(dataToCleanFromNA@gettingElectricityData)
  cleanedData <- subset(dataToCleanFromNA@gettingElectricityData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeNotAssignedRegisteringProperty <- function(dataToCleanFromNA, yearToStart = 2005)
{
  if(yearToStart >= 2014)
  {
    loginfo("yearToStart >= 2014",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveRegisteringProperty <- economiesToRemoveRegisteringProperty2014;
  }else if(yearToStart >= 2008)
  {
    loginfo("2014 >= yearToStart >= 2008",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveRegisteringProperty <- economiesToRemoveRegisteringProperty2008;
  }else if(yearToStart >= 2005)
  {
    loginfo("2008 >= yearToStart >= 2005",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveRegisteringProperty <- economiesToRemoveRegisteringProperty2005;
  }else
  {
    economiesToRemoveRegisteringProperty <- list();
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in registeringProperty isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  dataToCleanFromNA@registeringPropertyData <- removeYearsRegisteringProperty(dataToCleanFromNA, yearToStart);
  registeringProperty <- removeEconomiesRegisteringProperty(dataToCleanFromNA, economiesToRemoveRegisteringProperty);
  
  if (any(is.na(registeringProperty)))
  {
    logwarn("registeringProperty hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("registeringProperty has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (registeringProperty)
}

removeEconomiesRegisteringProperty <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@registeringPropertyData <- data.frame(dataToCleanFromNA@registeringPropertyData)
  registeringProperty <- subset(dataToCleanFromNA@registeringPropertyData,  !is.element(Economy, economiesToRemove))
  
  return(registeringProperty);
}

removeYearsRegisteringProperty <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@registeringPropertyData <- data.frame(dataToCleanFromNA@registeringPropertyData)
  cleanedData <- subset(dataToCleanFromNA@registeringPropertyData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeNotAssignedGettingCredit <- function(dataToCleanFromNA, yearToStart = 2005)
{
  if(yearToStart >= 2014)
  {
    loginfo("yearToStart >= 2014",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveGettingCredit <- economiesToRemoveGettingCredit2014;
  }else if(yearToStart >= 2006)
  {
    loginfo("2014 >= yearToStart >= 2006",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveGettingCredit <- economiesToRemoveGettingCredit2006;
  }else if(yearToStart >= 2005)
  {
    loginfo("2006 >= yearToStart >= 2005",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveGettingCredit <- economiesToRemoveGettingCredit2005;
  }else
  {
    economiesToRemoveGettingCredit <- list();
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in GettingCredit isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  dataToCleanFromNA@gettingCreditData <- removeYearsGettingCredit(dataToCleanFromNA, yearToStart);
  gettingCredit <- removeEconomiesGettingCredit(dataToCleanFromNA, economiesToRemoveGettingCredit);
  
  if (any(is.na(gettingCredit)))
  {
    logwarn("GettingCredit hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("GettingCredit has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (gettingCredit)
}

removeEconomiesGettingCredit <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@gettingCreditData <- data.frame(dataToCleanFromNA@gettingCreditData)
  gettingCredit <- subset(dataToCleanFromNA@gettingCreditData,  !is.element(Economy, economiesToRemove))
  
  return(gettingCredit);
}

removeYearsGettingCredit <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@gettingCreditData <- data.frame(dataToCleanFromNA@gettingCreditData)
  cleanedData <- subset(dataToCleanFromNA@gettingCreditData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeNotAssignedProtectingMinorityInvestors <- function(dataToCleanFromNA, yearToStart = 2006)
{
  if(yearToStart >= 2006)
  {
    loginfo("yearToStart >= 2006",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveprotectingMiniorityInvestors <- economiesToRemoveprotectingMiniorityInvestors;
    
  }else
  {
    economiesToRemoveprotectingMiniorityInvestors <- list();
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in ProtectingMinorityInvestors isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  dataToCleanFromNA@protectingMiniorityInvestorsData <- removeYearsProtectingMinorityInvestors(dataToCleanFromNA, yearToStart);
  protectingMiniorityInvestors <- removeEconomiesProtectingMinorityInvestors(dataToCleanFromNA, economiesToRemoveprotectingMiniorityInvestors);
  
  if (any(is.na(protectingMiniorityInvestors)))
  {
    logwarn("ProtectingMiniorityInvestors hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("ProtectingMiniorityInvestors has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (protectingMiniorityInvestors)
}

removeEconomiesProtectingMinorityInvestors <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@protectingMiniorityInvestorsData <- data.frame(dataToCleanFromNA@protectingMiniorityInvestorsData)
  protectingMiniorityInvestors <- subset(dataToCleanFromNA@protectingMiniorityInvestorsData,  !is.element(Economy, economiesToRemove))
  
  return(protectingMiniorityInvestors);
}

removeYearsProtectingMinorityInvestors <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@protectingMiniorityInvestorsData <- data.frame(dataToCleanFromNA@protectingMiniorityInvestorsData)
  cleanedData <- subset(dataToCleanFromNA@protectingMiniorityInvestorsData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeNotAssignedPayingTaxes <- function(dataToCleanFromNA, yearToStart = 2006)
{
  if(yearToStart >= 2014)
  {
    loginfo("yearToStart >= 2014",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemovePayingTaxes <- economiesToRemovePayingTaxes2014;
  }else if(yearToStart >= 2010)
  {
    loginfo("2014 >= yearToStart >= 2010",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemovePayingTaxes <- ecoomiesToRemovePayingTaxes2010;
  }else if(yearToStart >= 2006)
  {
    loginfo("2010 >= yearToStart >= 2006",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemovePayingTaxes <- ecoomiesToRemovePayingTaxes2006;
  }else
  {
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in PayingTaxes isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemovePayingTaxes <- list();
  }
  
  dataToCleanFromNA@payingTaxesData <- removeYearsPayingTaxes(dataToCleanFromNA, yearToStart);
  payingTaxes <- removeEconomiesPayingTaxes(dataToCleanFromNA, economiesToRemovePayingTaxes);
  
  if (any(is.na(payingTaxes)))
  {
    logwarn("payingTaxes hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("payingTaxes has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (payingTaxes)
}

removeEconomiesPayingTaxes <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@payingTaxesData <- data.frame(dataToCleanFromNA@payingTaxesData)
  payingTaxes <- subset(dataToCleanFromNA@payingTaxesData,  !is.element(Economy, economiesToRemove))
  
  return(payingTaxes);
}

removeYearsPayingTaxes <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@payingTaxesData <- data.frame(dataToCleanFromNA@payingTaxesData)
  cleanedData <- subset(dataToCleanFromNA@payingTaxesData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeNotAssignedEnforcingContracts <- function(dataToCleanFromNA, yearToStart = 2004)
{
  if(yearToStart >= 2014)
  {
    loginfo("yearToStart >= 2014",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveEnforcingContracts <- economiesToRemoveEnforcingContracts2014;
  }else if(yearToStart >= 2007)
  {
    loginfo("2014 >= yearToStart >= 2007",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveEnforcingContracts <- economiesToRemoveEnforcingContracts2007;
  }else if(yearToStart >= 2004)
  {
    loginfo("2007 >= yearToStart >= 2004",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveEnforcingContracts <- economiesToRemoveEnforcingContracts2004;
  }else
  {
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in EnforcingContractst isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveEnforcingContracts <- list();
  }
  
  dataToCleanFromNA@enforcingContractsData <- removeYearsEnforcingContracts(dataToCleanFromNA, yearToStart);
  enforcingContracts <- removeEconomiesEnforcingContracts(dataToCleanFromNA, economiesToRemoveEnforcingContracts);
  
  if (any(is.na(enforcingContracts)))
  {
    logwarn("enforcingContracts hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("enforcingContracts has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (enforcingContracts)
}

removeEconomiesEnforcingContracts <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@enforcingContractsData <- data.frame(dataToCleanFromNA@enforcingContractsData)
  enforcingContracts <- subset(dataToCleanFromNA@enforcingContractsData,  !is.element(Economy, economiesToRemove))
  
  return(enforcingContracts);
}

removeYearsEnforcingContracts <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@enforcingContractsData <- data.frame(dataToCleanFromNA@enforcingContractsData)
  cleanedData <- subset(dataToCleanFromNA@enforcingContractsData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeNotAssignedResolvingInsolvency <- function(dataToCleanFromNA, yearToStart = 2004)
{
  if(yearToStart >= 2014)
  {
    loginfo("yearToStart >= 2014",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveResolvingInsolvency <- economiesToRemoveResolvingInsolvency2014;
  }else if(yearToStart >= 2007)
  {
    loginfo("2014 >= yearToStart >= 2007",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveResolvingInsolvency <- economiesToRemoveResolvingInsolvency2007;
  }else if(yearToStart >= 2004)
  {
    loginfo("2007 >= yearToStart >= 2004",
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveResolvingInsolvency <- economiesToRemoveResolvingInsolvency2004;
  }else
  {
    logwarn("yearToStart hasn't expected value. Rows with not assigned fields in ResolvingInsolvency isn't going to be cleaned.", 
            logger='PEcAn.MetaAnalysis.function1')
    economiesToRemoveResolvingInsolvency <- list();
  }
  
  dataToCleanFromNA@resolvingInsolvencyData <- removeYearsResolvingInsolvency(dataToCleanFromNA, yearToStart);
  resolvingInsolvency <- removeEconomiesResolvingInsolvency(dataToCleanFromNA, economiesToRemoveResolvingInsolvency);
  
  if (any(is.na(resolvingInsolvency)))
  {
    logwarn("resolvingInsolvency hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("resolvingInsolvency has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return (resolvingInsolvency)
}

removeEconomiesResolvingInsolvency <- function(dataToCleanFromNA, economiesToRemove)
{
  dataToCleanFromNA@resolvingInsolvencyData <- data.frame(dataToCleanFromNA@resolvingInsolvencyData)
  payingTaxes <- subset(dataToCleanFromNA@resolvingInsolvencyData,  !is.element(Economy, economiesToRemove))
  
  return(payingTaxes);
}

removeYearsResolvingInsolvency <- function(dataToCleanFromNA, firstYearToSave)
{
  dataToCleanFromNA@resolvingInsolvencyData <- data.frame(dataToCleanFromNA@resolvingInsolvencyData)
  cleanedData <- subset(dataToCleanFromNA@resolvingInsolvencyData,  DB.Year >= firstYearToSave)
  
  summary(cleanedData)
  
  return(cleanedData);
}

removeNotAssignedEaseOfDoingBusiness <- function(dataToCleanFromNA)
{
  easeOfDoingBusiness <-
    na.omit(dataToCleanFromNA@easeOfDoingBusinessData)
  
  if (any(is.na(easeOfDoingBusiness)))
  {
    logwarn("easeOfDoingBusiness hasn't been cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }else
  {
    loginfo("easeOfDoingBusiness has been successfully cleaned from not assigment cells", 
            logger='PEcAn.MetaAnalysis.function1')
  }
  
  return(easeOfDoingBusiness)
}
