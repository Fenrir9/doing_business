source("lib/calculateScore_lib.R")
source("lib/db14_meth_lib.R")
source("lib/getters_lib.R")
source("lib/io_lib.R")
source("lib/processRankings_lib.R")
source("lib/removeNotAssigned_lib.R")

economicData = readDataFromSheet();
economicData = removeIncopatibleFeatures(economicData);

gruppedDataByField <- organizeDataByField(economicData)
gruppedDataByField <- handleScores(gruppedDataByField)
gruppedDataByField <- removeNotAssigned(gruppedDataByField, 2004)
gruppedDataByField <- assignRankings(gruppedDataByField)

saveFirstVersionData(gruppedDataByField)
