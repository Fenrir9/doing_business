import math
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as mtp
import numpy as np
import pandas as pd
from pathlib import Path


def create_plots_for_countries():
    best_metodologiest = pd.read_excel('../data/statistical_results/best_metogogies.xlsx', engine='openpyxl')

    best_metodologiest = best_metodologiest.loc[(best_metodologiest['country'] == "Poland") | (best_metodologiest['country'] == "Georgia")]

    for error_meas_met in ["RMSE", "MAE"]:
        if error_meas_met == "RMSE":
            best_metodologiest_filtered = best_metodologiest[best_metodologiest['RMSE'].notna()]
        else:
            best_metodologiest_filtered = best_metodologiest[best_metodologiest['MAE'].notna()]

        plots_directory = '../data/final_ranks_reesults/plots/'
        Path(plots_directory).mkdir(parents=True, exist_ok=True)
        def plot_scores(x):
            if x["MAE"].isnull().values.any():
                print("MAE NaN")
                error_meas_met ='MAE'
            elif x["RMSE"].isnull().values.any():
                print("RMSE NaN")
                error_meas_met = 'RMSE'
            print(x)
            print("RMSE")
            print(x["RMSE"])
            print("MAE")
            print(x["MAE"])

            with PdfPages(
                    plots_directory +
                    x.iloc[0]['country'] + '_' +
                    x.iloc[0]['version'] + '_' +
                    x.iloc[0]['algorithm: '] + '_' +
                    error_meas_met + '_' +
                    x.iloc[0]['field_of_study'] + '_' +
                    x.iloc[0]['indicator'] + '.pdf') as pdf:
                mtp.scatter(x["years"], x["mean_score"], color="blue", marker='x')

                for standard_deviation, confidence_intervals_value, year in zip(x["std"], x["confidence_intervals"], x["years"]):
                    if standard_deviation > 0:
                        confidence_intervals_value = confidence_intervals_value.replace("(", "")
                        confidence_intervals_value = confidence_intervals_value.replace("'", "")
                        confidence_intervals_value = confidence_intervals_value.replace(")", "")
                        lowwer_upper = list(confidence_intervals_value.split(","))
                        mtp.plot((year, year), (float(lowwer_upper[0]), float(lowwer_upper[1])), 'ro-', color='orange')

                mtp.xticks(np.arange(min(x['years']), max(x['years']) + 1, 1.0))
                mtp.xticks(rotation=90)
                mtp.legend(['average value', 'confidence intervals'])
                mtp.title(f"{x.iloc[0]['country'] + ' ' + x.iloc[0]['indicator']}")
                mtp.xlabel("Year")
                mtp.ylabel("Score")
                pdf.savefig()
                # mtp.show()
                mtp.close()


        best_metodologiest_filtered.groupby(
            ["country", "version", "field_of_study", "indicator", "algorithm: "]).apply(plot_scores).reset_index(drop=True)

    # for version in ["first_version", "second_version"]:
    #     print("create_plots_for_countries ", version)
    #     for error_meas_met in ["RMSE", "MAE"]:
    #         print("create_plots_for_countries ", error_meas_met)
    #         for field_of_study in best_metodologiest["field_of_study"].unique():
    #             print("create_plots_for_countries ", field_of_study)
    #             for indicator in best_metodologiest["indicator"].unique():
    #                 print("create_plots_for_countries ", indicator)
    #                 for country in best_metodologiest["country"].unique(): #["Zimbabwe"]:#
    #                     print("create_plots_for_countries ", country)
    #                     for algorithm in best_metodologiest["algorithm: "].unique():
    #                         print("create_plots_for_countries ", algorithm)
    #                         research = best_metodologiest.loc[(best_metodologiest['field_of_study'] == field_of_study) &
    #                                                   (best_metodologiest['indicator'] == indicator) &
    #                                                   (best_metodologiest['country'] == country) &
    #                                                   (best_metodologiest['version'] == version) &
    #                                                   (best_metodologiest['error_methodology'] == error_meas_met) &
    #                                                   (best_metodologiest['algorithm: '] == algorithm)]
    #
    #                         if research.empty:
    #                             continue
    #
    #                         Path(plots_directory).mkdir(parents=True, exist_ok=True)
    #                         with PdfPages(
    #                                 plots_directory +
    #                                 version + '_' +
    #                                 algorithm + '_' +
    #                                 error_meas_met + '_' +
    #                                 field_of_study + '_' +
    #                                 country + '_' +
    #                                 indicator + '.pdf') as pdf:
    #
    #                             years = research["years"].tolist()[0]
    #                             years = years.replace('[', '')
    #                             years = years.replace(']', '')
    #                             years = years.replace(' ', '')
    #                             years = list(years.split(","))
    #                             years = list(map(float, years))
    #                             score = research["score"].tolist()[0]
    #                             score = score.replace('[', '')
    #                             score = score.replace(']', '')
    #                             score = score.replace(' ', '')
    #                             score = list(score.split(","))
    #                             score = list(map(float, score))
    #                             std = research["std"].tolist()[0]
    #                             std = std.replace('[', '')
    #                             std = std.replace(']', '')
    #                             std = std.replace(' ', '')
    #                             std = list(std.split(","))
    #                             std = list(map(float, std))
    #                             confidence_intervals = research["confidence_intervals"].tolist()[0]
    #                             confidence_intervals = confidence_intervals.replace('[', '')
    #                             confidence_intervals = confidence_intervals.replace(']', '')
    #                             confidence_intervals = confidence_intervals.replace(")', '(", ";")
    #                             confidence_intervals = confidence_intervals.replace(")'", "")
    #                             confidence_intervals = confidence_intervals.replace("'(", "")
    #                             #confidence_intervals = confidence_intervals.replace(' ', '')
    #
    #                             confidence_intervals = list(confidence_intervals.split(";"))
    #
    #
    #
    #                             # confidence_intervals_years = list()
    #                             # confidence_intervals_lower = list()
    #                             # confidence_intervals_upper = list()
    #                             mtp.scatter(years, score, color="blue", marker='x')
    #                             for standard_deviation, confidence_intervals_value, year in zip(std, confidence_intervals, years):
    #                                 if standard_deviation > 0:
    #                                     lowwer_upper = list(confidence_intervals_value.split(","))
    #                                     mtp.plot((year, year), (float(lowwer_upper[0]), float(lowwer_upper[1])), 'ro-', color='orange')
    #
    #                             mtp.xticks(np.arange(min(years), max(years) + 1, 1.0))
    #                             mtp.xticks(rotation=90)
    #                             mtp.legend(['average value','confidence intervals'])
    #                             mtp.title(f"{country + ' ' + indicator}")
    #                             mtp.xlabel("Year")
    #                             mtp.ylabel("Score")
    #                             pdf.savefig()
    #                             #mtp.show()
    #                             mtp.close()
