from os import listdir
import numpy as np
import pandas as pd
import re
from scipy import stats


def create_metrics_dataframe(field_of_study, indicator, methodology_path, methodology):
    metrics_files = listdir(methodology_path + "/" + methodology + "/")

    all_metrics = pd.DataFrame()
    for metrics_file in metrics_files:
        metrics = \
            pd.read_excel(methodology_path + "/" + methodology + '/' + metrics_file)

        metrics = metrics[metrics['years'].notna()]
        #metrics = metrics.dropna(how='all', axis=0, inplace=True)

        # metrics_for_indicator = metrics.loc[
        #     (metrics['field_of_study'] == field_of_study) &
        #     (metrics['indicator'] == indicator) &
        #     ((metrics['country'] == "Poland") | (metrics['country'] == "Zimbabwe"))][
        #     ["years", "country", "score"]]

        metrics_for_indicator = metrics.loc[
            (metrics['field_of_study'] == field_of_study) &
            (metrics['indicator'] == indicator)][
            ["years", "country", "score"]]

        metrics_dataframe = pd.DataFrame()

        for i in range(metrics_for_indicator.shape[0]):
            new_dataframe = pd.DataFrame(
                {'years': [int(x) for x in
                           metrics_for_indicator["years"].iloc[i].replace("[", "").replace("]", "").replace(".0", "").split(
                               ",")],
                 'Economy': metrics_for_indicator['country'].iloc[i],
                 'score': [float(x) for x in
                           metrics_for_indicator["score"].iloc[i].replace("[", "").replace("]", "").split(",")]})

            metrics_dataframe = pd.concat([metrics_dataframe, new_dataframe], ignore_index=True)

        if all_metrics.empty:
            all_metrics = metrics_dataframe
        else:
            all_metrics = pd.merge(all_metrics, metrics_dataframe, on=["Economy", "years"])

    all_metrics["score"] = all_metrics[list(filter(re.compile("score*").match, all_metrics.columns))].mean(axis=1)
    all_metrics["std"] = all_metrics[list(filter(re.compile("score[^$]").match, all_metrics.columns))].std(axis=1)
    # a_df = all_metrics[list(filter(re.compile("score[^$]").match, all_metrics.columns))]
    # all_metrics["score_x"] = 1
    # all_metrics["score_y"] = 5
    # all_metrics['confidence_intervals'] = \
    #     all_metrics[list(filter(re.compile("score[^$]").match, all_metrics.columns))].apply(lambda row: stats.norm.interval(0.95, loc=np.mean(row), scale=np.std(row)), axis=1)

    print(all_metrics.dtypes)

    all_metrics = all_metrics.drop(all_metrics[all_metrics["std"] > 3.0].index)

    all_metrics.drop(columns=list(filter(re.compile("score[^$]").match, all_metrics.columns)), axis=1, inplace=True)
    all_metrics[f'rank'] = all_metrics.groupby('years')['score'].rank("min", ascending=False)

    return all_metrics


def create_reference_dataframe(reference_file_path, indicator):
    # if version == "first_version":
    #     fieldData = pd.read_excel('../data/all_data_common_denominator/first_version.xlsx', sheet_name='ranks')
    # elif version == "second_version":
    #     fieldData = pd.read_excel('../data/new_methodology/second_version.xlsx', sheet_name='ranks')
    # else:
    #     logging.exception("Invalid data_version")
    #     quit()

    fieldData = pd.read_excel(reference_file_path, sheet_name='ranks')

    score_starting_a_business = fieldData[["DB.Year", "Economy", "Rank " + indicator]]

    # ssab_compare_pl_zimb = score_starting_a_business.loc[
    #     (score_starting_a_business['Economy'] == "Poland") |
    #     (score_starting_a_business['Economy'] == "Zimbabwe")]

    ssab_compare_pl_zimb = \
        score_starting_a_business.assign(rank=score_starting_a_business.groupby("DB.Year")["Rank " + indicator].rank())

    return ssab_compare_pl_zimb