import lib.data_frames_creator
import logging
import numpy as np
import pandas as pd
from openpyxl import load_workbook
from pathlib import Path
from scipy import stats
import os
import re


def compare_rankings_to_relative_ranking():
    Path('../data/statistical_results').mkdir(parents=True, exist_ok=True)

    stats_df = pd.DataFrame()
    for version in ["first_version", "second_version"]: #["first_version"]:#
        print("compare_rankings_to_relative_ranking ", version)

        if version == "first_version":
            methodologies_path = '../data/all_data_common_denominator'
        elif version == "second_version":
            methodologies_path = '../data/new_methodology'
        else:
            logging.exception("Invalid data_version")
            quit()

        methodologies_file = methodologies_path + '/' + version + '.xlsx'

        for methodology in next(os.walk(methodologies_path))[1]: #["arima"]: #["exponential_regression_random"]:#["neural_prophet_AR_Net"]:#

            print("compare_rankings_to_relative_ranking ", methodology)
            sheetnames = load_workbook(methodologies_file, read_only=True, keep_links=False).sheetnames

            for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
                sheetnames.remove(sheetname)


            #print(sheetnames)

            for sheetname in sheetnames: #["protectingMiniorityInvestorsDat"]: #
                print("compare_rankings_to_relative_ranking ", sheetname)
                rankings = pd.read_excel(methodologies_file, sheet_name=sheetname).columns.values.tolist()
                # for meta_data in ['Economy', 'DB.Year', 'Country.code']:
                #     rankings.remove(meta_data)
                ranksss = list(filter(re.compile("Score[^$]").match, rankings))
                for rank in list(filter(re.compile("Score[^$]").match, rankings)):
                    print("compare_rankings_to_relative_ranking ", rank)
                    referece_dataframe = lib.data_frames_creator.create_reference_dataframe(methodologies_file, rank)

                    metrix_dataframe = \
                        lib.data_frames_creator.create_metrics_dataframe(sheetname, rank, methodologies_path, methodology)



                    for year in list(set(referece_dataframe["DB.Year"]).intersection(np.unique(metrix_dataframe["years"]))):
                        a = metrix_dataframe.loc[metrix_dataframe["years"] == year].sort_values(by=['Economy'])
                        b = referece_dataframe.loc[referece_dataframe["DB.Year"] == year].sort_values(by=['Economy'])
                        merged_df = pd.merge(a, b, on="Economy", how="inner")
                        tau, p_value = stats.kendalltau(merged_df["rank_x"], merged_df["rank_y"])
                        (corelation, p_value_spearman) = stats.spearmanr(merged_df["rank_x"], merged_df["rank_y"])

                        stats_row = pd.DataFrame({
                            "version": [version],
                            "methodology": [methodology],
                            "sheetname": [sheetname],
                            "rank": [rank],
                            "year": [year],
                            "tau": [tau],
                            "tau p_value": [p_value],
                            "speerman corelation": [corelation],
                            "p_value_spearman": [p_value_spearman]
                        })
                        stats_df = pd.concat([stats_df, stats_row], ignore_index=True, axis=0)

    Path("../data/statistical_results").mkdir(parents=True, exist_ok=True)

    if os.path.exists("../data/statistical_results/rank_stats_data_neuralprophet.xlsx"):
        os.remove("../data/statistical_results/rank_stats_data_neuralprophet.xlsx")
        print("The file has been deleted successfully")
    else:
        print("The file does not exist!")

    stats_df.to_excel('../data/statistical_results/rank_stats_data_neuralprophet.xlsx', index=False)

    # if os.path.exists("../data/statistical_results/rank_stats_data.xlsx"):
    #     os.remove("../data/statistical_results/rank_stats_data.xlsx")
    #     print("The file has been deleted successfully")
    # else:
    #     print("The file does not exist!")
    #
    # stats_df.to_excel('../data/statistical_results/rank_stats_data.xlsx', index=False)


