import logging
from os import listdir
from os.path import isfile, join
import os
import pandas as pd
from pathlib import Path


def error_per_country_per_metodology_per_indicator():
    mae_average = pd.DataFrame()
    rmse_average = pd.DataFrame()
    for version in ["first_version", "second_version"]: #["first_version"]:#
        print("error_per_country_per_metodology_per_indicator ", version)
        if version == "first_version":
            methodologies_path = '../data/all_data_common_denominator'
        elif version == "second_version":
            methodologies_path = '../data/new_methodology'
        else:
            logging.exception("Invalid data_version")
            quit()

        for methodology in next(os.walk(methodologies_path))[1]: #["bayesian_ridge_regression_linear_random", "exponential_regression"]:#["neural_prophet_AR_Net"]:#
            print("error_per_country_per_metodology_per_indicator ", methodology)
            metrices_file_path = methodologies_path + "/" + methodology
            print(metrices_file_path)
            all_methodology_metrics = pd.DataFrame()
            for metrics_file in [f for f in listdir(metrices_file_path) if isfile(join(metrices_file_path, f))]:
                file = metrices_file_path + "/" + metrics_file
                meetrics = pd.read_excel(file, engine='openpyxl')
                print("error_per_country_per_metodology_per_indicator ", metrics_file)
                all_methodology_metrics = pd.concat([all_methodology_metrics, meetrics], axis=0)



        # for (field_of_study, indicator, country) in zip(
        #         all_methodology_metrics.loc[:, "field_of_study"],
        #         all_methodology_metrics.loc[:, "indicator"],
        #         all_methodology_metrics.loc[:, "country"]):
        #     print(field_of_study)
        #     print(indicator)
        #     print(country)
        #     print("=================================")
            all_methodology_metrics['MAE'] = all_methodology_metrics['MAE'].fillna(0)
            all_methodology_metrics['RMSE'] = all_methodology_metrics['RMSE'].fillna(0)

            mae_average_methodology = all_methodology_metrics.groupby(['field_of_study', 'indicator', 'country'])['MAE'].mean().reset_index()
            # all_methodology_metrics.to_excel('../data/statistical_results/test___mae_results_neural+prophet.xlsx', sheet_name="mae_average", index=False)
            mae_average_methodology['methodology'] = methodology
            mae_average_methodology['version'] = version
            rmse_average_methodology = all_methodology_metrics.groupby(['field_of_study', 'indicator', 'country'])['RMSE'].mean().reset_index()
            rmse_average_methodology['methodology'] = methodology
            rmse_average_methodology['version'] = version

            mae_average = pd.concat([mae_average, mae_average_methodology], axis=0)
            rmse_average = pd.concat([rmse_average, rmse_average_methodology], axis=0)


    Path("../data/statistical_results").mkdir(parents=True, exist_ok=True)
    if os.path.exists("../data/statistical_results/rmse_results_neural+prophet.xlsx"):
        os.remove("../data/statistical_results/rmse_results_neural+prophet.xlsx")
        print("The file has been deleted successfully")
    else:
        print("The file does not exist!")

    if os.path.exists("../data/statistical_results/mae_results_neural+prophet.xlsx"):
        os.remove("../data/statistical_results/mae_results_neural+prophet.xlsx")
        print("The file has been deleted successfully")
    else:
        print("The file does not exist!")

    mae_average.to_excel('../data/statistical_results/mae_results_neural+prophet.xlsx', sheet_name="mae_average", index=False)
    rmse_average.to_excel('../data/statistical_results/rmse_results_neural+prophet.xlsx', sheet_name="rmse_average", index=False)
