import assign_best_metodologiests
import average_error_calculations
import best_country_ranking
import calculate_scores
import compare_rankings
import create_plots
import logging
import rank_methodology
import statisctical_calculation

logging.warning("----------------STEP 1 Statistical calculations----------------------")
statisctical_calculation.statistical_calculations()

logging.warning("----------STEP 2 compare ranings to relative ranking-----------------")
compare_rankings.compare_rankings_to_relative_ranking()

logging.warning("-----STEP 3 evaluate the methodology for the country indicator-------")
average_error_calculations.error_per_country_per_metodology_per_indicator()

logging.warning("----------STEP 4 remove metodologiests with random output------------")
assign_best_metodologiests.clean_dataset_from_randomly_working_metodologiests()

logging.warning("----------------------STEP 5 rank metodologies-----------------------")
rank_methodology.rank_metodologies()

logging.warning("------------------STEP 6 predict country rankings--------------------")
best_country_ranking.predict_coutries_ranking()

logging.warning("----------------------STEP 7 calculate scores------------------------")
calculate_scores.calculate_scores()

logging.warning("------------------------STEP 8 create plots--------------------------")
create_plots.create_plots_for_countries()
