import pandas as pd
from pathlib import Path
import os


def predict_coutries_ranking():
    best_metodologiest = pd.read_excel('../data/statistical_results/best_metogogies.xlsx', engine='openpyxl')

    # rankings = pd.DataFrame()
    # for row in best_metodologiest.iterrows():
    #     years = row[1]["years"]
    #
    #     years = years.replace('[', '')
    #     years = years.replace(']', '')
    #     years = years.replace(' ', '')
    #     years = list(years.split(","))
    #     years = list(map(float, years))
    #     score = row[1]["score"]
    #     score = score.replace('[', '')
    #     score = score.replace(']', '')
    #     score = score.replace(' ', '')
    #     score = list(score.split(","))
    #     score = list(map(float, score))
    #
    #     for year, mean_score in zip(years, score):
    #         if year < 2020:
    #             continue
    #         print(year)
    #         print("-----")
    #         print(score)
    #         ranings_row = pd.DataFrame({
    #             "version": [row[1]["version"]],
    #             "methodology": [row[1]["algorithm: "]],
    #             "sheetname": [row[1]["field_of_study"]],
    #             "indicator": [row[1]["indicator"]],
    #             "year": [year],
    #             "mean_score": [mean_score],
    #             "country": [row[1]["country"]],
    #             "error_methodology": [row[1]["error_methodology"]]
    #         })
    #         rankings = pd.concat([rankings, ranings_row], ignore_index=True, axis=0)

    Path("../data/statistical_results").mkdir(parents=True, exist_ok=True)

    for error_meth in ["RMSE", "MAE"]:
        rank_file = "rank_countries_" + error_meth + ".xlsx"
        if error_meth == "MAE":
            best_metodologiest_mae = best_metodologiest.dropna(axis=0, subset=['MAE'])
            best_metodologiest_mae = best_metodologiest_mae.groupby(by=['MAE'])
            best_metodologiest_mae = best_metodologiest_mae.apply(lambda g: g[g['MAE'] == g['MAE'].min()])
            best_metodologiest_mae = best_metodologiest_mae.groupby(
                ["country", "version", "field_of_study", "indicator", "years"]).nth(0).reset_index()
            best_metodologiest_mae[f'country_raiting'] = best_metodologiest_mae.groupby(["version", "field_of_study", "indicator", "years"])["mean_score"].rank(axis=0, method="min", ascending=False)

            if os.path.exists("../data/final_ranks_reesults/" + rank_file):
                os.remove("../data/final_ranks_reesults/" + rank_file)
                print("The file has been deleted successfully")
            else:
                print("The file does not exist!")

            best_metodologiest_mae.to_excel("../data/final_ranks_reesults/" + rank_file, index=False)
        else:
            best_metodologiest_rmse = best_metodologiest.dropna(axis=0, subset=['RMSE'])
            best_metodologiest_rmse = best_metodologiest_rmse.groupby(by=['RMSE'])
            best_metodologiest_rmse = best_metodologiest_rmse.apply(lambda g: g[g['RMSE'] == g['RMSE'].min()])
            best_metodologiest_rmse = best_metodologiest_rmse.groupby(["country", "version", "field_of_study", "indicator", "years"]).nth(0).reset_index()
            best_metodologiest_rmse[f'country_raiting'] = \
            best_metodologiest_rmse.groupby(["version", "field_of_study", "indicator", "years"])["mean_score"].rank(
                axis=0, method="min", ascending=False)

            rank_file = "rank_countries_" + error_meth + ".xlsx"

            if os.path.exists("../data/final_ranks_reesults/" + rank_file):
                os.remove("../data/final_ranks_reesults/" + rank_file)
                print("The file has been deleted successfully")
            else:
                print("The file does not exist!")

            best_metodologiest_rmse.to_excel("../data/final_ranks_reesults/" + rank_file, index=False)
