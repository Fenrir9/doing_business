import logging
import pandas as pd
from pathlib import Path
import re
import os


def clean_dataset_from_randomly_working_metodologiests():
    best_algorithms = pd.DataFrame()
    std_all_algorithms = pd.DataFrame()
    stats_files_path = '../data/statistical_results'
    for stats_file in next(os.walk(stats_files_path))[2]: #["stats_first_version_exponential_regression.xlsx", "stats_second_version_mlp_regressor.xlsx"]:#
        stats_regex = re.compile(r'stats.*')
        if not stats_regex.match(stats_file):
            continue
        # groups = re.search(r"(_).*?(_)", stats_file)

        version = stats_file.split("_")[1] + "_version"
        algorithm = stats_file.split("version_")[1].replace('.xlsx', '')

        stats_file_fd = pd.read_excel("../data/statistical_results/stats_" + version + "_" + algorithm + ".xlsx", engine='openpyxl')
        stats_file_fd["version"] = version
        #stats_file_fd["algorithm"] = algorithm
        print(stats_file)
        print(stats_file_fd.dtypes)
        std_all_algorithms = pd.concat([std_all_algorithms, stats_file_fd])
        print(std_all_algorithms.dtypes)

    cleaned_all_algorithms = std_all_algorithms.copy(deep=True)
    cleaned_all_algorithms = cleaned_all_algorithms.sort_values(
        by=["country", "field_of_study", "indicator", "algorithm: ", "version", "std"])
    cleaned_all_algorithms = cleaned_all_algorithms.drop_duplicates(
        subset=["country", "field_of_study", "indicator", "algorithm: ", "version"], keep='last')
    cleaned_all_algorithms = cleaned_all_algorithms[cleaned_all_algorithms['std'] <= 3]
    cleaned_all_algorithms = cleaned_all_algorithms.drop(columns=['years'])
    print("cleaned_all_algorithms ", cleaned_all_algorithms.dtypes)

    for error_meas_met in ["RMSE", "MAE"]:
        if error_meas_met == "RMSE":
            error_meas = pd.read_excel("../data/statistical_results/rmse_results.xlsx", engine='openpyxl')
            error_meas['RMSE'] = error_meas['RMSE'].str.replace(",", ".").astype(float)
        elif error_meas_met == "MAE":
            error_meas = pd.read_excel("../data/statistical_results/mae_results.xlsx", engine='openpyxl')

        print(error_meas_met)
        errors_meas_with_cleaned_all_algorithms = cleaned_all_algorithms.merge(
            error_meas,
            how='inner',
            left_on=["country", "field_of_study", "indicator", "algorithm: ", "version"],
            right_on=["country", "field_of_study", "indicator", "methodology", "version"])

        print("errors_meas_with_cleaned_all_algorithms: ", errors_meas_with_cleaned_all_algorithms.dtypes)

        errors_meas_with_cleaned_all_algorithms[f'mathods_raiting'] = \
            errors_meas_with_cleaned_all_algorithms.groupby([
                "country",  "version", "field_of_study", "indicator"])[error_meas_met].\
                rank(axis=0, method="min", ascending=True)

        errors_meas_with_cleaned_all_algorithms = errors_meas_with_cleaned_all_algorithms[errors_meas_with_cleaned_all_algorithms['mathods_raiting'] == 1]
        best_algorithms = pd.concat([best_algorithms, errors_meas_with_cleaned_all_algorithms])

    std_all_algorithms = std_all_algorithms[std_all_algorithms['years'] > 2020]
    best_algorithms = best_algorithms.drop(['mean_score', 'std', 'confidence_intervals'], axis=1)
    best_algorithms = best_algorithms.merge(
        std_all_algorithms,
        on=["country", "field_of_study", "indicator", "algorithm: ", "version"]
    )
    print("lllalala")
    # index = 1
    # best_algorithm_with_year = pd.DataFrame()
    # for row in best_algorithms.iterrows():
    #     index +=1
    #     print(index)
    #
    #     tmp_dataframe = std_all_algorithms[(std_all_algorithms["country"] == row[1]["country"]) &
    #                                        (std_all_algorithms["field_of_study"] == row[1]["field_of_study"]) &
    #                                        (std_all_algorithms["indicator"] == row[1]["indicator"]) &
    #                                        (std_all_algorithms["algorithm: "] == row[1]["methodology"]) &
    #                                        (std_all_algorithms["version"] == row[1]["version"])]
    #
    #     algorithm_row = pd.DataFrame({
    #         "algorithm: ": [row[1]["algorithm: "]],
    #         "field_of_study": [row[1]["field_of_study"]],
    #         "indicator": [row[1]["indicator"]],
    #         "country": [row[1]["country"]],
    #         "years": [tmp_dataframe['years'].tolist()],
    #         "score": [tmp_dataframe['mean_score'].tolist()],
    #         "std": [tmp_dataframe['std'].tolist()],
    #         "confidence_intervals": [tmp_dataframe['confidence_intervals'].tolist()],
    #         "RMSE": [row[1]["RMSE"]],
    #         "MAE": [row[1]["MAE"]],
    #         "version": [row[1]["version"]]})

    #    best_algorithm_with_year = pd.concat([best_algorithm_with_year, algorithm_row], ignore_index=True, axis=0)























    #for version in ["first_version", "second_version"]:
    # for error_meas_met in ["RMSE", "MAE"]:
    #     if error_meas_met == "RMSE":
    #         error_meas = pd.read_excel("../data/statistical_results/rmse_results.xlsx", engine='openpyxl')
    #     elif error_meas_met == "MAE":
    #         error_meas = pd.read_excel("../data/statistical_results/mae_results.xlsx", engine='openpyxl')
    #
    #     all_algorithms_with_error = std_all_algorithms.merge(
    #         error_meas,
    #         how='right',
    #         left_on=["country", "field_of_study", "indicator", "algorithm: ", "version"],
    #         right_on=["country", "field_of_study", "indicator", "methodology", "version"])
    #
    #     all_algorithms_with_error[f'mathods_raiting'] = \
    #         all_algorithms_with_error.groupby([
    #             "country",  "version", "field_of_study", "indicator"])[error_meas_met].\
    #             rank(axis=0, method="min", ascending=True)
    #
    #     best_algorithms = pd.concat([best_algorithms, all_algorithms_with_error])

            # algorithm_row = pd.DataFrame({
            #     "algorithm: ": [algorithm],
            #     "field_of_study": [field_of_study],
            #                                     "indicator": [indicator],
            #                                     "country": [country],
            #                                     "years": [std_for_methodology['years'].tolist()],
            #                                     "score": [std_for_methodology['mean_score'].tolist()],
            #                                     "std": [std_for_methodology['std'].tolist()],
            #                                     "confidence_intervals": [std_for_methodology['confidence_intervals'].tolist()],
            #                                     "error_methodology": [error_meas_met],
            #                                     "version": [version]})
            #
            #                                 best_algorithm = pd.concat([best_algorithm, algorithm_row], ignore_index=True, axis=0)

    # std_data_frame = pd.read_excel(metodologies_file + algorithm + ".xlsx", engine='openpyxl')

    # for version in ["first_version", "second_version"]:
    #     print("clean_dataset_from_randomly_working_metodologiests ", version)
    #     for error_meas_met in ["RMSE", "MAE"]:
    #         if error_meas_met == "RMSE":
    #             error_meas = pd.read_excel("../data/statistical_results/rmse_results.xlsx", engine='openpyxl')
    #         elif error_meas_met == "MAE":
    #             error_meas = pd.read_excel("../data/statistical_results/mae_results.xlsx", engine='openpyxl')
    #
    #         print("clean_dataset_from_randomly_working_metodologiests ", error_meas_met)
    #         score_regex = re.compile(r'Score.*')
    #         for field_of_study in error_meas["field_of_study"].unique(): #["startingBusinessData"]:#
    #             print("clean_dataset_from_randomly_working_metodologiests ", field_of_study)
    #             for indicator in error_meas["indicator"].unique(): #["Score.Procedures...Women..number."]:#
    #                 if not score_regex.match(indicator):
    #                     continue
    #                 print("clean_dataset_from_randomly_working_metodologiests ", indicator)
    #                 for country in error_meas["country"].unique(): #["Albania", "Poland", "Zimbabwe"]:#
    #                     print("clean_dataset_from_randomly_working_metodologiests ", country)
    #                     research = error_meas.loc[(error_meas['field_of_study'] == field_of_study) &
    #                                               (error_meas['indicator'] == indicator) &
    #                                               (error_meas['country'] == country) &
    #                                               (error_meas['version'] == version)]
    #
    #                     if research.empty:
    #                         continue
    #
    #                     research[f'metodologiests_raiting'] = research[error_meas_met].rank(axis=0, method="min", ascending=True)
    #                     research = research.sort_values(by=['metodologiests_raiting'])
    #
    #                     print(research)
    #
    #                     metodologies_file = "../data/statistical_results/stats_" + version + "_"
    #                     for algorithm in research['methodology']:
    #                         std_data_frame = pd.read_excel(metodologies_file + algorithm + ".xlsx", engine='openpyxl')
    #
    #                         print(error_meas.keys())
    #                         std_for_methodology = std_data_frame.loc[(std_data_frame['field_of_study'] == field_of_study) &
    #                                                              (std_data_frame['indicator'] == indicator) &
    #                                                              (std_data_frame['country'] == country) &
    #                                                              (std_data_frame['algorithm: '] == algorithm)]
    #
    #                         if std_for_methodology.empty:
    #                             continue
    #
    #                         if std_for_methodology['std'].max() <= 3.0:
    #                             std_for_methodology = std_for_methodology.sort_values(by=['years'])
    #                             algorithm_row = pd.DataFrame({
    #                                 "algorithm: ": [algorithm],
    #                                 "field_of_study": [field_of_study],
    #                                 "indicator": [indicator],
    #                                 "country": [country],
    #                                 "years": [std_for_methodology['years'].tolist()],
    #                                 "score": [std_for_methodology['mean_score'].tolist()],
    #                                 "std": [std_for_methodology['std'].tolist()],
    #                                 "confidence_intervals": [std_for_methodology['confidence_intervals'].tolist()],
    #                                 "error_methodology": [error_meas_met],
    #                                 "version": [version]})
    #
    #                             best_algorithm = pd.concat([best_algorithm, algorithm_row], ignore_index=True, axis=0)
    #                             continue
    #
    Path('../data/statistical_results').mkdir(parents=True, exist_ok=True)
    best_algorithms.to_excel('../data/statistical_results/best_metogogies.xlsx', index=False)
