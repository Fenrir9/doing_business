import pandas as pd
import os


def calculate_scores():
    error_meth_patg = '../data/final_ranks_reesults/'
    for error_meth in ["RMSE", "MAE"]:  # ["RMSE"]:#
        if error_meth == "RMSE":
            error_meth_file = "rank_countries_RMSE"
        elif error_meth == "MAE":
            error_meth_file = "rank_countries_MAE"
        ranked_countries = pd.read_excel(error_meth_patg + error_meth_file + ".xlsx", engine='openpyxl')

        # print(ranked_countries)

        indicators_to_remove = "Score.Dealing.with.construction.permits.|Score.Getting.credit.|Score.Getting.electricity.|" \
                               "Score.Paying.Taxes.|Score.Protecting.minority.investors.|Score.Registering.property.|" \
                               "Score.Dealing.with.construction.permits..DB16.20.methodology.|" \
                               "Score.Enforcing.contracts..DB17.20.methodology.|Score.Getting.credit..DB15.20.methodology.|" \
                               "Score.Protecting.minority.investors..DB15.20.methodology.|" \
                               "Score.Registering.property..DB17.20.methodology.|Score.Starting.a.business|" \
                               "Score.Trading.across.borders..DB16.20.methodology.|Score.Enforcing.contracts.|" \
                               "Score.Paying.taxes..DB17.20.methodology.|Score.Resolving.insolvency"
        ranked_countries = ranked_countries[ranked_countries["indicator"].str.contains(indicators_to_remove) == False]

        # print(ranked_countries)


        def calculate_fos_score(x):
            score = x[x.columns[10]].mean()

            score_fos_row = pd.DataFrame({
                "country": [x.iloc[0]['country']],
                "version": [x.iloc[0]['version']],
                "field_of_study": [x.iloc[0]['field_of_study']],
                "indicator": ["Score." + x.iloc[0]['field_of_study']],
                "years": [x.iloc[0]['years']],
                "algorithm: ": [x.iloc[0]['algorithm: ']],
                "RMSE": [""],
                "methodology": [x.iloc[0]['methodology']],
                "mathods_raiting": [""],
                "MAE": [""],
                "mean_score": [score],
                "std": [""],
                "confidence_intervals": [""],
                "country_raiting": [""]
                })

            x = pd.concat([x, score_fos_row], ignore_index=True, axis=0)
            return x

        ranked_countries = ranked_countries.groupby(["country", "version", "field_of_study", "years"]).apply(calculate_fos_score).reset_index(drop=True)

        def calculate_eodb_score(x):
            score = x.loc[((x['indicator'] == 'Score.dealingWithConstructionPermitsD')
                 | (x['indicator'] == 'Score.enforcingContractsData')
                 | (x['indicator'] == 'Score.gettingCreditData')
                 | (x['indicator'] == 'Score.gettingElectricityData')
                 | (x['indicator'] == 'Score.payingTaxesData')
                 | (x['indicator'] == 'Score.protectingMiniorityInvestorsDat')
                 | (x['indicator'] == 'Score.registeringPropertyData')
                 | (x['indicator'] == 'Score.startingBusinessData')
                 | (x['indicator'] == 'Score.resolvingInsolvencyData')
                 | (x['indicator'] == 'Score.tradingAcrossBordersData')), 'mean_score'].mean()

            score_eodb_row = pd.DataFrame({
                "country": [x.iloc[0]['country']],
                "version": [x.iloc[0]['version']],
                "field_of_study": [""],
                "indicator": ["Score.EaseOfDoingBusiness"],
                "years": [x.iloc[0]['years']],
                "algorithm: ": [""],
                "RMSE": [""],
                "methodology": [""],
                "mathods_raiting": [""],
                "MAE": [""],
                "mean_score": [score],
                "std": [""],
                "confidence_intervals": [""],
                "country_raiting": [""]
                })

            x = pd.concat([x, score_eodb_row], ignore_index=True, axis=0)
            return x

        ranked_countries = ranked_countries.groupby(["country", "version", "years"]).apply(
            calculate_eodb_score).reset_index(drop=True)

        ranked_countries[f'country_raiting'] = \
            ranked_countries.groupby(["version", "field_of_study", "indicator", "years"])["mean_score"].rank(
                axis=0, method="min", ascending=False)

        if os.path.exists("../data/final_ranks_reesults/" + error_meth_file + "_with_scored.xlsx"):
            os.remove("../data/final_ranks_reesults/" + error_meth_file + "_with_scored.xlsx")
            print("The file has been deleted successfully")
        else:
            print("The file does not exist!")

        ranked_countries.to_excel("../data/final_ranks_reesults/" + error_meth_file + "_with_scored.xlsx", index=False)