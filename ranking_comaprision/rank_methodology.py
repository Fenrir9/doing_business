import logging
import numpy as np
import os
import pandas as pd
from pathlib import Path
import re


def rank_metodologies():
    for version in ["first_version", "second_version"]:

        # if version == "first_version":
        #     methodologies_path = '../data/all_data_common_denominator'
        # elif version == "second_version":
        #     methodologies_path = '../data/new_methodology'
        # else:
        #     logging.exception("Invalid data_version")
        #     quit()

        statistic_path = '../data/statistical_results'

        rank_stats = pd.read_excel(statistic_path + '/rank_stats_data.xlsx')

        sample_number = rank_stats.year.unique().size
        rank_stats['tau p_value scalled'] = rank_stats['tau p_value'] * sample_number
        rank_stats['speerman corelation scalled'] = rank_stats['p_value_spearman'] * sample_number

        rank_stats = rank_stats.fillna({'tau': 0, 'speerman corelation': 0})

        kendal_tau_rank_compatibility = rank_stats.groupby(['version', 'methodology', 'sheetname', 'rank'])[['tau', 'tau p_value scalled']].mean().reset_index()
        speerman_rank_compatibility = rank_stats.groupby(['version', 'methodology', 'sheetname', 'rank'])[['speerman corelation', 'speerman corelation scalled']].mean().reset_index()

        kendal_tau_rank_compatibility[f'kendal_tau_raiting'] = kendal_tau_rank_compatibility.groupby(['version', 'sheetname', 'rank'])['tau'].rank("min", ascending=False)


        speerman_rank_compatibility[f'speerman_raiting'] = speerman_rank_compatibility.groupby(['version', 'sheetname', 'rank'])[
            'speerman corelation'].rank("min", ascending=False)


        #print(kendal_tau_test_value)

    Path("../data/final_ranks_reesults").mkdir(parents=True, exist_ok=True)
    if os.path.exists("../data/final_ranks_reesults/kendal_tau_only_compatibility.xlsx"):
        os.remove("../data/final_ranks_reesults/kendal_tau_only_compatibility.xlsx")
        print("The file has been deleted successfully")
    else:
        print("The file does not exist!")

    if os.path.exists("../data/final_ranks_reesults/speerman_only_compatibility.xlsx"):
        os.remove("../data/final_ranks_reesults/speerman_only_compatibility.xlsx")
        print("The file has been deleted successfully")
    else:
        print("The file does not exist!")

    kendal_tau_rank_compatibility.to_excel('../data/final_ranks_reesults/kendal_tau_only_compatibility.xlsx', sheet_name="tau_kendal", index=False)
    speerman_rank_compatibility.to_excel('../data/final_ranks_reesults/speerman_only_compatibility.xlsx', sheet_name="speerman", index=False)

    kendal_tau_rank_occurences = kendal_tau_rank_compatibility.groupby(['methodology', 'kendal_tau_raiting']).size().unstack(fill_value=0)
    speerman_rank_occurences = speerman_rank_compatibility.groupby(['methodology', 'speerman_raiting']).size().unstack(fill_value=0)

    if os.path.exists("../data/final_ranks_reesults/kendal_tau_compare_rank_compatibility.xlsx"):
        os.remove("../data/final_ranks_reesults/kendal_tau_compare_rank_compatibility.xlsx")
        print("The file has been deleted successfully")
    else:
        print("The file does not exist!")

    if os.path.exists("../data/final_ranks_reesults/speerman_compare_rank_compatibility.xlsx"):
        os.remove("../data/final_ranks_reesults/speerman_compare_rank_compatibility.xlsx")
        print("The file has been deleted successfully")
    else:
        print("The file does not exist!")

    kendal_tau_rank_occurences.to_excel('../data/final_ranks_reesults/kendal_tau_compare_rank_compatibility.xlsx', sheet_name="tau_kendal", index=True)
    speerman_rank_occurences.to_excel('../data/final_ranks_reesults/speerman_compare_rank_compatibility.xlsx', sheet_name="speerman", index=True)