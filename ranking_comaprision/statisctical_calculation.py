import logging
import numpy as np
from openpyxl import load_workbook
import os
#from os import listdir
import pandas as pd
from pathlib import Path
import re
from scipy import stats

#methodologies_path = '../data/all_data_common_denominator'
#methodology = 'bayesian_ridge_regression_linear'


def create_data_frame_per_indicator(field_of_study, indicator, methodologies_path, methodology):
    all_metrics = pd.DataFrame()

    metrics_files = os.listdir(methodologies_path + "/" + methodology + "/")

    for metrics_file in metrics_files:
        print("create_data_frame_per_indicator ", metrics_file)
        metrics = \
            pd.read_excel(methodologies_path + "/" + methodology + '/' + metrics_file, engine='openpyxl')#[
                #["field_of_study", "indicator", "country", "years", "score", "algorithm: "]]

        metrics = metrics[metrics['years'].notna()]

        # metrics_for_indicator = metrics.loc[
        #     (metrics['field_of_study'] == field_of_study) &
        #     (metrics['indicator'] == indicator) &
        #     ((metrics['country'] == "Poland") | (metrics['country'] == "Zimbabwe"))][
        #     ["years", "country", "score"]]

        metrics_for_indicator = metrics.loc[
            (metrics['field_of_study'] == field_of_study) &
            (metrics['indicator'] == indicator)][["years", "country", "score"]]

        metrics_dataframe = pd.DataFrame()

        for i in range(metrics_for_indicator.shape[0]):
            new_dataframe = pd.DataFrame(
                {'years': [int(x) for x in
                           metrics_for_indicator["years"].iloc[i].replace("[", "").replace("]", "").replace(".0", "").split(
                               ",")],
                 'country': metrics_for_indicator['country'].iloc[i],
                 'score': [float(x) for x in
                                       metrics_for_indicator["score"].iloc[i].replace("[", "").replace("]", "").split(",")],
                 'field_of_study': field_of_study,
                 'indicator': indicator,
                 'algorithm: ': methodology})

            new_dataframe = new_dataframe.rename(columns={'score': f'score_{metrics_file}'})
            metrics_dataframe = pd.concat([metrics_dataframe, new_dataframe], ignore_index=True)

        if all_metrics.empty:
            all_metrics = metrics_dataframe
        else:
            all_metrics = \
                pd.merge(
                    all_metrics,
                    metrics_dataframe,
                    how='outer',
                    on=["field_of_study", "indicator", "country", "years", "algorithm: "],
                    suffixes=('', '_y'))

        all_metrics.drop(all_metrics.filter(regex='_y$').columns.tolist(), axis=1, inplace=True)
    # print(all_metrics)
    return all_metrics


def statistical_calculations():
    for version in ["first_version", "second_version"]: #
        print("statistical_calculations ", version)
        if version == "first_version":
            methodologies_path = '../data/all_data_common_denominator'
            # methodologies_path = '../../../temp_data/plots/all_data_common_denominator'
        elif version == "second_version":
            methodologies_path = '../data/new_methodology'
        else:
            logging.exception("Invalid data_version")
            quit()

        methodologies_file = methodologies_path + '/' + version + '.xlsx'

        for methodology in ["bayesian_ridge_regression_linear",
                            "bayesian_ridge_regression_linear_random",
                            "exponential_regression",
                            "exponential_regression_random",
                            "linear_regression_ind_separated",
                            "linear_regression_ind_separated_random",
                            "polynomial_regression_random_sklearn",
                            "polynomial_regression_sklearn",
                            "neural_prophet_AR_Net"]:

            metrix_dataframe_for_indicator = pd.DataFrame()
            print("__________________________________ ", methodology)


            sheetnames = load_workbook(methodologies_file, read_only=True, keep_links=False).sheetnames

            for sheetname in ['economicData', 'ranks', 'unrankedData', 'scoreData', 'easeOfDoingBusinessData']:
                sheetnames.remove(sheetname)


            #print(sheetnames)

            for sheetname in sheetnames:
                print("statistical_calculations ", sheetname)
                rankings = pd.read_excel(methodologies_file, sheet_name=sheetname).columns.values.tolist()
                # for meta_data in ['Economy', 'DB.Year', 'Country.code']:
                #     rankings.remove(meta_data)

                for rank in list(filter(re.compile("Score[^$]").match, rankings)):
                    print(rank)
                    metrix_dataframe_for_indicator = pd.concat(
                        [metrix_dataframe_for_indicator,
                        create_data_frame_per_indicator(sheetname, rank, methodologies_path, methodology)],
                        ignore_index=True)

            metrix_dataframe_for_indicator["mean_score"] = metrix_dataframe_for_indicator[
                list(filter(re.compile("^score*").match, metrix_dataframe_for_indicator.columns))].mean(axis=1)
            metrix_dataframe_for_indicator["std"] = metrix_dataframe_for_indicator[list(filter(re.compile("score[^$]").match, metrix_dataframe_for_indicator.columns))].std(axis=1)
            metrix_dataframe_for_indicator['confidence_intervals'] = \
                metrix_dataframe_for_indicator[list(filter(re.compile("score[^$]").match, metrix_dataframe_for_indicator.columns))].apply(lambda row: stats.norm.interval(0.95, loc=np.mean(row), scale=np.std(row)), axis=1)

            metrix_dataframe_for_indicator.drop(columns=list(filter(re.compile("score[^$]").match, metrix_dataframe_for_indicator.columns)), axis=1, inplace=True)

            Path("../data/statistical_results").mkdir(parents=True, exist_ok=True)
            metrix_dataframe_for_indicator.to_excel(
                '../data/statistical_results/stats_' + version + '_' + methodology + '.xlsx', index=False)

            # Path("../../../temp_data/plots/data/statistical_results").mkdir(parents=True, exist_ok=True)
            # metrix_dataframe_for_indicator.to_excel(
            #     '../../../temp_data/plots/data/statistical_results/stats_' + version + '_' + methodology + '.xlsx', index=False)

